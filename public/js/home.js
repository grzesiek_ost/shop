$('.slider').slick({
    dots: true,
    autoplay: true,
    infinite: true,
    autoplaySpeed: 4000,
    speed: 1000,
    mobileFirst: true,
    arrows: false,
    variableWidth: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    pauseOnHover: false
});