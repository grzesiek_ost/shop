$(function(){
    updateQuantities();
    
    var product_id = $('#product_id').val();
    var cookie_val = getCookie("product_"+product_id+"_displated");
    var now_time = new Date().getTime();
    
    if(typeof cookie_val === 'undefined' || 
            (typeof cookie_val !== 'undefined' && now_time - parseInt(cookie_val) > 10 * 1000)) {
        document.cookie = "product_"+product_id+"_displated="+new Date().getTime()+"; expires=Fri, 31 Dec "+(new Date().getFullYear() + 5)+" 23:59:59 GMT; path=/";
    
        increaseCounter();
    }
});

function getCookie(name) {
  var value = "; " + document.cookie;
  var parts = value.split("; " + name + "=");
  if (parts.length == 2) return parts.pop().split(";").shift();
}

//action after selecting size
$('.size').on('changed.bs.select', function(e){
    updateQuantities();
});

//action after click add to cart button
$('#add_to_cart').on('click', function(e){
    addToCart(this);
});

//action after click buy button
$('#buy').on('click', function(e){
    buy(this);
});

//gallery
var myLightbox = GLightbox({
    selector: 'glightbox'
});

var url_cart_store = $("[name=url_cart_store]").val();
var url_get_quantity = $("[name=url_get_quantity]").val();

/* add +1 to number of product views */
//TODO
function increaseCounter(){  
    var url_increase_counter = $('[name=url_increase_counter]').val();
//    axios.post(url_increase_counter, {
//        product_id:product_id
//    }).then(function(response){
//
//    });
}

//add product to session
function addToCart(e, cart_path){
    var product_id = $("#product_id").val();
    var size_id = $("#size").val();
    var quantity = $("#quantity").val();
    var $button = $(e);
    $button.children('.fa-spinner').fadeIn();

    axios.post(url_cart_store, {
        size_id:size_id, 
        product_id:product_id,
        quantity:quantity
    }).then(function(response){
        $button.children('.fa-spinner').fadeOut();
        if(cart_path){
            window.location.replace(cart_path);
        }
        else{
            $('#addedToCart').modal('show');
        }
    });
}

//add product to session and redirect to cart
function buy(e){
    addToCart(e, $(e).data('cart'));
}

//get number of available products and refresh quantities select (max 5 items)
function updateQuantities(){
    var product_id = $("#product_id").val();
    var size = $("#size").val();
    axios.post(url_get_quantity, {
        size:size,
        product_id:product_id
    }).then(function(response){
        $('select[name=quantity]').empty();
        var i;
        for (i = 0; i < response.data; i++) {
            if (i === 5) {
                break;
            }
            $('select[name=quantity]').append('<option value="'+(i+1)+'">'+(i+1)+'</option>');
        }
        $('.quantity').selectpicker('refresh');

        if(response.data === 0){
            $('#add_to_cart').attr("disabled", "disabled");
            $('#buy').attr("disabled", "disabled");
        }
        else{
            $('#add_to_cart').attr("disabled", false);
            $('#buy').attr("disabled", false);
        }
    });
}