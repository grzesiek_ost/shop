var click_type, product_id, size_id, cart_path;

$('.buy_now, .add_to_cart').on('click', function(e){
   e.preventDefault();
   $('.social').css('opacity', 0);
   $(this).closest('.social').siblings('.product_sizes').css('transform', 'scale(1)').css('opacity', 1);
   click_type = $(this).data('type');
});

$('.product_sizes').mouseleave(function(){
    $('.product_sizes').css('transform', 'scale(0)').css('opacity', 0);
    $('.social').css('opacity', 1);
});

$('.select_item').on('click', function(e){
    e.preventDefault();
    size_id = $(this).data('size-id');
    product_id = $(this).data('product-id');
    addToCart(this);
});

var url_cart_store = $("[name=url_cart_store]").val();

//add product to session
function addToCart(e){            
    axios.post(url_cart_store, {
        size_id:size_id, 
        product_id:product_id,
        quantity:1
    }).then(function(response){
        if(click_type == 'buy'){
            cart_path = $('[name=cart_path]').val();
            window.location.replace(cart_path);
        }
        else if(click_type == 'add'){
            $('#addedToCart').modal('show');
        }
    });
}