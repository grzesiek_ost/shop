//action after remove product from cart
$('.remove_cart').on('click', function(){
    removeFromCart(this);
});

var url = $("[name=url_cart_destroy]").val();

function removeFromCart(e){
    new Vue({
        created: function () {
            var product_id = $(e).data('product');
            var size_id = $(e).data('size');
   
            axios.post(url, {
                size_id:size_id, 
                product_id:product_id
            }).then(function(response){
                $(e).closest('tr').remove();
                var row_count = $('.product_item').length;
                if(row_count == 0){
                    $('.products_list').hide();
                    $('.no_products_div').show();
                }
            });
        }
    });
}