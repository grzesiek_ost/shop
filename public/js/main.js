$(function(){ 
    
    $('.language, .currency').selectpicker(); 
    
    /* change language */
    $('.language').on('change', function(){
        $('#change_language_form').submit();
    });
    
    /* change currency */
    $('.currency').on('change', function(){
        $('#change_currency_form').submit();
    });
    
    /* cookie info */
    $('.cookie_info button').on('click', function(){
        $('.cookie_info').fadeOut();
        document.cookie = "CookieAccepted=true; expires=Fri, 31 Dec "+(new Date().getFullYear() + 5)+" 23:59:59 GMT; path=/";
    });
    
    /* if cookie was accepted */
    if (document.cookie.indexOf("CookieAccepted=true")<0) {
        $(".cookie_info").css('opacity', 1);
    }
    else{
        $(".cookie_info").hide();
    }
});
