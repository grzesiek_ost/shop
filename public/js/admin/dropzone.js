Dropzone.autoDiscover = false;
let uploadedCroppedImage;
$(document).ready(function(){
    let url = $('#dropzone').data('url');
    let uploadedFiles = [];
    let dropzoneOptions = {
        url: url,
        method: 'POST',
        addRemoveLinks: true,
        headers: {
            'X-CSRF-TOKEN': $('[name="csrf-token"]').attr('content')
        },
        maxFilesize: 5,
        maxFiles: 4,
        dictDefaultMessage: "Please drop files to be uploaded here",
        paramName: "file",
        acceptedFiles: "image/jpeg",
        transformFile: function(file, done) {
            
            let fd = new FormData();
            fd.append('file', file);
            
            axios.post(url, fd).then(function(response){
                if(response.status == 200){
                    myDropzone.emit("complete", file);
                    // var blobURL = URL.createObjectURL(file);
                    //
                    // $('#dropzoneModal').modal('show');
                    // $('#dropzoneModal').on('shown.bs.modal', function() {
                    //     $('#image').cropper('replace', blobURL).attr('src', blobURL);
                    // });
                    $('#photo_data').after('<input type="hidden" name="photo_original_file_name[]" value="'+response.data.file_name+'" />');
                }
            });
        },
        init: function() {
            let totalFiles = 0,
                completeFiles = 0;

            uploadedFiles = [];

            this.on("addedfile", function(file) {
                totalFiles += 1;
                console.log('added fiiille');
            });
            this.on("processing", function(file) {
                console.log('processinggg');
            });
            
            this.on("error", function(file, response) {
                console.log('errrrr');
            });

            this.on("removedfile", function () {
                console.log('remmm');
            });

            this.on("canceled", function (file) {
                console.log('cann');
            });

            /*after every upload*/
            this.on("complete", function (file) {
                console.log('complete');
                completeFiles += 1;
                uploadedFiles.push(file);
                if (completeFiles === totalFiles) {
                    console.log('complete all');

                    let blobURL = URL.createObjectURL(uploadedFiles[0]);
                    $('#dropzoneModal').modal('show');
                    $('#dropzoneModal').on('shown.bs.modal', function() {
                        $('#image').cropper('replace', blobURL).attr('src', blobURL);
                    });
                }
            });
        }
    };

    let croppedClicks = 0;
    let myDropzone = new Dropzone("#dropzone", dropzoneOptions);

    /*action on crop image*/
    $('button[data-method=getCroppedCanvas]').on('click', function() {
        croppedClicks += 1;
        $image = $('#image');
        let cropped_image = $image.cropper('getCroppedCanvas');
        let cropped_image_url = cropped_image.toDataURL('image/jpeg');
        $('#photo_data').after('<input type="hidden" name="photo_blob[]" value="'+cropped_image_url+'" />');
        uploadedFiles.shift();

        if (uploadedFiles.length) {
            let blobURL = URL.createObjectURL(uploadedFiles[0]);
            $image.cropper('replace', blobURL).attr('src', blobURL);
        }
        else {
            $('#dropzoneModal').modal('hide');
        }
    });
});