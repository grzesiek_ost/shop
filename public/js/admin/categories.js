$(function(){
//    action after click icon to show subcategory
    $( "body" ).delegate( ".show_subcategory", "click", function(e) {
        e.preventDefault();
        var url = $(this).data('show_subcategories_url');
        showSubcategories(this, url);
    });
    
//    action after click icon to hide subcategory
    $( "body" ).delegate( ".hide_subcategory", "click", function(e) {
        e.preventDefault();
        hideSubcategories(this);
    });
});

//show subcategories
function showSubcategories(e, url){
    axios.post(url)
    .then(function (response) {
        // handle success
        if(response.data){
            var category_id = $(e).data('category_id');
            
            $(e)
                .toggleClass('show_subcategory')
                .toggleClass('hide_subcategory');
        
//            rotate icon
            $(e).children('svg')
                .toggleClass('fa-caret-square-down')
                .toggleClass('fa-caret-square-up');
            $('#category_'+category_id).after('<div data-category_id="'+category_id+'">'+response.data+'</div>');
        }
    })
    .catch(function (error) {
        // handle error
        console.log(error);
    })
    .then(function () {
        // always executed
    });
}

//hide subcategories
function hideSubcategories(e){
    var category_id = $(e).data('category_id');
    if($('div[data-category_id="'+category_id+'"]').length){
        $(e)
            .toggleClass('show_subcategory')
            .toggleClass('hide_subcategory');

//            rotate icon
        $(e).children('svg')
            .toggleClass('fa-caret-square-down')
            .toggleClass('fa-caret-square-up');
    
        $('div[data-category_id="'+category_id+'"]').remove();
    }
}
