//sort photos drag&drop
let container = document.getElementById("sortable_div");
let url = $("[name=url_change_order]").val();

Sortable.create(container, {
    animation: 200, // ms, animation speed moving items when sorting, `0` — without animation
    handle: ".sortable_handle", // Restricts sort start click/touch to the specified element
    draggable: ".sortable_element", // Specifies which items inside the element should be sortable
    onEnd: function (/**Event*/evt) {        
        let order = getOrder();
        let product = document.getElementById('product_id').value;
        
//        save order
        axios.post(url, {
            product:product,
            order:order
        });
    }
});

// Get the sorted NodeList (array) of items
function getOrder() {
    let my_srt_items = document.querySelectorAll("[data-sortable-id]");
    let order = [].map.call(my_srt_items, function (el) {
        return el.dataset.sortableId; // [data-sortable-id]
    });
    return order;
}