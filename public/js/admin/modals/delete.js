$(function(){ 
    /* show modal to confirm delete */
    $('body').delegate('.delete_item', 'click', function(e){
        e.preventDefault();
        let url = $(this).attr('href');
        $('#deleteModal').modal('show');
        $('#deleteModal').find('form').attr('action', url);
    });
});