let uploadedFiles = [];
/*cropper*/
$(function () {
    'use strict';

    let croppedClicks = 0;
    var console = window.console || { log: function () {} };
    var URL = window.URL || window.webkitURL;
    var $image = $('#image');
    var $download = $('#download');
    var $dataX = $('#dataX');
    var $dataY = $('#dataY');
    var $dataHeight = $('#dataHeight');
    var $dataWidth = $('#dataWidth');
    var $dataRotate = $('#dataRotate');
    var $dataScaleX = $('#dataScaleX');
    var $dataScaleY = $('#dataScaleY');
    var options = {
        aspectRatio: $('#dropzone').data('ratio'),
        preview: '.img-preview',
        crop: function (e) {
            $dataX.val(Math.round(e.detail.x));
            $dataY.val(Math.round(e.detail.y));
            $dataHeight.val(Math.round(e.detail.height));
            $dataWidth.val(Math.round(e.detail.width));
            $dataRotate.val(e.detail.rotate);
            $dataScaleX.val(e.detail.scaleX);
            $dataScaleY.val(e.detail.scaleY);
        }
    };
    var originalImageURL = $image.attr('src');
    var uploadedImageName = 'cropped.jpg';
    var uploadedImageType = 'image/jpeg';
    var uploadedImageURL;

    // Tooltip
    $('[data-toggle="tooltip"]').tooltip();

    // Cropper
    $image.on({
        ready: function (e) {
            console.log(e.type);
        },
        cropstart: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropmove: function (e) {
            console.log(e.type, e.detail.action);
        },
        cropend: function (e) {
            console.log(e.type, e.detail.action);
        },
        crop: function (e) {
            console.log(e.type);
        },
        zoom: function (e) {
            console.log(e.type, e.detail.ratio);
        }
    }).cropper(options);

    // Buttons
    if (!$.isFunction(document.createElement('canvas').getContext)) {
        $('button[data-method="getCroppedCanvas"]').prop('disabled', true);
    }

    if (typeof document.createElement('cropper').style.transition === 'undefined') {
        $('button[data-method="rotate"]').prop('disabled', true);
        $('button[data-method="scale"]').prop('disabled', true);
    }

    // Download
    if (typeof $download[0].download === 'undefined') {
        $download.addClass('disabled');
    }

    // Options
    $('.docs-toggles').on('change', 'input', function () {
        var $this = $(this);
        var name = $this.attr('name');
        var type = $this.prop('type');
        var cropBoxData;
        var canvasData;

        if (!$image.data('cropper')) {
            return;
        }

        if (type === 'checkbox') {
            options[name] = $this.prop('checked');
            cropBoxData = $image.cropper('getCropBoxData');
            canvasData = $image.cropper('getCanvasData');

            options.ready = function () {
                $image.cropper('setCropBoxData', cropBoxData);
                $image.cropper('setCanvasData', canvasData);
            };
        } else if (type === 'radio') {
            options[name] = $this.val();
        }

        $image.cropper('reset').cropper(options);
        if($(this).attr('name') == 'aspectRatio'){
            $image.cropper('setAspectRatio', $(this).attr('value'));
        }
//    $image.cropper('destroy').cropper(options);
    });

    // Methods
    $('.docs-buttons').on('click', '[data-method]', function () {
        var $this = $(this);
        var data = $this.data();
        var cropper = $image.data('cropper');
        var cropped;
        var $target;
        var result;

        if ($this.prop('disabled') || $this.hasClass('disabled')) {
            return;
        }

        if (cropper && data.method) {
            data = $.extend({}, data); // Clone a new one

            if (typeof data.target !== 'undefined') {
                $target = $(data.target);

                if (typeof data.option === 'undefined') {
                    try {
                        data.option = JSON.parse($target.val());
                    } catch (e) {
                        console.log(e.message);
                    }
                }
            }

            cropped = cropper.cropped;

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $image.cropper('clear');
                    }

                    break;

                case 'getCroppedCanvas':
                    if (uploadedImageType === 'image/jpeg') {
                        if (!data.option) {
                            data.option = {};
                        }

                        data.option.fillColor = '#fff';
                    }

                    break;
            }

            result = $image.cropper(data.method, data.option, data.secondOption);

            switch (data.method) {
                case 'rotate':
                    if (cropped && options.viewMode > 0) {
                        $image.cropper('crop');
                    }

                    break;

                case 'scaleX':
                case 'scaleY':
                    $(this).data('option', -data.option);
                    break;
                /* akcja przy uploadowaniu */
                case 'getCroppedCanvas':
                    if (result) {

                        uploadedCroppedImage = result.toDataURL(uploadedImageType);

                        // Bootstrap's Modal
                        $('#getCroppedCanvasModal').modal().find('.modal-body').html(result);

                        if (!$download.hasClass('disabled')) {
                            download.download = uploadedImageName;
                            $download.attr('href', result.toDataURL(uploadedImageType));
                        }
                    }

                    break;
                case 'cropImage':
                    croppedClicks += 1;
                    let cropped_image = $image.cropper('getCroppedCanvas');
                    let cropped_image_url = cropped_image.toDataURL('image/jpeg');
                    let file_name = $('[name="photo_original_file_name[]"]').eq(uploadedFiles.length - 1).val();
                    $('#photo_data').after('<input type="hidden" name="photo_blob[]" value="'+cropped_image_url+'" data-name="'+file_name+'" />');
                    uploadedFiles.shift();

                    if (uploadedFiles.length) {
                        let blobURL = URL.createObjectURL(uploadedFiles[0]);
                        $image.cropper('replace', blobURL).attr('src', blobURL);
                    }
                    else {
                        $('#dropzoneModal').modal('hide');
                    }
                    break;
                case 'destroy':
                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                        uploadedImageURL = '';
                        $image.attr('src', originalImageURL);
                    }

                    break;
            }

            if ($.isPlainObject(result) && $target) {
                try {
                    $target.val(JSON.stringify(result));
                } catch (e) {
                    console.log(e.message);
                }
            }
        }
    });

    // Keyboard
    $(document.body).on('keydown', function (e) {
        if (e.target !== this || !$image.data('cropper') || this.scrollTop > 300) {
            return;
        }

        switch (e.which) {
            case 37:
                e.preventDefault();
                $image.cropper('move', -1, 0);
                break;

            case 38:
                e.preventDefault();
                $image.cropper('move', 0, -1);
                break;

            case 39:
                e.preventDefault();
                $image.cropper('move', 1, 0);
                break;

            case 40:
                e.preventDefault();
                $image.cropper('move', 0, 1);
                break;
        }
    });

    // Import image
    var $inputImage = $('#inputImage');

    if (URL) {
        $inputImage.change(function () {
            var files = this.files;
            var file;

            if (!$image.data('cropper')) {
                return;
            }

            if (files && files.length) {
                file = files[0];

                if (/^image\/\w+$/.test(file.type)) {
                    uploadedImageName = file.name;
                    uploadedImageType = file.type;

                    if (uploadedImageURL) {
                        URL.revokeObjectURL(uploadedImageURL);
                    }

                    uploadedImageURL = URL.createObjectURL(file);
                    $image.cropper('destroy').attr('src', uploadedImageURL).cropper(options);
                    $inputImage.val('');
                } else {
                    window.alert('Please choose an image file.');
                }
            }
        });
    } else {
        $inputImage.prop('disabled', true).parent().addClass('disabled');
    }


    $('#dataWidth').on('keyup', function(e){
        var new_width = parseInt($(this).val());
        console.log($image.cropper('getCropBoxData'));
        $image.cropper('reset').cropper(options);
        $image.cropper("setCropBoxData", { width: new_width });
    });
    $('#dataHeight').on('keyup', function(e){
        var new_height = parseInt($(this).val());
        $image.cropper('reset').cropper(options);
        $image.cropper("setCropBoxData", { height: new_height });
    });
});



/*dropzone*/
Dropzone.autoDiscover = false;
let uploadedCroppedImage;
$(document).ready(function(){
    let url = $('#dropzone').data('url');
    let delete_url = $('#dropzone').data('delete-url');
    let dataAddRemoveLinks = ($('#dropzone').data('add-remove-links') == true);
    let dataMaxFileSize = $('#dropzone').data('max-file-size') ? $('#dropzone').data('max-file-size') : 2
    let dataMaxFiles = $('#dropzone').data('max-files') ? $('#dropzone').data('max-files') : 1;
    let dataDefauleMessage = $('#dropzone').data('default-message') ? $('#dropzone').data('default-message') : 'Please drop files to be uploaded here';
    let dataParamName = $('#dropzone').data('param-name') ? $('#dropzone').data('param-name') : 'file';
    let dataAcceptedFiles = $('#dropzone').data('accepted-files') ? $('#dropzone').data('accepted-files') : 'image/jpeg';
    let transformedFilesCount = 0;
    let dropzoneOptions = {
        url: url,
        method: 'POST',
        addRemoveLinks: dataAddRemoveLinks,
        headers: {
            'X-CSRF-TOKEN': $('[name="csrf-token"]').attr('content')
        },
        maxFilesize: dataMaxFileSize,
        maxFiles: dataMaxFiles,
        parallelUploads: dataMaxFiles,
        dictDefaultMessage: dataDefauleMessage,
        paramName: dataParamName,
        acceptedFiles: dataAcceptedFiles,
        transformFile: function(file, done) {
            if(myDropzone.options.maxFiles > transformedFilesCount){
                transformedFilesCount += 1;
                let fd = new FormData();
                fd.append('file', file);

                axios.post(url, fd).then(function(response){
                    if(response.status == 200){
                        myDropzone.emit("complete", file);
                        $('#photo_data').after('<input type="hidden" name="photo_original_file_name[]" value="'+response.data.file_name+'" />');
                    }
                });
            }
        },
        init: function() {
            let totalFiles = 0,
                completeFiles = 0;

            uploadedFiles = [];

            this.on("maxfilesexceeded", function(file){
                console.log("Too many files added");
            });

            this.on("addedfile", function(file) {
                action = 'addedfile';
                console.log('added fiiille');
            });
            this.on("processing", function(file) {
                totalFiles += 1;
            });

            this.on("error", function(file, response) {
                console.log('errrrr');
            });

            this.on("removedfile", function (file) {
                action = 'removedfile';
                console.log('remmm');
                totalFiles -= 1;
                axios.delete(delete_url, {data: {'file':file.name}}).then(function(response){
                    if(response.status == 200){
                        $('[name="photo_blob[]"][data-name="'+file.name+'"]').remove();
                        $('[name="photo_original_file_name[]"][value="'+file.name+'"]').remove();
                    }
                });
            });

            this.on("queuecomplete", function (file) {
                console.log('queuecomplete');
            });

            /*after every upload*/
            this.on("complete", function (file) {
                console.log('complete');
                completeFiles += 1;
                uploadedFiles.push(file);
                if (action != 'canceled' && (completeFiles === totalFiles || completeFiles == myDropzone.options.maxFiles)) {
                    uploadedFiles = uploadedFiles.filter(function(item) {
                        return item.status !== 'error';
                    });

                    let blobURL = URL.createObjectURL(uploadedFiles[0]);
                    $('#dropzoneModal').modal('show');
                    $('#dropzoneModal').on('shown.bs.modal', function() {
                        $('#image').cropper('replace', blobURL).attr('src', blobURL);
                    });
                }
            });
        },
        canceled: function (file) {
            action = 'canceled';
            console.log('canceled');
        }
    };

    let myDropzone = new Dropzone("#dropzone", dropzoneOptions);
});