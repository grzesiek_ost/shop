<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateWysiwygTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('wysiwyg_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('wysiwyg_id');
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->text('content');
            $table->timestamps();

            $table->foreign('wysiwyg_id')->references('id')->on('wysiwygs')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('wysiwyg_translations');
    }
}
