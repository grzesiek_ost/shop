<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateProductPhotoTranslationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('product_photo_translations', function (Blueprint $table) {
            $table->increments('id');
            $table->unsignedInteger('product_photo_id');
            $table->unsignedInteger('language_id');
            $table->string('name');
            $table->timestamps();
            
            $table->foreign('product_photo_id')->references('id')->on('product_photos')->onDelete('cascade');
            $table->foreign('language_id')->references('id')->on('languages')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('product_photo_translations');
    }
}
