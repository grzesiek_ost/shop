<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_translations')->insert([
            'delivery_id' => 1,
            'language_id' => 1,
            'name' => 'Post',
            'description' => 'Post'
        ]);
    }
}
