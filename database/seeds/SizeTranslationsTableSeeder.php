<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class SizeTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('size_translations')->insert([
            'size_id' => 1,
            'language_id' => 1,
            'name' => 'one size',
        ]);
    }
}
