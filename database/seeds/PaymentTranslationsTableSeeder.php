<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class PaymentTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('payment_translations')->insert([
            'name' => 'Cash',
            'description' => 'Cash',
            'payment_id' => 1,
            'language_id' => 1
        ]);
    }
}
