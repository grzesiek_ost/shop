<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $this->call(LanguagesTableSeeder::class);
        $this->call(RolesTableSeeder::class);
        $this->call(UsersTableSeeder::class);
        $this->call(CurrenciesTableSeeder::class);
        $this->call(CurrencyTranslationsTableSeeder::class);
        $this->call(PaymentsTableSeeder::class);
        $this->call(PaymentTranslationsTableSeeder::class);
        $this->call(SizesTableSeeder::class);
        $this->call(SizeTranslationsTableSeeder::class);
        $this->call(DeliveriesTableSeeder::class);
        $this->call(DeliveryTranslationsTableSeeder::class);
        $this->call(DeliveryPricesTableSeeder::class);
        $this->call(PagesTableSeeder::class);
    }
}
