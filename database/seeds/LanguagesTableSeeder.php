<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class LanguagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('languages')->insert([
            'name' => 'english',
            'native_name' => 'english',
            'shortcut' => 'en',
            'active' => 1,
            'main' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
