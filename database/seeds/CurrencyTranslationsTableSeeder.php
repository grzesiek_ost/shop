<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrencyTranslationsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies_translations')->insert([
            'currency_id' => 1,
            'language_id' => 1,
            'name' => 'Euro',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
