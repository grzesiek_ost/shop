<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class DeliveryPricesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('delivery_prices')->insert([
            'delivery_id' => 1,
            'currency_id' => 1,
            'price' => 10
        ]);
    }
}
