<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class CurrenciesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('currencies')->insert([
            'id' => 1,
            'shortcut' => 'EUR',
            'symbol' => '&euro;',
            'active' => 1,
            'main' => 1,
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
