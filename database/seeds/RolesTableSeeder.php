<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;

class RolesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('roles')->insert([
            'id' => 1,
            'name' => 'admin',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'id' => 2,
            'name' => 'worker',
            'created_at' => date('Y-m-d H:i:s')
        ]);
        DB::table('roles')->insert([
            'id' => 3,
            'name' => 'user',
            'created_at' => date('Y-m-d H:i:s')
        ]);
    }
}
