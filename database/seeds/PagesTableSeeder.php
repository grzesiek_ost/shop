<?php

use App\Page;
use App\PageElement;
use App\Slider;
use App\Wysiwyg;
use Illuminate\Database\Seeder;

class PagesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $page = Page::create([
            'name' => 'Home page',
            'url' => '/'
        ]);

        $wysiwyg = Wysiwyg::create([]);
        PageElement::create([
            'page_id' => $page->id,
            'elementable_type' => Wysiwyg::class,
            'elementable_id' => $wysiwyg->id,
            'type' => 'wysiwyg'
        ]);

        $slider = Slider::create([]);
        PageElement::create([
            'page_id' => $page->id,
            'elementable_type' => Slider::class,
            'elementable_id' => $slider->id,
            'type' => 'slider'
        ]);
    }
}
