<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;
use Illuminate\Support\Facades\File;

class ProductPhoto extends Model
{
    protected $table = 'product_photos';

    protected $fillable = [
        'product_id',
        'photo',
        'sequence'
    ];

    public static $photo_width = 480;
    public static $photo_height = 640;
    public static $ratio_height = 4;
    public static $ratio_width = 3;
    public static $keep_original = false;

    /**
     * relation to product
     * @return type
     */
    public function product() {
        return $this->belongsTo('App\Product');
    }
    
    /**
     * photo translations
     * @return type
     */
    public function translation() {
        return $this->hasMany('App\ProductPhotoTranslation');
    }
    
    /**
     * get photo data in selected language
     * @param type $language_id
     * @return type
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
    
    /**
     * removes photos
     */
    public function removePhotos() {
        if($this->photo){
            $base_path = 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' .
                DIRECTORY_SEPARATOR . 'products' . DIRECTORY_SEPARATOR . $this->product_id . DIRECTORY_SEPARATOR;
            $image_path = storage_path($base_path . ProductPhoto::$photo_width . 'x' . ProductPhoto::$photo_height . DIRECTORY_SEPARATOR . $this->photo);
            $original_image_path = storage_path($base_path . 'original' . DIRECTORY_SEPARATOR . $this->photo);

            $files = array($image_path, $original_image_path);

            foreach ($files as $file) {
                File::delete($file);
            }
            
            $this->photo = null;
        }
    }
}
