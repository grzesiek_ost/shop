<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Page extends Model
{
    protected $table = 'pages';

    protected $fillable = [
        'name', 'url'
    ];

    /**
     * @return HasMany
     */
    public function elements(): HasMany
    {
        return $this->hasMany(PageElement::class);
    }
}
