<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class SliderImageTranslation extends Model
{
    protected $table = 'slider_image_translations';

    protected $fillable = [
        'language_id', 'slider_image_id', 'name'
    ];

    public function image(): BelongsTo
    {
        return $this->belongsTo(SliderImage::class);
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class);
    }
}
