<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;

class User extends Authenticatable implements MustVerifyEmail
{
    use Notifiable;

    protected $table = 'users';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 
        'surname', 
        'email', 
        'phone', 
        'password'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];
    
    /**
     * relation to user orders
     * @return type
     */
    public function orders() {
        return $this->hasMany('App\Order');
    }
    
    /**
     * relation to user role
     * @return type
     */
    public function role() {
        return $this->belongsTo('App\Role');
    }
    
    /**
     * relation to user data
     * @return type
     */
    public function userInformation() {
        return $this->hasOne('App\UserInformation');
    }
    
    /**
     * relation to product statistics
     * @return type
     */
    public function productStatistics() {
        return $this->hasMany(ProductStatistic::class);
    }
    
    /**
     * check if user is worker or admin
     * @return boolean
     */
    public function isWorker(){
        if(Auth::user()){
            if(in_array(Auth::user()->role->name, ['worker', 'admin'])){
                return true;
            }
        }
        
        return false;
    }
    
    /**
     * return sum of all orders
     * @return type
     */
    public function getOrdersSum(){
        $sum = 0;
        foreach($this->orders as $order){
            $sum += $order->getOrderSum();
        }
        return $sum;
    }
}
