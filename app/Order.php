<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;
use App\Product;
use App\Size;

class Order extends Model
{
    protected $table = 'orders';

    protected $fillable = [
        'user_id',
        'currency_id',
        'payment_id',
        'delivery_id',
        'currency',
        'payment',
        'delivery',
        'delivery_price'
    ];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function items(){
        return $this->hasMany('App\OrderItem');
    }
    
    public function data(){
        return $this->hasOne('App\OrderData');
    }
    
    public function invoice(){
        return $this->hasOne('App\Invoice');
    }
    
    /**
     * returns the amount to be paid for the product from the order
     * @return type
     */
    public function getOrderSum()
    {
        $sum = 0;
        foreach($this->items as $item){
            $sum += $item->item_price * $item->count;
        }
        return $sum;
    }

    /**
     * return products from cart
     * @param Request $request
     * @return type
     */
    public static function getCartProducts(Request $request)
    {
        if($request->session()->has('cart')){
            $products = collect();
            $cart = $request->session()->get('cart');
            foreach ($cart as $key => $value) {
                $product = Product::findOrFail($value['product_id']);
                $product['size'] = Size::findOrFail($value['size']);
                $product['quantity'] = $value['quantity'];
                $products->push($product);
            }
            return $products;
        }
        else{
            return null;
        }
        
    }
    
    /**
     * returns the amount to pay from the basket
     * @param Request $request
     * @return int
     */
    public static function getCartSum(Request $request)
    {
        $sum = 0;
        if($request->session()->has('cart')){
            $cart = $request->session()->get('cart');
            foreach ($cart as $key => $value) {
                $product = Product::findOrFail($value['product_id']);
                $sum += $product->getPrice()->price * $value['quantity'];
            }
        }
        
        return $sum;
    }
}
