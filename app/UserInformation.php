<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserInformation extends Model
{
    protected $table = 'user_informations';

    protected $fillable = [
        'city', 
        'street', 
        'house_number', 
        'apartment_number', 
        'zip_code'
    ];
    
    public function user() {
        return $this->belongsTo('App\User');
    }
}
