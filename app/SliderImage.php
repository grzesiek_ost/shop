<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Support\Facades\File;

class SliderImage extends Model
{
    protected $table = 'slider_images';

    protected $fillable = [
        'photo'
    ];

    public static $photo_width = 1200;
    public static $photo_height = 600;
    public static $ratio_width = 2;
    public static $ratio_height = 1;
    public static $keep_original = false;

    /**
     * @return BelongsTo
     */
    public function slider(): BelongsTo
    {
        return $this->belongsTo(Slider::class);
    }

    /**
     * @return HasMany
     */
    public function translation(): HasMany
    {
        return $this->hasMany(SliderImageTranslation::class);
    }

    /**
     * get image data in selected language
     *
     * @param bool $language_id
     * @return mixed
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }

        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }

    /**
     * removes photos
     */
    public function removePhotos() {
        if($this->photo){
            $base_path = 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' .
                DIRECTORY_SEPARATOR . 'slider' . DIRECTORY_SEPARATOR . $this->slider_id . DIRECTORY_SEPARATOR;
            $image_path = storage_path($base_path . SliderImage::$photo_width . 'x' . SliderImage::$photo_height . DIRECTORY_SEPARATOR . $this->photo);
            $original_image_path = storage_path($base_path . 'original' . DIRECTORY_SEPARATOR . $this->photo);

            $files = array($image_path, $original_image_path);

            foreach ($files as $file) {
                File::delete($file);
            }

            $this->photo = null;
        }
    }
}
