<?php

namespace App\Providers;

use App\Currency;
use App\Language;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;
use Illuminate\Support\Facades\Schema;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if(Schema::hasTable((new Currency)->getTable())){
            $currencies = Currency::active()->get();
            View::share('all_currencies', $currencies);
        }

        if(Schema::hasTable((new Language)->getTable())){
            $languages = Language::active()->get();
            View::share('all_languages', $languages);
        }

        Schema::defaultStringLength(191);
        //
    }

    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }
}
