<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PaymentTranslation extends Model
{
    protected $table = 'payment_translations';

    protected $fillable = [
        'name',
        'description'
    ];
}
