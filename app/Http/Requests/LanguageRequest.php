<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Illuminate\Support\Facades\Auth;

class LanguageRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return Auth::user()->isWorker();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'name' => 'required|string|max:191',
            'native_name' => 'required|string|max:191',
            'shortcut' => 'required|string|min:2|max:2',
        ];
    }

    public function attributes()
    {
        return [
            'name' => trans('admin.name'),
            'native_name' => trans('admin.native_name'),
            'shortcut' => trans('admin.shortcut'),
        ];
    }

//    public function messages()
//    {
//        return [
//            'name.required' => '',
//        ];
//    }
}
