<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use App\User;

/**
 * Middleware class responsible for filtering users with admin or worker role
 */
class IsWorker
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->isWorker()){
            return $next($request);
        }
        
        return redirect()->route('home_page');
    }
}
