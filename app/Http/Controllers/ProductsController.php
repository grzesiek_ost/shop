<?php

namespace App\Http\Controllers;

use App\Product;
use App\Currency;
use App\Category;
use App\ProductStatistic;
use Illuminate\Support\Facades\Auth;

use Illuminate\Http\Request;

class ProductsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Product::all();
        foreach($products as $key=>$product){
            if(!$product->isFilled()){
                $products->forget($key);
            }
        }
        
        return view('products.index', 
                ['products' => $products, 
                'currency' => Currency::getCurrency()]);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        if($product->isFilled()) {
            $categories = Category::all();
            
            $product_statistic = new ProductStatistic;
            $product_statistic->product_id = $product->id;
            $product_statistic->user_id = Auth::user()->id ?? null;
            $product_statistic->browser = $_SERVER['HTTP_USER_AGENT'];
            $product_statistic->save();
            
            return view('products.show', 
                    ['product' => $product, 
                    'currency' => Currency::getCurrency(),
                    'categories' => $categories]);
        }
        else{
            return redirect()->back();
        }
    }    
}
