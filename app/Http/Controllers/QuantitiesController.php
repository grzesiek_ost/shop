<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;

class QuantitiesController extends Controller
{    
    /**
     * return the number of products 
     * @param Request $request
     * @return int
     */
    public function returnQuantity(Request $request){
        $validatedData = $request->validate([
            'size' => 'required|integer',
            'product_id' => 'required|integer',
        ]);
        
        $product = Product::findOrFail($request->post('product_id'));
        return $product->sizeQuantity($request->post('size'));
    }
}
