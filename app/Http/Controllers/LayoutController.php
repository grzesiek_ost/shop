<?php

namespace App\Http\Controllers;

use App\Page;
use App\Slider;
use App\SliderImage;
use App\Wysiwyg;
use Illuminate\View\View;

class LayoutController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $page = Page::find(1);
        $slider = $page->elements->where('elementable_type', Slider::class)->first()->elementable ?? null;
        $wysiwyg = $page->elements->where('elementable_type', Wysiwyg::class)->first()->elementable ?? null;

        return view('home_page', [
            'slider' => $slider,
            'sliderConfig' => [
                'width' => SliderImage::$photo_width,
                'height' => SliderImage::$photo_height
            ],
            'wysiwyg' => $wysiwyg,
        ]);
    }
}
