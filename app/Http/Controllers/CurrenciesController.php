<?php

namespace App\Http\Controllers;

use App\Currency;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;

class CurrenciesController extends Controller
{
    /**
     * Chacnges currency.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function changeCurrency(Request $request): RedirectResponse
    {
        $request->validate([
            'currency' => 'required|integer|exists:currencies,id'
        ]);

        $currency = Currency::where('id', session('currency'))->first();
        Session::put('currency', $request->currency);
        Cache::put('currency', $currency);

        return redirect()->back();
    }
}
