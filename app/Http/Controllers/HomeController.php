<?php

namespace App\Http\Controllers;

use App\User;
use App\Order;
use Illuminate\Support\Facades\Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(Auth::user()->isWorker()){
            /* return last 5 registered users */
            $last_registered = User::whereHas('role', function($query){
                $query->where('name', 'user');
            })
            ->orderBy('created_at', 'desc')
            ->limit(5)
            ->get();

            /* return last 5 orders */
            $last_orders = Order::orderBy('created_at', 'DESC')
                    ->limit(5)
                    ->get();
        }

        return view('home', [
            'last_registered' => $last_registered ?? null,
            'last_orders' => $last_orders ?? null,
        ]);
    }
}
