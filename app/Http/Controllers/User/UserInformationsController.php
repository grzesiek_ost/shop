<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\UserInformation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Hash;

class UserInformationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $user = Auth::user();
        
        return view('user.userInformation.index', ['user'=>$user]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|string|max:191',
            'surname' => 'required|string|max:191',
            'email' => 'required|email|min:6|max:191',
            'phone' => 'string|nullable|min:7|max:50',
            'street' => 'string|nullable|max:100',
            'house_number' => 'string|nullable|max:10',
            'apartment_number' => 'string|nullable|max:10',
            'zip_code' => 'string|nullable|max:10',
            'city' => 'string|nullable|max:100'
        ]);

        $user = Auth::user();
        $user->name = htmlspecialchars($request->post('name'));
        $user->surname = htmlspecialchars($request->post('surname'));
        $user->email = htmlspecialchars($request->post('email'));
        $user->phone = !empty($request->post('phone')) ? htmlspecialchars($request->post('phone')) : null;
        $user->save();

        if($user->userInformation){
            $user_data = UserInformation::where('user_id', $user->id)->first();
        }
        else{
            $user_data = new UserInformation;
            $user_data->user_id = $user->id;
        }
        
        $user_data->street = htmlspecialchars($request->post('street'));
        $user_data->house_number = htmlspecialchars($request->post('house_number'));
        $user_data->apartment_number = htmlspecialchars($request->post('apartment_number'));
        $user_data->zip_code = htmlspecialchars($request->post('zip_code'));
        $user_data->city = htmlspecialchars($request->post('city'));
        $user_data->save();
        
        return back()->with('message', trans('admin.saved_changes'));
    }
    
    /**
     * display change password page
     * @return type
     */
    public function password() 
    {
        return view('user.userInformation.password', [
            'user' => Auth::user()
        ]);
    }
    
    /**
     * changes user password
     * @param Request $request
     */
    public function changePassword(Request $request) 
    {
        $input = $request->post();
        $validator = Validator::make($input, [
            'old_password' => 'required',
            'password' => 'required|string|min:6|confirmed'
        ]);

        if (!Hash::check($request->post('old_password'), Auth::user()->password)) {
            $validator->errors()->add('old_password', trans('passwords.failed_old_pass'));
        }
                
        if ($validator->errors()->count() > 0) {
            return back()->withErrors($validator);
        }
        
        $user = Auth::user();
        $user->password = bcrypt($request->post('password'));
        $user->save();

        return redirect()->back()->with('message', trans('passwords.changed'));
    }
}
