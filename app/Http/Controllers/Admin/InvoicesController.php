<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Support\Str;
use App\Invoice;
use App\InvoiceData;
use App\InvoiceProduct;
use App\Order;
use App\Http\Controllers\Controller;
use Barryvdh\DomPDF\Facade as PDF;

class InvoicesController extends Controller
{
    public function index() {
        $invoices = Invoice::paginate(5);
        
        return view('admin.invoices.index', ['invoices' => $invoices]);
    }
    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store($id)
    {
        $order = Order::findOrFail($id);
        
        $invoice = new Invoice;
        $invoice->order_id = $order->id;
        $invoice->user_id = $order->user_id;
        $invoice->invoice_number = Invoice::makeInvoiceNumber();
        $invoice->currency = $order->currency;
        $invoice->payment = $order->payment;
        $invoice->delivery = $order->delivery;
        $invoice->delivery_price = $order->delivery_price;
        $invoice->duty = 23;
        $invoice->save();
        
        $invoice_data = new InvoiceData;
        $invoice_data->invoice_id = $invoice->id;
        $invoice_data->name = $order->data->name;
        $invoice_data->surname = $order->data->surname;
        $invoice_data->email = $order->data->email;
        $invoice_data->phone = $order->data->phone;
        $invoice_data->city = $order->data->city;
        $invoice_data->street = $order->data->street;
        $invoice_data->house_number = $order->data->house_number;
        $invoice_data->apartment_number = $order->data->apartment_number;
        $invoice_data->zip_code = $order->data->zip_code;
        $invoice_data->save();
        
        foreach ($order->items as $item) {
            $invoice_product = new InvoiceProduct;
            $invoice_product->invoice_id = $invoice->id;
            $invoice_product->name = $item->product;
            $invoice_product->size = $item->size;
            $invoice_product->quantity = $item->count;
            $invoice_product->price = $item->item_price;
            $invoice_product->save();
        }
        
        return redirect()->route('admin.orders.index')->with('message', trans('admin.invoice_generated'));
    }
    
    public function show($id){
        $invoice = Invoice::findOrFail($id);
        $pdf = PDF::loadView('admin.invoices.pdf', ['invoice' => $invoice]);
        $name = Str::slug(trans('admin.invoice').' '.$invoice->invoice_number, '_').'.pdf';
        return $pdf->download($name);
    }
    
    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return type
     */
    public function destroy($id)
    {
        $invoice = Invoice::findOrFail($id);
        $invoice->delete();
        
        return redirect()->route('admin.invoices.index')->with('message', trans('admin.deleted'));
    }
}
