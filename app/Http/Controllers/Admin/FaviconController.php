<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Storage;
use Illuminate\Validation\Rule;
use Illuminate\View\View;

class FaviconController extends Controller
{
    /**
     * Show the form for editing the specified resource.
     *
     * @return View
     */
    public function edit(): View
    {
        return view('admin.favicon.edit', [
                'favicon' => 'favicon',
                'photoConfig' =>
                    [
                        'aspectRatio' => [
                            'width' => 1,
                            'height' => 1
                        ],
                        'width' => 128,
                        'height' => 128
                    ]
            ]
        );
    }

    /**
     * Update favicon.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(Request $request): RedirectResponse
    {
        $request->validate([
            'favicon' => [
                'nullable',
                'max:200',
                'mimes:ico',
                Rule::dimensions()->maxWidth(256)->maxHeight(256)->ratio(1 / 1)
            ],
            'delete' => 'in:on'
        ]);

        if($request->delete && !$request->favicon){
            Storage::disk('public')->delete('favicon.ico');
        }
        else{
            Storage::disk('public')->putFileAs('', $request->file('favicon'), 'favicon.ico');
        }

        return redirect()->route('admin.favicon.edit')->with('message', trans('admin.saved_changes'));
    }
}
