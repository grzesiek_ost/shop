<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    /**
     * save uploaded images to temp directory
     * @param Request $request
     * @return type
     */
    public function create(Request $request) {
        if ($request->hasFile('file')){
            $request->validate([
                'file' => 'image|max:5024',
            ]);
//            $file_name = time().Str::random(5).'.'.$request->file->getClientOriginalExtension();
            $file_name = $request->file->getClientOriginalName();
            $photo_file = $request->file('file');
            $photo_file->storeAs(
                'public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'temp',
                $file_name
            );

            return response()->json([
                'status' => 'success',
                'file_name' => $file_name
            ]);
//            return \Illuminate\Support\Facades\Response::json('success', 200);
        }
        else{
            return \Illuminate\Support\Facades\Response::json('error', 422);
        }
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function delete(Request $request) {
        Storage::delete('public'.DIRECTORY_SEPARATOR.'images'.DIRECTORY_SEPARATOR.'temp'.DIRECTORY_SEPARATOR.$request->file);
        return \Illuminate\Support\Facades\Response::json('success', 200);
    }
}