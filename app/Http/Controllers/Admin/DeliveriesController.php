<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Delivery;
use App\Currency;
use App\Language;
use App\DeliveryTranslation;
use App\DeliveryPrice;

class DeliveriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $deliveries = Delivery::orderBy('sequence', 'ASC')->paginate(5);
        
        return view('admin.deliveries.index', 
                ['deliveries' => $deliveries, 
                'currency' => Currency::getCurrency()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $currencies = Currency::all();
        
        return view('admin.deliveries.create', 
                ['languages' => $languages, 
                'currencies' => $currencies]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $required_fields = ['name', 'description'];
        
        $error = false;
        $error_data = [];
        $delivery_translations = [];
        $delivery_currencies = [];
        $delivery_id = $request->post('id') ?? false;
        
//        check if all required fields were filled
        
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $delivery_translations[$language->id][$field] = $request->post($field)[$language->id];
                }
            }
        }
        
        $currencies = Currency::all();
        foreach($currencies as $currency){
            if(!($request->post('price')[$currency->id])){
                $error = true;
                $error_data['price'][$currency->id] = 'error';
            }
            else{
                $delivery_currencies[$currency->id] = $request->post('price')[$currency->id];
            }
        }
        
        if(!$error){
//            creating new delivery
            if($delivery_id){
                $delivery = Delivery::find($delivery_id);
//                remove old logo
                if($request->post('delete_photo')){
                    $delivery->removeLogo();
                }
            }
            else{
                $delivery = new Delivery;
            }
                        
            $active = $request->post('active') ? 1 : 0;
            $delivery->active = $active;
            $delivery->save();
                                    
//            saving delivery translations
            foreach($delivery_translations as $language_id=>$translation){
                if($delivery_id){
                    $delivery_translation = DeliveryTranslation::where('language_id', $language_id)
                            ->where('delivery_id', $delivery_id)
                            ->first();
                    if(! $delivery_translation){
                        $delivery_translation = new DeliveryTranslation;
                    }
                }
                else{
                    $delivery_translation = new DeliveryTranslation;
                }
                $delivery_translation->delivery_id = $delivery->id;
                $delivery_translation->language_id = $language_id;
                foreach ($required_fields as $field){
                    $delivery_translation->$field = $translation[$field];
                }
                $delivery_translation->save();
            }
            
//            saving delivery prices in all currencies
            foreach ($delivery_currencies as $currency_id=>$price) {
                if($delivery_id){
                    $delivery_price = DeliveryPrice::where('currency_id', $currency_id)
                            ->where('delivery_id', $delivery_id)
                            ->first();
                    if($delivery_price->count() < 1){
                        $delivery_price = new DeliveryPrice;
                    }
                }
                else{
                    $delivery_price = new DeliveryPrice;
                }
                $delivery_price->delivery_id = $delivery->id;
                $delivery_price->currency_id = $currency_id;  
                $delivery_price->price = $price;
                $delivery_price->save();
            }
            
//            saving logo
            if ($request->hasFile('photo')){
                
//                $delivery = Delivery::findOrFail($delivery->id);
                
//              remove old logo
                $delivery->removeLogo();
                
                $request->validate([
                    'photo' => 'image|file|max:1024',
                ]);
                
                $file_name = time().'.'.$request->photo->getClientOriginalExtension();

                $photo_file = $request->file('photo');
                
                $photo_file->storeAs(
                    'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'deliveries' .
                    DIRECTORY_SEPARATOR . $delivery->id . DIRECTORY_SEPARATOR . 'original',
                    $file_name
                );
                
                $delivery->logo = $file_name;
                $delivery->save();
            }
            
            return redirect()->route('admin.deliveries.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();
        $currencies = Currency::all();
        $delivery = Delivery::find($id);
        
        return view('admin.deliveries.create', 
                ['delivery' => $delivery, 
                'languages' => $languages, 
                'currencies' => $currencies]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $delivery = Delivery::findOrFail($id);
        
        $delivery->removeLogo();
        
        $delivery->delete();
        
        return redirect()->route('admin.deliveries.index')->with('message', trans('admin.deleted'));
    }
    
    /**
     * changes order of deliveries
     */
    public function changeOrder(Request $request) {
        $deliveries = Delivery::orderBy('sequence', 'ASC')->get();
        foreach ($deliveries as $key =>$delivery) {
            $delivery->sequence = $request->post('order')[$key];
            $delivery->save();
        }
    }
}
