<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Currency;
use App\Language;
use App\ProductTranslation;
use App\ProductPrice;
use App\Category;
use App\Size;

class ProductsController extends Controller
{
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        $currencies = Currency::all();
        $categories = Category::all();
        $sizes = Size::all();
        
        return view('admin.products.create', 
                ['languages' => $languages, 
                'currencies' => $currencies,
                'categories' => $categories,
                'sizes' => $sizes]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $required_fields = ['name', 'description'];
        
        $error = false;
        $error_data = [];
        $product_translations = [];
        $product_currencies = [];
        $product_id = $request->post('id') ?? false;
        
//        check if all required fields were filled
        
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $product_translations[$language->id][$field] = $request->post($field)[$language->id];
                }
            }
        }
        
        $currencies = Currency::all();
        foreach($currencies as $currency){
            if(!($request->post('price')[$currency->id])){
                $error = true;
                $error_data['price'][$currency->id] = 'error';
            }
            else{
                $product_currencies[$currency->id] = $request->post('price')[$currency->id];
            }
        }
        
        if(!$error){
//            creating new product
            if($product_id){
                $product = Product::find($request->post('id'));
            }
            else{
                $product = new Product;
            }
            $active = $request->post('active') ? 1 : 0;
            $product->active = $active;
            $product->category_id = $request->post('category');
            $product->save();
            
//            saving product translations
            foreach($product_translations as $language_id=>$translation){
                if($product_id){
                    $product_translation = ProductTranslation::where('language_id', $language_id)
                            ->where('product_id', $product_id)
                            ->first();
                    if(empty($product_translation)){
                        $product_translation = new ProductTranslation;
                    }
                }
                else{
                    $product_translation = new ProductTranslation;
                }
                $product_translation->product_id = $product->id;
                $product_translation->language_id = $language_id;
                foreach ($required_fields as $field){
                    $product_translation->$field = $translation[$field];
                }
                $product_translation->save();
            }
            
//            saving product prices in all currencies
            foreach ($product_currencies as $currency_id=>$price) {
                if($product_id){
                    $product_price = ProductPrice::where('currency_id', $currency_id)
                            ->where('product_id', $product_id)
                            ->first();
                    if($product_price->count() < 1){
                        $product_price = new ProductPrice;
                    }
                }
                else{
                    $product_price = new ProductPrice;
                }
                $product_price->product_id = $product->id;
                $product_price->currency_id = $currency_id;  
                $product_price->price = $price;
                $product_price->save();
            }
            
//            save product sizes
            $default_size = Size::where('default_size', 1)->first();
            $sizes = $request->sizes ?? $default_size->id ?? null;
            $product->sizes()->sync($sizes);
            
            return redirect()->route('admin.products.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }
    
    /**
     * Show the form for editing the specified resource.
     * @param int $id
     * @return type
     */
    public function edit($id)
    {
        $languages = Language::all();
        $currencies = Currency::all();
        $product = Product::find($id);
        $categories = Category::all();
        $sizes = Size::all();
        $selected_sizes = $product->sizes()->pluck('id')->toArray();
        
        return view('admin.products.create', 
                ['product' => $product, 
                'languages' => $languages, 
                'currencies' => $currencies,
                'categories' => $categories,
                'sizes' => $sizes,
                'selected_sizes' => $selected_sizes]);
    }
    
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Product  $product
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $product = Product::find($id);

        if($product->photos()){
            $product->removePhotos();
        }
        
        $product->sizes()->detach();
        
        $product->delete();
        
        return redirect()->route('admin.products.index')->with('message', trans('admin.deleted'));
    }
    
    
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $products = Product::paginate(5);
        
        return view('admin.products.index', 
                ['products' => $products, 
                'currency' => Currency::getCurrency()]);
    }
}
