<?php

namespace App\Http\Controllers\Admin;

use App\Language;
use App\LanguageTranslation;
use App\Http\Controllers\Controller;
use App\Slider;
use App\SliderImage;
use App\SliderImageTranslation;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class SliderController extends Controller
{
    /**
     * @param int $page_id
     * @param int $slider_id
     * @return View
     */
    public function index(int $page_id, int $slider_id): View
    {
        $slider = Slider::with('images.translation')->findOrFail($slider_id);

        return view('admin.slider.index', [
            'slider' => $slider,
            'photoConfig' =>
                [
                    'width' => SliderImage::$photo_width,
                    'height' => SliderImage::$photo_height
                ]
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param int $slider_id
     * @return View
     */
    public function create(int $page_id, int $slider_id): View
    {
        $slider = Slider::find($slider_id);

        return view('admin.slider.create', [
                'slider' => $slider,
                'photoConfig' =>
                    [
                        'aspectRatio' => [
                            'width' => SliderImage::$ratio_width,
                            'height' => SliderImage::$ratio_height
                        ],
                        'width' => SliderImage::$photo_width,
                        'height' => SliderImage::$photo_height
                    ]
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $slider_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(int $page_id, int $slider_id, Request $request): RedirectResponse
    {
        $slider = Slider::findOrFail($slider_id);

        $languages = Language::all();
        $required_fields = ['name'];

        $error = false;
        $error_data = [];
        $translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob' => 'required',
                'photo_blob.*' => 'required|base64image|base64dimensions:min_width='
                    . SliderImage::$photo_width . ',min_height=' . SliderImage::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else {
                if ($request->photo_blob) {
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
                    $base_dir = 'slider';

                    if (!File::isDirectory(storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id)) {
                        File::makeDirectory(storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id, 493, true);
                    }
                    if (!File::isDirectory(storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR .
                        $slider_id . DIRECTORY_SEPARATOR . SliderImage::$photo_width . 'x' . SliderImage::$photo_height)) {
                        File::makeDirectory(storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id .
                            DIRECTORY_SEPARATOR . SliderImage::$photo_width . 'x' . SliderImage::$photo_height, 493, true);
                    }

                    $sequence = SliderImage::where('slider_id', $slider_id)->max('sequence');
                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $photo = new SliderImage();
                        $photo->slider_id = $slider_id;
                        $photo->sequence = ++$sequence;
                        $photo->active = $request->active ? 1 : null;
                        $data_image = $photo_blob;
                        $ext = explode('/', explode(';', $data_image)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR .
                            $slider_id . DIRECTORY_SEPARATOR . SliderImage::$photo_width . 'x' . SliderImage::$photo_height
                            . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(SliderImage::$photo_width, SliderImage::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(SliderImage::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id .
                                    DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }

                        $photo->photo = $photo_name;
                        $photo->save();

                        foreach ($translations as $language_id => $translation) {
                            $photo_translation = new SliderImageTranslation();
                            $photo_translation->slider_image_id = $photo->id;
                            $photo_translation->language_id = $language_id;
                            $photo_translation->name = $translation['name'];
                            $photo_translation->save();
                        }
                    }
                }

                return redirect()->route('admin.pages.slider.index', ['id' => $slider->element->page_id, 'slider_id' => $slider_id])->with('message', trans('admin.saved_changes'));
            }
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param int $slider_id
     * @param int $photo_id
     * @return View
     */
    public function edit(int $page_id, int $slider_id, int $photo_id): View
    {
        $photo = SliderImage::findOrFail($photo_id);
        $slider = Slider::findOrFail($slider_id);

        return view('admin.slider.edit', [
                'photo' => $photo,
                'slider' => $slider,
                'photoConfig' =>
                    [
                        'aspectRatio' => [
                            'width' => SliderImage::$ratio_width,
                            'height' => SliderImage::$ratio_height
                        ],
                        'width' => SliderImage::$photo_width,
                        'height' => SliderImage::$photo_height
                    ]
            ]
        );
    }

    /**
     * Update photo.
     *
     * @param int $page_id
     * @param int $slider_id
     * @param int $photo_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $page_id, int $slider_id, int $photo_id, Request $request): RedirectResponse
    {
        $slider = Slider::findOrFail($slider_id);

        $languages = Language::all();
        $required_fields = ['name'];

        $error = false;
        $error_data = [];
        $translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob.*' => 'base64image|base64dimensions:min_width='
                    . SliderImage::$photo_width . ',min_height=' . SliderImage::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else{
                $photo = SliderImage::find($photo_id);
                if($request->photo_blob){
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;
                    $base_dir = 'slider';

//                    remove old photo
                    $photo->removePhotos();

                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $ext = explode('/', explode(';', $photo_blob)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id . DIRECTORY_SEPARATOR .
                            SliderImage::$photo_width . 'x' . SliderImage::$photo_height . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(SliderImage::$photo_width, SliderImage::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(SliderImage::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . $base_dir . DIRECTORY_SEPARATOR . $slider_id .
                                    DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }

                        $photo->photo = $photo_name;
                    }
                }
                $photo->active = $request->active ? 1 : null;
                $photo->save();

                foreach($translations as $language_id=>$translation){
                    SliderImageTranslation::updateOrCreate(
                        ['slider_image_id' => $photo_id, 'language_id' => $language_id],
                        ['name' => $translation['name']]
                    );
                }
            }
            return redirect()->route('admin.pages.slider.index', ['id' => $slider->element->page_id, 'slider_id' => $slider->id])->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $page_id
     * @param int $slider_id
     * @param int $photo_id
     * @return RedirectResponse
     */
    public function destroy(int $page_id, int $slider_id, int $photo_id): RedirectResponse
    {
        $slider = Slider::findOrFail($slider_id);
        $photo = SliderImage::findOrFail($photo_id);

        if($photo->photo){
            $photo->removePhotos();
        }

        $photo->delete();

        return redirect()->route('admin.pages.slider.index', ['id' => $photo->slider->element->page_id, 'slider_id' => $slider->id])->with('message', trans('admin.deleted'));
    }

    /**
     * changes order of photos
     *
     * @param Request $request
     */
    public function changeOrder(int $page_id, int $slider_id, Request $request): void
    {
        $photos = SliderImage::where('slider_id', $slider_id)->orderBy('sequence', 'ASC')->get();
        foreach ($photos as $key =>$photo) {
            $photo->sequence = $request->post('order')[$key];
            $photo->save();
        }
    }
}
