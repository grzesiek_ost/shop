<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Language;
use App\Product;
use App\ProductPhoto;
use App\ProductPhotoTranslation;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;
use Illuminate\Support\Facades\File;

class ProductPhotosController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(int $product_id): View
    {
        $languages = Language::all();
        $product = Product::with('photos.translation')->findOrFail($product_id);

        return view('admin.productPhotos.index', [
            'product' => $product,
            'languages' => $languages,
            'photoConfig' =>
                [
                    'width' => ProductPhoto::$photo_width,
                    'height' => ProductPhoto::$photo_height
                ]
            ]
        );
    }

    /**
     * Show the form for creating a new resource.
     *
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|View
     */
    public function create(int $product_id): View
    {
        $languages = Language::all();
        $product = Product::find($product_id);
        
        return view('admin.productPhotos.create', [
            'product' => $product,
            'languages' => $languages,
            'photoConfig' =>
                [
                    'aspectRatio' => [
                        'width' => ProductPhoto::$ratio_width,
                        'height' => ProductPhoto::$ratio_height
                    ],
                    'width' => ProductPhoto::$photo_width,
                    'height' => ProductPhoto::$photo_height
                ]
            ]
        );
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param int $product_id
     * @param Request $request
     * @return \Illuminate\Http\RedirectResponse
     */
    public function store(int $product_id, Request $request): RedirectResponse
    {
        $languages = Language::all();
        $required_fields = ['name'];
        
        $error = false;
        $error_data = [];
        $translations = [];
        
//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob' => 'required',
                'photo_blob.*' => 'required|base64image|base64dimensions:min_width='
                    . ProductPhoto::$photo_width . ',min_height=' . ProductPhoto::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else {
                if ($request->photo_blob) {
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

                    if (!File::isDirectory(storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR . $product_id)) {
                        File::makeDirectory(storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR . $product_id, 493, true);
                    }
                    if (!File::isDirectory(storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR .
                        $product_id . DIRECTORY_SEPARATOR . ProductPhoto::$photo_width . 'x' . ProductPhoto::$photo_height)) {
                        File::makeDirectory(storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR . $product_id .
                            DIRECTORY_SEPARATOR . ProductPhoto::$photo_width . 'x' . ProductPhoto::$photo_height, 493, true);
                    }

                    $sequence = ProductPhoto::where('product_id', $product_id)->max('sequence');
                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $photo = new ProductPhoto();
                        $photo->product_id = $product_id;
                        $photo->sequence = ++$sequence;
                        $data_image = $photo_blob;
                        $ext = explode('/', explode(';', $data_image)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR .
                            $product_id . DIRECTORY_SEPARATOR . ProductPhoto::$photo_width . 'x' . ProductPhoto::$photo_height
                            . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(ProductPhoto::$photo_width, ProductPhoto::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(ProductPhoto::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR . $product_id .
                                    DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }

                        $photo->photo = $photo_name;
                        $photo->save();

                        foreach ($translations as $language_id => $translation) {
                            $photo_translation = new ProductPhotoTranslation();
                            $photo_translation->product_photo_id = $photo->id;
                            $photo_translation->language_id = $language_id;
                            $photo_translation->name = $translation['name'];
                            $photo_translation->save();
                        }
                    }
                }

                return redirect()->route('admin.products.photos.index', ['id' => $product_id])->with('message', trans('admin.saved_changes'));
            }
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Update photo.
     *
     * @param int $product_id
     * @param int $photo_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $product_id, int $photo_id, Request $request): RedirectResponse
    {
        $languages = Language::all();
        $required_fields = ['name'];

        $error = false;
        $error_data = [];
        $translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob.*' => 'base64image|base64dimensions:min_width='
                    . ProductPhoto::$photo_width . ',min_height=' . ProductPhoto::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else{
                if($request->photo_blob){
                    $photo = ProductPhoto::find($photo_id);
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

//                    remove old photo
                    $photo->removePhotos();

                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $ext = explode('/', explode(';', $photo_blob)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR .
                            $product_id . DIRECTORY_SEPARATOR . ProductPhoto::$photo_width . 'x' . ProductPhoto::$photo_height
                            . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(ProductPhoto::$photo_width, ProductPhoto::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(ProductPhoto::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . 'products' . DIRECTORY_SEPARATOR . $product_id .
                                    DIRECTORY_SEPARATOR . 'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }

                        $photo->photo = $photo_name;
                        $photo->save();
                    }
                }

                foreach($translations as $language_id=>$translation){
                    ProductPhotoTranslation::updateOrCreate(
                        ['product_photo_id' => $photo_id, 'language_id' => $language_id],
                        ['name' => $translation['name']]
                    );
                }
            }
            return redirect()->route('admin.products.photos.index', ['id' => $product_id])->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $product_id
     * @param $photo_id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit(int $product_id, int $photo_id): View
    {
        $languages = Language::all();
        $photo = ProductPhoto::findOrFail($photo_id);
        $product = Product::findOrFail($product_id);
        
        return view('admin.productPhotos.edit', [
            'product_photo' => $photo,
            'languages' => $languages,
            'product' => $product,
            'photoConfig' =>
                [
                    'aspectRatio' => [
                        'width' => ProductPhoto::$ratio_width,
                        'height' => ProductPhoto::$ratio_height
                    ],
                    'width' => ProductPhoto::$photo_width,
                    'height' => ProductPhoto::$photo_height
                ]
            ]
        );
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $product_id
     * @param $photo_id
     * @return RedirectResponse
     */
    public function destroy(int $product_id, int $photo_id): RedirectResponse
    {
        $photo = ProductPhoto::findOrFail($photo_id);
        
        if($photo->photo){
            $photo->removePhotos();
        }
        
        $photo->delete();
        
        return redirect()->route('admin.products.photos.index', ['id' => $product_id])->with('message', trans('admin.deleted'));
    }

    /**
     * changes order of photos
     *
     * @param Request $request
     */
    public function changeOrder(int $product_id, Request $request): void
    {
        $products = ProductPhoto::where('product_id', $product_id)->orderBy('sequence', 'ASC')->get();
        foreach ($products as $key =>$product) {
            $product->sequence = $request->post('order')[$key];
            $product->save();
        }
    }
}
