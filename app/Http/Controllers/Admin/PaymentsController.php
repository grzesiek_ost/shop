<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Payment;
use App\PaymentTranslation;
use App\Language;

class PaymentsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $payments = Payment::orderBy('sequence', 'ASC')->paginate(5);
        
        return view('admin.payments.index', 
                ['payments' => $payments]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $languages = Language::all();
        
        return view('admin.payments.create', 
                ['languages' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $required_fields = ['name'];
        $translatable_fields = ['name', 'description'];
        
        $error = false;
        $error_data = [];
        $payment_translations = [];
        $payment_id = $request->post('id') ?? false;
                
        foreach ($languages as $language){
            foreach($translatable_fields as $field){
                //        check if all required fields were filled
                if(!($request->post($field)[$language->id]) && in_array($field, $required_fields)){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $payment_translations[$language->id][$field] = $request->post($field)[$language->id];
                }
            }
        }
                
        if(!$error){
//            creating new payment
            if($payment_id){
                $payment = Payment::find($payment_id);
//                remove old logo
                if($request->post('delete_photo')){
                    $payment->removeLogo();
                }
            }
            else{
                $payment = new Payment;
            }
                        
            $active = $request->post('active') ? 1 : 0;
            $payment->active = $active;
            $payment->save();
                        
//            saving logo
            if ($request->hasFile('photo')){
                
//              remove old logo
                $payment->removeLogo();
                
                $request->validate([
                    'photo' => 'image|file|max:1024',
                ]);
                
                $file_name = time().'.'.$request->photo->getClientOriginalExtension();

                $photo_file = $request->file('photo');
                
                $photo_file->storeAs(
                    'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR . 'payments' .
                    DIRECTORY_SEPARATOR . $payment->id . DIRECTORY_SEPARATOR . 'original',
                    $file_name
                );
                
                $payment->logo = $file_name;
                $payment->save();
            }
            
//            saving payment translations
            foreach($payment_translations as $language_id=>$translation){
                if($payment_id){
                    $payment_translation = PaymentTranslation::where('language_id', $language_id)
                            ->where('payment_id', $payment_id)
                            ->first();
                    if(! $payment_translation){
                        $payment_translation = new PaymentTranslation;
                    }
                }
                else{
                    $payment_translation = new PaymentTranslation;
                }
                $payment_translation->payment_id = $payment->id;
                $payment_translation->language_id = $language_id;
                foreach ($translatable_fields as $field){
                    $payment_translation->$field = $translation[$field];
                }
                $payment_translation->save();
            }
                        
            return redirect()->route('admin.payments.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();
        $payment = Payment::find($id);
        
        return view('admin.payments.create', 
                ['payment' => $payment, 
                'languages' => $languages]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $payment = Payment::findOrFail($id);
        
        $payment->removeLogo();
        
        $payment->delete();
        
        return redirect()->route('admin.payments.index')->with('message', trans('admin.deleted'));
    }
    
    /**
     * changes order of payments
     */
    public function changeOrder(Request $request) {
        $payments = Payment::orderBy('sequence', 'ASC')->get();
        foreach ($payments as $key =>$payment) {
            $payment->sequence = $request->post('order')[$key];
            $payment->save();
        }
    }
}
