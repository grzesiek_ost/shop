<?php

namespace App\Http\Controllers\Admin;

use App\Order;
use App\Http\Controllers\Controller;

class OrdersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {        
        $orders = Order::orderBy('created_at', 'DESC')->Paginate(5);
        
        return view('admin.orders.index', [
            'orders' => $orders
        ]);
    }
    
    /**
     * Remove the specified resource from storage.
     * @param type $id
     * @return type
     */
    public function destroy($id)
    {
        $order = Order::findOrFail($id);
        $order->delete();
        
        return redirect()->route('admin.orders.index')->with('message', trans('admin.deleted'));
    }
}
