<?php

namespace App\Http\Controllers\Admin;

use App\Language;
use App\LanguageTranslation;
use App\Http\Controllers\Controller;
use App\Page;
use App\Slider;
use App\Wysiwyg;
use App\WysiwygTranslation;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class PagesController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        return view('admin.pages.index', [
            'pages' => Page::paginate(5)
        ]);
    }

    /**
     * @param int $page_id
     * @param int $wysiwyg_id
     * @return View
     */
    public function wysiwyg(int $page_id, int $wysiwyg_id): View
    {
        $wysiwyg = Wysiwyg::findOrFail($wysiwyg_id);

        return view('admin.pages.wysiwyg', [
            'wysiwyg' => $wysiwyg
        ]);
    }

    /**
     * @param Request $request
     * @param int $page_id
     * @param int $wysiwyg_id
     * @return RedirectResponse
     */
    public function updateWysiwyg(Request $request, int $page_id, int $wysiwyg_id): RedirectResponse
    {
        $wysiwig = Wysiwyg::findOrFail($wysiwyg_id);
        $languages = Language::all();
        $required_fields = ['name', 'content'];

        $error = false;
        $error_data = [];
        $translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            foreach($translations as $language_id=>$translation){
                WysiwygTranslation::updateOrCreate(
                    ['wysiwyg_id' => $wysiwig->id, 'language_id' => $language_id],
                    ['name' => $translation['name'], 'content' => $translation['content']]
                );
            }
            return redirect()->route('admin.pages.wysiwyg.index', ['id' => $page_id, 'wysiwyg_id' => $wysiwig->id])
                ->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }
}
