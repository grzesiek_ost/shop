<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Product;
use App\Quantity;

class QuantitiesController extends Controller
{
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, $id)
    {
        if($request->filled('size')){
            $size_changes = $request->post('size');
            foreach($size_changes as $size => $changes){
                if($changes != 0){
                    $quantity = new Quantity;
                    $quantity->size_id = $size;
                    $quantity->product_id = $id;
                    $quantity->quantity = $changes;
                    $quantity->comment = 'Changed by '.auth()->user()->name.' '.auth()->user()->surname;
                    $quantity->user_id = auth()->user()->id;
                    $quantity->save();
                }
            }
        }
        
        return redirect()->back()->with('message', trans('admin.saved_changes'));
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $product = Product::findOrFail($id);
        
        return view('admin.quantities.show', ['product' => $product]);
    }
}
