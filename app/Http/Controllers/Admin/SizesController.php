<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Size;
use App\SizeTranslation;
use App\Language;

class SizesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $sizes = Size::Paginate(5);
        $languages = Language::all();
        
        return view('admin.sizes.index', ['sizes' => $sizes, 'languages' => $languages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $languages = Language::all();
        
        return view('admin.sizes.create', ['languages' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $required_fields = ['name'];
        
        $error = false;
        $error_data = [];
        $size_translations = [];
        $size_id = htmlspecialchars($request->post('id')) ?? false;
        
//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $size_translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }       
            }
        }
        
        if(!$error){
//            creating new size
            if($size_id){
                $size = Size::find($size_id);
            }
            else{
                $size = new Size;
            }
            
            if($request->post('default_size')){
                Size::whereNotNull('default_size')->update(['default_size' => null]);
            }
            $size->default_size = $request->filled('default_size') ? 1 : 0;
            $size->save();
            
//            saving size translations
            foreach($size_translations as $language_id=>$translation){
                if($size_id){
                    $size_translation = SizeTranslation::where('language_id', $language_id)
                            ->where('size_id', $size_id)
                            ->first();
                    if(! $size_translation){
                        $size_translation = new SizeTranslation;
                    }
                }
                else{
                    $size_translation = new SizeTranslation;
                }
                $size_translation->size_id = $size->id;
                $size_translation->language_id = $language_id;
                foreach ($required_fields as $field){
                    $size_translation->$field = $translation[$field];
                }
                $size_translation->save();
            }
            
            return redirect()->route('admin.sizes.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();
        $size = Size::findOrFail($id);
        
        return view('admin.sizes.create', 
                ['size' => $size, 
                'languages' => $languages]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $size = Size::findOrFail($id);
        
        $size->products()->detach();
        
        $size->delete();
        
        return redirect()->route('admin.sizes.index')->with('message', trans('admin.deleted'));
    }
}
