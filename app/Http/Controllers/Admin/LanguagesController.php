<?php

namespace App\Http\Controllers\Admin;

use App\Http\Requests\LanguageRequest;
use App\Language;
use App\LanguageTranslation;
use App\Http\Controllers\Controller;
use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\View\View;

class LanguagesController extends Controller
{
    /**
     * @param Request $request
     * @return View
     */
    public function index(Request $request): View
    {
        return view('admin.languages.index', [
            'languages' => Language::all()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        return view('admin.languages.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param LanguageRequest $request
     * @return RedirectResponse
     */
    public function store(LanguageRequest $request): RedirectResponse
    {
        $language = new Language();
        $language->name = $request->post('name');
        $language->native_name = $request->post('native_name');
        $language->shortcut = $request->post('shortcut');
        $language->active = $request->post('active') ? 1 : 0;
        $language->save();

        return redirect()->route('admin.languages.index')->with('message', trans('admin.saved_changes'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return View
     */
    public function edit($id): View
    {
        $language = Language::find($id);

        return view('admin.languages.edit', [
            'language' => $language
        ]);
    }

    /**
     * @param $id
     * @param LanguageRequest $request
     * @return RedirectResponse
     */
    public function update($id, LanguageRequest $request): RedirectResponse
    {
        $language = Language::findOrFail($id);
        $language->name = $request->post('name');
        $language->native_name = $request->post('native_name');
        $language->shortcut = $request->post('shortcut');
        $language->active = $request->post('active') ? 1 : 0;
        $language->save();

        return redirect()->route('admin.languages.index')->with('message', trans('admin.saved_changes'));
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return RedirectResponse
     */
    public function destroy($id): RedirectResponse
    {
        $language = Language::findOrFail($id);
        $language->delete();

        return redirect()->route('admin.languages.index')->with('message', trans('admin.deleted'));
    }
}
