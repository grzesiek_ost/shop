<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Status;
use App\StatusTranslation;
use App\Language;

class StatusesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $statuses = Status::Paginate(5);
        $languages = Language::all();
        
        return view('admin.statuses.index', ['statuses' => $statuses, 'languages' => $languages]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {        
        $languages = Language::all();
        
        return view('admin.statuses.create', ['languages' => $languages]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $languages = Language::all();
        $required_fields = ['name'];
        
        $error = false;
        $error_data = [];
        $status_translations = [];
        $status_id = htmlspecialchars($request->post('id')) ?? false;
        
//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $status_translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }       
            }
        }
        
        if(!$error){
//            creating new status
            if($status_id){
                $status = Status::find($status_id);
            }
            else{
                $status = new Status;
            }
            
            $status->save();
            
//            saving status translations
            foreach($status_translations as $language_id=>$translation){
                if($status_id){
                    $status_translation = StatusTranslation::where('language_id', $language_id)
                            ->where('status_id', $status_id)
                            ->first();
                    if($status_translation->count() < 1){
                        $status_translation = new StatusTranslation;
                    }
                }
                else{
                    $status_translation = new StatusTranslation;
                }
                $status_translation->status_id = $status->id;
                $status_translation->language_id = $language_id;
                foreach ($required_fields as $field){
                    $status_translation->$field = $translation[$field];
                    
                    if($field == 'name'){
                        $status_translation->slug = Str::slug($translation[$field], '-');
                    }
                }
                $status_translation->save();
            }
            
            return redirect()->route('admin.statuses.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $languages = Language::all();
        $status = Status::findOrFail($id);
        
        return view('admin.statuses.create', 
                ['status' => $status, 
                'languages' => $languages]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::findOrFail($id);
        
        $status->delete();
        
        return redirect()->route('admin.statuses.index')->with('message', trans('admin.deleted'));
    }
}
