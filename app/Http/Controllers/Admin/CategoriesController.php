<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\RedirectResponse;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use App\Http\Controllers\Controller;
use App\Category;
use App\CategoryTranslation;
use App\Language;
use Illuminate\View\View;
use Intervention\Image\Facades\Image;

class CategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return View
     */
    public function index(): View
    {
        $categories = Category::whereNull('category_id')->get();
        
        return view('admin.categories.index', ['categories' => $categories]);
    
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return View
     */
    public function create(): View
    {
        $languages = Language::all();
        $categories = Category::all();
        
        return view('admin.categories.create', [
            'languages' => $languages,
            'categories' => $categories,
            'photoConfig' => [
                'aspectRatio' => [
                    'width' => Category::$ratio_width,
                    'height' => Category::$ratio_height
                ],
                'width' => Category::$photo_width,
                'height' => Category::$photo_height
            ]
        ]);
    }

    /**
     * Save new category.
     *
     * @param Request $request
     * @return RedirectResponse
     */
    public function store(Request $request): RedirectResponse
    {
        $request->validate([
            'category' => 'integer|nullable'
        ]);

        $languages = Language::all();
        $required_fields = ['name'];

        $error = false;
        $error_data = [];
        $category_translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $category_translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob' => 'required',
                'photo_blob.*' => 'required|base64image|base64dimensions:min_width='
                    . Category::$photo_width . ',min_height=' . Category::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else {
                $category = new Category();
                if ($request->photo_blob) {
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

                    if (!File::isDirectory(storage_path() . $base_path . 'categories')) {
                        File::makeDirectory(storage_path() . $base_path . 'categories', 493, true);
                    }
                    if (!File::isDirectory(storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                        Category::$photo_width . 'x' . Category::$photo_height)) {
                        File::makeDirectory(storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                            Category::$photo_width . 'x' . Category::$photo_height, 493, true);
                    }

                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $ext = explode('/', explode(';', $photo_blob)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                            Category::$photo_width . 'x' . Category::$photo_height
                            . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(Category::$photo_width, Category::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(Category::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                                    'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }
                        $category->photo = $photo_name;
                    }
                }

                $category->category_id = $request->post('category') ?? null;
                $category->save();

//                saving category translations
                foreach($category_translations as $language_id=>$translation){
                    $category_translation = new CategoryTranslation();
                    $category_translation->language_id = $language_id;
                    $category_translation->category_id = $category->id;
                    foreach ($required_fields as $field){
                        $category_translation->$field = $translation[$field];
                        if($field == 'name'){
                            $category_translation->slug =
                                $category->getParentCategory() ?
                                    $category->getParentCategory()->makeSlug($language_id).'-'.Str::slug($translation[$field]):
                                    Str::slug($translation[$field]);
                        }
                    }
                    $category_translation->save();
                }
                return redirect()->route('admin.categories.index')->with('message', trans('admin.saved_changes'));
            }
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param $id
     * @return View
     */
    public function edit(int $id): View
    {
        $languages = Language::all();
        $category = Category::find($id);
        $categories = Category::all();
        
        return view('admin.categories.edit', [
            'category' => $category, 
            'categories' => $categories, 
            'languages' => $languages,
            'photoConfig' => [
                'aspectRatio' => [
                    'width' => Category::$ratio_width,
                    'height' => Category::$ratio_height
                ],
                'width' => Category::$photo_width,
                'height' => Category::$photo_height
            ]
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param int $category_id
     * @param Request $request
     * @return RedirectResponse
     */
    public function update(int $category_id, Request $request): RedirectResponse
    {
        $request->validate([
            'category' => 'integer|nullable'
        ]);

        $languages = Language::all();
        $required_fields = ['name'];

        $error = false;
        $error_data = [];
        $category_translations = [];

//        check if all required fields were filled
        foreach($required_fields as $field){
            foreach ($languages as $language){
                if(!($request->post($field)[$language->id])){
                    $error = true;
                    $error_data[$field][$language->id] = 'error';
                }
                else{
                    $category_translations[$language->id][$field] = htmlspecialchars($request->post($field)[$language->id]);
                }
            }
        }

        if(!$error){
            $validator = Validator::make($request->all(), [
                'photo_blob.*' => 'base64image|base64dimensions:min_width='
                    . Category::$photo_width . ',min_height=' . Category::$photo_height
            ]);

            if($validator->fails()){
                return back()->withInput()->withErrors($validator);
            }
            else{
                $category = Category::findOrFail($category_id);
                if($request->photo_blob){
                    $base_path = DIRECTORY_SEPARATOR . 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' . DIRECTORY_SEPARATOR;

//                    remove old photo
                    $category->removePhotos();

                    foreach ($request->photo_blob as $key => $photo_blob) {
                        $ext = explode('/', explode(';', $photo_blob)[0])[1];
                        $photo_name = time() . Str::random(5) . '.' . $ext;
                        $path = storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                            Category::$photo_width . 'x' . Category::$photo_height . DIRECTORY_SEPARATOR . $photo_name;

                        Image::make(file_get_contents($photo_blob))->resize(Category::$photo_width, Category::$photo_height)->save($path);

                        $temp_file_path = storage_path() . $base_path . 'temp' . DIRECTORY_SEPARATOR . $request->photo_original_file_name[$key];

                        if (File::exists($temp_file_path)) {
                            if(Category::$keep_original) {
                                $orig_file_path = storage_path() . $base_path . 'categories' . DIRECTORY_SEPARATOR .
                                    'original' . DIRECTORY_SEPARATOR . $photo_name;
                                File::move($temp_file_path, $orig_file_path);
                            }
                            else{
                                File::delete($temp_file_path);
                            }
                        }

                        $category->photo = $photo_name;
                    }
                }
                $category->category_id = $request->post('category') ?? null;
                $category->save();
            }

//            saving category translations
            foreach($category_translations as $language_id=>$translation){

                $category_translation = CategoryTranslation::where('language_id', $language_id)
                        ->where('category_id', $category_id)
                        ->first();
                if(!$category_translation){
                    $category_translation = new CategoryTranslation();
                }

                $category_translation->language_id = $language_id;
                $category_translation->category_id = $category->id;
                foreach ($required_fields as $field){
                    $category_translation->$field = $translation[$field];
                    if($field == 'name'){
                        $category_translation->slug =
                                $category->getParentCategory() ?
                                $category->getParentCategory()->makeSlug($language_id) . '-' . Str::slug($translation[$field]):
                                Str::slug($translation[$field]);
                    }
                }
                $category_translation->save();
            }

            return redirect()->route('admin.categories.index')->with('message', trans('admin.saved_changes'));
        }
        else{
            return back()->withInput()->with('error', $error)->with('error_data', $error_data);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param int $id
     * @return RedirectResponse
     */
    public function destroy(int $id): RedirectResponse
    {
        $category = Category::findOrFail($id);
        
        if($category->photo){
            $category->removePhotos();
        }
        
        $category->delete();
        
        return redirect()->route('admin.categories.index')->with('message', trans('admin.deleted'));
    }

    /**
     * return subcategories list or false | ajax request
     *
     * @param int $id
     * @return bool|\Illuminate\Contracts\View\Factory|View
     */
    public function showSubcategories(int $id) {
        $subcategories = Category::where('category_id', $id)->get();
        
        if($subcategories){
            return view('admin.categories.row', ['categories' => $subcategories]);
        }
        else{
            return false;
        }
    }
}
