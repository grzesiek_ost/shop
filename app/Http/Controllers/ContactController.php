<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use App\Mail\Contact;

class ContactController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('contact.index');
    }

    /**
     * send mail
     * @param Request $request
     * @return type
     */
    public function send(Request $request)
    {
        $request->validate([
            'name' => 'required|min:2|max:100',
            'email' => 'required|email|max:100',
            'phone' => 'nullable|min:7|max:15',
            'message' => 'required|min:1|max:5000',
            'g-recaptcha-response' => 'required'
        ]);
        
        $google_url = 'https://www.google.com/recaptcha/api/siteverify';
        $query = http_build_query([
            'secret' => env('GOOGLE_RECAPTCHA_SECRET_KEY'), 
            'response' => $request['g-recaptcha-response'],
            'remoteip' => $_SERVER['REMOTE_ADDR']
        ]);
        
        $ch = curl_init();  
        curl_setopt($ch, CURLOPT_URL, $google_url);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, 
           ['Content-Type: application/x-www-form-urlencoded; charset=utf-8', 
           'Content-Length: ' . strlen($query)]);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $query); 
        $googresp = curl_exec($ch);       
        $response = json_decode($googresp);
        curl_close($ch);
        
        if($response->success){
            Mail::to(env('MAIL_USERNAME'))->send(new Contact($request));
            return redirect()->back()->with(['message' => trans('mails.mail_send')]);
        }
        else{
            return redirect()->back()->withInput($request->input())->with(['message' => trans('mails.mail_error')]);
        }
    }
}
