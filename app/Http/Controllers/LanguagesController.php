<?php

namespace App\Http\Controllers;

use App\Language;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Cache;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\App;

class LanguagesController extends Controller
{    
    /**
     * Changes language.
     * 
     * @param Request $request
     * @return type
     */
    public function changeLanguage(Request $request) {
        $language = Language::findOrFail(request()->language);
        Session::put('language', request()->language);
        App::setLocale($language->shortcut);
        Cache::put('language', $language);
        return redirect()->back();
    }
}
