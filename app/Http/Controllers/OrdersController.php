<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\Delivery;
use App\Payment;
use App\Order;
use App\OrderData;
use App\OrderItem;
use App\Currency;
use App\Size;
use App\Product;
use App\Quantity;
use App\Language;
use App\Mail\OrderSubmit;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Mail;

class OrdersController extends Controller
{
    /**
     * finalizing the purchase 
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(Order::getCartProducts($request)->count()){
            $validatedData = $request->validate([
                'payment' => 'required|integer',
                'delivery' => 'required|integer',
                'name' => 'required|string|max:191',
                'surname' => 'required|string|max:191',
                'email' => 'required|email|min:6|max:191',
                'phone' => 'required|string|min:7|max:50',
                'city' => 'required|string|max:100',
                'street' => 'required|string|max:100',
                'house_number' => 'required|string|max:10',
                'apartment_number' => 'string|nullable|max:10',
                'zip_code' => 'required|string|max:10'
            ]);

            $payment = Payment::findOrFail($request->post('payment'));
            $delivery = Delivery::findOrFail($request->post('delivery'));
            $language = Language::getLanguage();

    //        save order
            $order = new Order;
            $order->user_id = auth()->user()->id ?? null;
            $order->currency = Currency::getCurrency()->shortcut;
            $order->currency_id = Currency::getCurrency()->id;
            $order->language = Language::getLanguage()->shortcut;
            $order->language_id = Language::getLanguage()->id;
            $order->payment = $payment->getData()->name;
            $order->payment_id = $request->post('payment');
            $order->delivery = $delivery->getData()->name;
            $order->delivery_id = $request->post('delivery');
            $order->delivery_price = $delivery->getPrice()->price;
            $order->save();

            $order_data = new OrderData;
            $order_data->order_id = $order->id;
            $order_data->name = $request->post('name');
            $order_data->surname = $request->post('surname');
            $order_data->email = $request->post('email');
            $order_data->phone = $request->post('phone');
            $order_data->city = $request->post('city');
            $order_data->street = $request->post('street');
            $order_data->house_number = $request->post('house_number');
            $order_data->apartment_number = $request->post('apartment_number') ?? null;
            $order_data->zip_code = $request->post('zip_code');
            $order_data->save();

            $cart = $request->session()->get('cart');
            foreach($cart as $key => $value){
                $product = Product::findOrFail($value['product_id']);
                $size = Size::findOrFail($value['size']);

    //            save product in order
                $order_item = new OrderItem;
                $order_item->size_id = $value['size'];
                $order_item->product_id = $value['product_id'];
                $order_item->order_id = $order->id;
                $order_item->size = $size->getData()->name;
                $order_item->product = $product->getProductData()->name;
                $order_item->count = $value['quantity'];
                $order_item->item_price = $product->getPrice()->price;
                $order_item->save();

    //            change in the product inventory
                $quantity = new Quantity;
                $quantity->size_id = $value['size'];
                $quantity->product_id = $value['product_id'];
                $quantity->quantity = -($value['quantity']);
                $quantity->comment = 'During order '.$order->id;
                $quantity->save();
            }

    //        send email to user
            Mail::to($order->data->email)->send(new OrderSubmit($order));

            $request->session()->forget('cart');

            return redirect()->route('order.finalization');
        }
        else{
            return redirect()->route('cart.index');
        }
    }

    
    /**
     * 
     * show shipping info form during order
     * @return \Illuminate\Http\Response
     */
    public function shippingInfo(Request $request)
    {
        if(Order::getCartProducts($request)->count()){
            $user = Auth::user();
            
            return view('order.shipping_info', ['user' => $user]);
        }
        else{
            return redirect()->route('cart.index');
        }
    }
    
    
    /**
     * back to page with shipping info
     * show shipping info form during order
     * @return \Illuminate\Http\Response
     */
    public function backToShippingInfo(Request $request)
    {
        if(Order::getCartProducts($request)->count()){
            $user = Auth::user();
            
            return view('order.shipping_info', ['user' => $user, 'post' => $request->except('_token')]);
        }
        else{
            return redirect()->route('cart.index');
        }
    }
    
    /**
     * back to delivery page
     * @param Request $request
     * @return type
     */
    public function backToDelivery(Request $request)
    {
        if ($request->isMethod('post')) {
            $user = Auth::user();

            $deliveries = Delivery::where('active', 1)->orderBy('sequence', 'ASC')->get();

            return view('order.delivery', ['deliveries' => $deliveries, 'post' => $request->except('_token')]);
        }
        else{
            return redirect()->route('cart.index');
        }
    }
    
    /**
     * back to payment page
     * @param Request $request
     * @return type
     */
    public function backToPayment(Request $request)
    {
        if ($request->isMethod('post')) {
            $payments = Payment::where('active', 1)->orderBy('sequence', 'ASC')->get();
            return view('order.payment', ['payments' => $payments, 'post' => $request->except('_token')]);
        }
        else{
            return redirect()->route('cart.index');  
        }
    }
    
    /**
     * validate shipping informations and redirect to delivery page
     * @param Request $request
     */
    public function delivery(Request $request)
    {        
        if ($request->isMethod('post')) {
            if(Order::getCartProducts($request)->count()){
                $input = $request->post();
                $validator = Validator::make($input,
                    [
                        'name' => 'required|string|max:191',
                        'surname' => 'required|string|max:191',
                        'email' => 'required|email|min:6|max:191',
                        'phone' => 'string|nullable|min:7|max:50',
                        'street' => 'string|nullable|max:100',
                        'house_number' => 'string|nullable|max:10',
                        'apartment_number' => 'string|nullable|max:10',
                        'zip_code' => 'string|nullable|max:10',
                        'city' => 'string|nullable|max:100'
                    ]
                );

                if ($validator->fails()) {
                    $user = Auth::user();
                    return back()
                            ->withErrors($validator)
                            ->withInput($request->all)
                            ->with('user', $user);
                }
                else {
                    $deliveries = Delivery::where('active', 1)->orderBy('sequence', 'ASC')->get();
                    return view('order.delivery', ['deliveries' => $deliveries, 'post' => $request->except('_token')]);
                }
            }
        }
        return redirect()->route('cart.index');    
    }
    
    /**
     * 
     * show payment during order
     * @return \Illuminate\Http\Response
     */
    public function payment(Request $request)
    {
        if(Order::getCartProducts($request)->count()){
            $input = $request->post();
            $validator = Validator::make($input,
                [
                    'delivery' => 'required'
                ]
            );

            if ($validator->fails()) {
                $deliveries = Delivery::where('active', 1)->orderBy('sequence', 'ASC')->get();
                return back()
                        ->withErrors($validator)
                        ->withInput($request->all)
                        ->with('deliveries', $deliveries);
            }
            else {
                $payments = Payment::where('active', 1)->orderBy('sequence', 'ASC')->get();
                return view('order.payment', ['payments' => $payments, 'post' => $request->except('_token')]);
            }
        }
        else{
            return redirect()->route('cart.index');
        }
    }
    
    /**
     * show summary page
     * @param Request $request
     * @return type
     */
    public function summary(Request $request)
    {
        if ($request->isMethod('post') && Order::getCartProducts($request)->count()) {
            $payment = Payment::findOrFail($request->post('payment'));
            $delivery = Delivery::findOrFail($request->post('delivery'));
            return view('order.summary', [
                'post' => $request->except('_token', 'payment', 'delivery'), 
                'products' => Order::getCartProducts($request),
                'currency' => Currency::getCurrency(),
                'payment' => $payment,
                'delivery' => $delivery,
                'cart_sum' => Order::getCartSum($request)
            ]);
        }
        else{
            return redirect()->route('cart.index');
        }
    }
    
    /**
     * show finalization page
     * @param Request $request
     * @return type
     */
    public function finalization(Request $request) 
    {
        return view('order.finalization');
    }
}
