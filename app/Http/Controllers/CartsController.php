<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Product;
use App\Currency;
use App\Order;

class CartsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        return view('cart.index', ['products' => Order::getCartProducts($request), 'currency' => Currency::getCurrency()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'size_id' => 'required|integer',
            'product_id' => 'required|integer',
            'quantity' => 'required|integer'
        ]);
        
        $size = $request->post('size_id');
        $product = $request->post('product_id');
        $quantity = $request->post('quantity');
        $added_product = false;

        if ($request->session()->has('cart')){
            foreach($request->session()->get('cart') as $key => $value){
                if($value['product_id'] == $product && $value['size'] == $size){
                    $available_quantity = Product::where('active', 1)->where('id', $product)->first()->sizeQuantity($value['size']);
                    $existing_items = $request->session()->get('cart.'.$key.'.quantity');
                    $wanted_quantity = (int)$existing_items + (int)$quantity;
                    $quantity = $wanted_quantity < $available_quantity ?
                                $wanted_quantity :
                                $available_quantity;

                    $request->session()->put('cart.'.$key.'.quantity', $quantity);
                    $added_product = true;
                    break;
                }
            }
        }
        else{
            $request->session()->put('cart');
        }
        
        if(!$added_product){
            $new_product = ['size' => $size, 'product_id' => $product, 'quantity' => $quantity];
            $request->session()->push('cart', $new_product);
        }
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request)
    {
        $request->validate([
            'size_id' => 'required|integer',
            'product_id' => 'required|integer'
        ]);
        
        $product = $request->post('product_id');
        $size = $request->post('size_id');
        
        if ($request->session()->has('cart')){
            foreach($request->session()->get('cart') as $key => $value){
                if($value['product_id'] == $product && $value['size'] == $size){
                    $request->session()->forget('cart.'.$key);
                    break;
                }
            }
        }
    }
    
    /**
     * return to cart
     * @param Request $request
     * @return type
     */
    public function backToCart(Request $request)
    {        
        return view('cart.index', [
            'products' => Order::getCartProducts($request), 
            'currency' => Currency::getCurrency(),
            'post' => $request->except('_token')
        ]);
    }
}
