<?php

namespace App\Http\Controllers;

use App\Category;
use App\CategoryTranslation;
use App\Language;
use App\Product;
use App\Currency;
use Illuminate\Http\Request;

class CategoriesController extends Controller
{
    /**
     * display products from selected category
     * @param type $category_slug
     * @return type
     */
    public function products($category_slug)
    {
        $language = Language::getLanguage();
        $category_translation = CategoryTranslation::where('language_id', $language->id)->where('slug', $category_slug)->first();
        
        if(!$category_translation){
            $category_translation = CategoryTranslation::where('slug', $category_slug)->first();
        }
        
        if($category_translation){
            $category = Category::findOrFail($category_translation->category_id);
            $products = $this->productList($category_slug);

            return view('products.index', [
                'products' => $products,
                'category' => $category,
                'currency' => Currency::getCurrency()
            ]);
        }
        else{
            return redirect()->route('home_page');
        }
    }

    /**
     * Display main shop page with main categories
     * @return type
     */
    public function categories()
    {
        $categories = Category::whereNull('category_id')->get();
        
        return view('categories.index', ['categories' => $categories]);
    }

    /**
     * @param Request $request
     * @param string $category_slug
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function search(Request $request, string $category_slug)
    {
        $language = Language::getLanguage();
        $category_translation = CategoryTranslation::where('language_id', $language->id)->where('slug', $category_slug)->first();

        if(!$category_translation){
            $category_translation = CategoryTranslation::where('slug', $category_slug)->first();
        }

        $category = Category::findOrFail($category_translation->category_id);
        return view('products.index', [
            'products' => $this->productList($category_slug, $request),
            'category' => $category,
            'currency' => Currency::getCurrency()
        ]);
    }

    /**
     * @param string $category_slug
     * @param Request|null $request
     * @return \Illuminate\Database\Eloquent\Builder
     */
    public function productList(string $category_slug, Request $request = null)
    {
        $language = Language::getLanguage();
        $category_translation = CategoryTranslation::where('language_id', $language->id)->where('slug', $category_slug)->first();

        if(!$category_translation){
            $category_translation = CategoryTranslation::where('slug', $category_slug)->first();
        }
        $category = Category::findOrFail($category_translation->category_id);

        $products = Product::query();
        $products = $products->select(['products.id', 'products.created_at', 'products.category_id',
            'product_translations.name', 'product_translations.description', 'product_translations.language_id']);
        $products = $products->join('product_translations', function ($join) use ($language, $request) {
            $join->on('products.id', '=', 'product_translations.product_id');
            $join->where('product_translations.language_id', '=', $language->id);
            if($request) {
                $join->where(function($query) use ($request) {
                    $query->where('product_translations.name', 'LIKE', '%' . $request->search_text . '%')
                        ->orWhere('product_translations.description', 'LIKE', '%' . $request->search_text . '%');
                });
            }
        });

        $products = $products->whereIn('category_id', $category->getThisAndSubCategoriesId());
        $products = $products->where('active', 1);
        $products = $products->orderBy('products.id', 'ASC');
        $products = $products->paginate(10);

        if($request) {
            $products = $products->appends(array(
                'search_text' => $request->get('search_text')
            ));
        }

        return $products;
    }
}
