<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;
use Illuminate\Support\Facades\File;

class Category extends Model
{
    protected $table = 'categories';

    protected $fillable = [
        'category_id',
        'photo'
    ];

    public static $photo_width = 640;
    public static $photo_height = 480;
    public static $ratio_height = 3;
    public static $ratio_width = 4;
    public static $keep_original = false;

    /**
     * relation to translation
     * @return type
     */
    public function translation() {
        return $this->hasMany('App\CategoryTranslation');
    }
    
    /**
     * get category data in selected language
     * @param type $language_id
     * @return type
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
    
    /**
     * removes photos
     */
    public function removePhotos() {
        if($this->photo){
            $base_path = 'app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR . 'images' .
                DIRECTORY_SEPARATOR . 'categories' . DIRECTORY_SEPARATOR;
            $image_path = storage_path($base_path . Category::$photo_width . 'x' . Category::$photo_height . DIRECTORY_SEPARATOR . $this->photo);
            $original_image_path = storage_path($base_path . 'original' . DIRECTORY_SEPARATOR . $this->photo);

            $files = array($image_path, $original_image_path);

            foreach ($files as $file) {
                File::delete($file);
            }
            
            $this->photo = null;
        }
    }
    
    /**
     * return parent category
     * @return type
     */
    public function getParentCategory() {
        return $this->category_id ? Category::where('id', $this->category_id)->first() : false;
    }
    
    /**
     * return subcategories
     * @return type
     */
    public function getSubCategories() {
        return Category::where('category_id', $this->id)->get();
    }
    
    /**
     * return this id and all subcategories id 
     * @param type $sub_arr
     * @return type
     */
    public function getThisAndSubCategoriesId($sub_arr = false) {
        if(!$sub_arr){
            $sub_arr = []; 
        }
        
        if(!in_array($this->id, $sub_arr)){
            $sub_arr[] = $this->id;
        }
        
        if($this->getSubCategories()->count() > 0){
            foreach($this->getSubCategories() as $sub_category){
                $sub_arr[] = $sub_category->id;
                $sub_arr = $sub_category->getThisAndSubCategoriesId($sub_arr);
            }
        }
        
        return $sub_arr;
    }
    
    /**
     * return same level categories
     * @return type
     */
    public function getSameLevelCategories() {
        return Category::where('category_id', $this->category_id)->get();
    }
        
    /**
     * return parent tree categories
     * @param type $tree
     * @return type
     */
    public function getParentTreeCategories($tree = false) {
        if(!$tree){
            $tree = []; 
        }
        $tree[] = $this;
        if($this->category_id){
            if($this->getParentCategory()){
                return $this->getParentCategory()->getParentTreeCategories($tree);
            }
        }
        
        return array_reverse($tree);
    }
    
    /**
     * return tree category like 'first category > second category > ...'
     * @return string
     */
    public function makeTree(){
        $path = '';
        $tree = $this->getParentTreeCategories();
        foreach($tree as $key => $cat){
            $path .= $cat->getData()->name;
            if($key != count($tree)-1){
                $path .= ' > ';
            }
        }
        return $path;
    }
    
    /**
     * make category slug 
     * @param type $language_id
     * @return type
     */
    public function makeSlug($language_id = false) {
        if(!$language_id){
            $language_id = Language::getLanguage()->id;
        }
        $tree = $this->getParentTreeCategories();
        $slug = '';
        foreach ($tree as $key => $category){
            $slug .= $category->getData($language_id)->slug.'-';
        }
        return substr($slug, 0, -1);
    }
}
