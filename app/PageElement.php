<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\MorphTo;

class PageElement extends Model
{
    protected $table = 'page_elements';

    protected $fillable = [
        'page_id', 'elementable_type', 'elementable_id', 'type'
    ];

    /**
     * @return MorphTo
     */
    public function elementable(): MorphTo
    {
        return $this->morphTo();
    }
}
