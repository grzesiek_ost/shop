<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Language extends Model
{
    protected $table = 'languages';

    protected $fillable = [
        'name',
        'native_name',
        'shortcut',
        'active',
        'main'
    ];
    
    public function currency() {
        return $this->belongsTo('App\CurrencyTranslation');
    }
    
    public function productPhoto() {
        return $this->belongsTo('App\ProductPhotoTranslation');
    }
    
    public function productSize() {
        return $this->belongsTo('App\SizeTranslation');
    }
    
    public function product() {
        return $this->belongsTo('App\ProductTranslation');
    }

    public static function getMain() {
        if (Cache::has('main_language')) {
            $language = Cache::get('main_language');
        }
        else{
            $language = Language::where('main', 1)->first();
            Cache::put('main_language', $language);
        }

        return $language;
    }

    public static function getLanguage() {
        if (Cache::has('language')) {
            $language = Cache::get('language');
        }
        elseif(session()->has('language')){
            $language = Language::select(['id', 'shortcut'])->where('id', session('language'))->first();
            Cache::put('language', $language);
        }
        else{
            $language = Language::select(['id', 'shortcut'])->where('main', 1)->first();
            Cache::put('language', $language);
        }

        return $language;
    }

    /**
     * Scope a query to only include active languages.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query): Builder
    {
        return $query->where('active', 1);
    }
}
