<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductTranslation extends Model
{
    protected $table = 'product_translations';

    protected $fillable = [
        'name',
        'description'
    ];
    
    public function product() {
        return $this->belongsTo('App\Product');
    }
    
    public function language() {
        return $this->hasMany('App\Language');
    }
}
