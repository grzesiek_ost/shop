<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SizeTranslation extends Model
{
    protected $table = 'size_translations';

    protected $fillable = [
        'name'
    ];
    
    public function size() {
        return $this->belongsTo('App\Size');
    }
    
    public function language() {
        return $this->hasMany('App\Language');
    }
}
