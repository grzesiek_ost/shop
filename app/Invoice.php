<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Invoice extends Model
{
    protected $table = 'invoices';

    protected $fillable = [
        'order_id',
        'user_id',
        'invoice_number',
        'currency',
        'payment',
        'delivery',
        'delivery_price',
        'duty'
    ];
    
    public function user(){
        return $this->belongsTo('App\User');
    }
    
    public function order(){
        return $this->belongsTo('App\Order');
    }
    
    public function products(){
        return $this->hasMany('App\InvoiceProduct');
    }
    
    public function data(){
        return $this->hasOne('App\InvoiceData');
    }
    
    public function netPrice($duty) {
        return round($this->delivery_price / (1 + $duty / 100), 2);
    }
    
    /**
     * return new invoice number
     * @return type
     */
    public static function makeInvoiceNumber(){
        $invoices = Invoice::select(DB::raw('SUBSTRING(invoice_number, 9) AS invoice_number'))
                ->whereDate('created_at', '=', date('Y-m-d'))
                ->get()
                ->max('invoice_number');

        $new_number = intval($invoices) + 1;
        $date = date('Ymd');
        
        return $date.$new_number;
    }
}
