<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Status extends Model
{
    protected $table = 'statuses';

    public function translation() {
        return $this->hasMany('App\StatusTranslation');
    }
    
//    get data in selected language
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
}
