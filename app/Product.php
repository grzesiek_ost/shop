<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Currency;
use App\Language;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\File;

class Product extends Model
{
//    determines the maximum amount of a given product in a given size in cart
    const MAX_COUNT = 5;

    protected $table = 'products';

    protected $fillable = [
        'category_id',
        'active'
    ];
    
    public function prices() {
        return $this->hasMany('App\ProductPrice');
    }
    
    public function translation() {
        return $this->hasMany('App\ProductTranslation');
    }
    
    public function photos() {
        return $this->hasMany('App\ProductPhoto')->orderBy('sequence', 'ASC');
    }
    
    public function sizes() {
        return $this->belongsToMany('App\Size');
    }
    
    public function productStatistics() {
        return $this->hasMany(ProductStatistic::class);
    }
    
    public function quantities() {
        return $this->hasMany('App\Quantity');
    }
    
    public function sizeQuantity($id) {
        return $this->quantities->where('size_id', $id)->sum('quantity');
    }
    
    /**
     * get price in selected currency
     * @param type $currency_id
     * @return type
     */
    public function getPrice($currency_id = false) {
        $currency = Currency::getMain();
        if(!$currency_id){
            $currency_id = session()->has('currency') ? session('currency') : $currency->id;
        }
        
        if($currency_id == $currency->id){
            return $this->prices->where('currency_id', $currency_id)->first();
        }
        else{
            if($this->prices->where('currency_id', $currency_id)->count() > 0){
                return $this->prices->where('currency_id', $currency_id)->first();
            }
            else{
                return $this->prices->where('currency_id', $currency->id)->first();
            }
        }
    }
    
    /**
     * get product data in selected language
     * @param type $language_id
     * @return type
     */
    public function getProductData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
    
    /**
     * checks if product has selected currency and selected translation
     * @return boolean
     */
    public function isFilled(): bool
    {
        $currency = Currency::getMain();
        $currency_id = session()->has('currency') ? session('currency') : $currency->id;
        $language = Language::getMain();
        $language_id = session()->has('language') ? session('language') : $language->id;
        if($this->prices->where('currency_id', $currency_id)->count() > 0 && 
            $this->translation->where('language_id', $language_id)->count() > 0){
            return true;
        }
        else{
            return false;
        }
    }

    /**
     * check if product is novelty
     * @return bool
     */
    public function isNovelty(): bool
    {
        $novelty = Carbon::now()->sub(7, 'day');
        return $novelty->greaterThan($this->created_at) ? false : true;
    }
    
    /**
     * removes photos
     */
    public function removePhotos(): void
    {
        if($this->photos()){
            $files = [
                public_path("images/products/{$this->id}/original"),
                public_path("images/products/{$this->id}/550x400")
            ];
            
            foreach ($this->photos as $photo){
                foreach ($files as $file) {
                    if (File::exists($file.'/'.$photo->photo)) {
                        File::delete($file.'/'.$photo->photo);
                    }
                }
            }
        }
    }
}
