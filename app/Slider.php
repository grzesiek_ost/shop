<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Slider extends Model
{
    protected $table = 'sliders';

    protected $fillable = [

    ];

    /**
     * @return HasMany
     */
    public function images(): HasMany
    {
        return $this->hasMany(SliderImage::class)->orderBy('sequence', 'ASC');
    }

    /**
     * @return HasMany
     */
    public function activeImages(): HasMany
    {
        return $this->hasMany(SliderImage::class)->whereActive(1)->orderBy('sequence', 'ASC');
    }

    /**
     * @return MorphOne
     */
    public function element(): MorphOne
    {
        return $this->morphOne(PageElement::class, 'elementable');
    }
}
