<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Language;

class Size extends Model
{
    protected $table = 'sizes';

    public function translation() {
        return $this->hasMany('App\SizeTranslation');
    }
    
    public function quantities() {
        return $this->hasMany('App\Quantity');
    }
    
    public function products() {
        return $this->belongsToMany('App\Product');
    }
    
//    get data in selected language
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
}
