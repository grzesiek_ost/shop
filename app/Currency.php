<?php

namespace App;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Cache;

class Currency extends Model
{
    protected $table = 'currencies';

    protected $fillable = [
        'shortcut',
        'symbol',
        'active',
        'main',
    ];
    
    public function productPrice() {
        return $this->belongsTo('App\ProductPrice');
    }
    
    public function translation() {
        return $this->hasMany('App\CurrencyTranslation');
    }

    public static function getMain() {
        if (Cache::has('main_currency')) {
            $currency = Cache::get('main_currency');
        }
        else{
            $currency = Currency::where('main', 1)->first();
            Cache::put('main_currency', $currency);
        }

        return $currency;
    }

    public static function getCurrency() {
        if(Cache::has('currency')){
            $currency = Cache::get('currency');
        }
        elseif(session()->has('currency')){
            $currency = Currency::where('id', session('currency'))->first();
            Cache::put('currency', $currency);
        }
        else{
            $currency = Currency::getMain();
            Cache::put('currency', $currency);
        }

        return $currency;
    }

    /**
     * Scope a query to only include active currencies.
     *
     * @param $query
     * @return mixed
     */
    public function scopeActive($query): Builder
    {
        return $query->where('active', 1);
    }
}
