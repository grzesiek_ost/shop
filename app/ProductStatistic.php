<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductStatistic extends Model
{
    protected $table = 'product_statistics';

    /**
     * relation to product
     * @return type
     */
    public function product() {
        return $this->belongsTo(Product::class);
    }
    
    /**
     * relation to user
     * @return type
     */
    public function user() {
        return $this->belongsTo(User::class);
    }
}
