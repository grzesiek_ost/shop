<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Delivery extends Model
{
    protected $table = 'deliveries';

    protected $fillable = [
        'active',
        'logo'
    ];
    
    public function prices() {
        return $this->hasMany('App\DeliveryPrice');
    }
    
    public function translation() {
        return $this->hasMany('App\DeliveryTranslation');
    }
    
    /**
     * get price in selected currency
     * @param type $currency_id
     * @return type
     */
    public function getPrice($currency_id = false) {
        $currency = Currency::getMain();
        if(!$currency_id){
            $currency_id = session()->has('currency') ? session('currency') : $currency->id;
        }
        
        if($currency_id == $currency->id){
            return $this->prices->where('currency_id', $currency_id)->first();
        }
        else{
            if($this->prices->where('currency_id', $currency_id)->count() > 0){
                return $this->prices->where('currency_id', $currency_id)->first();
            }
            else{
                return $this->prices->where('currency_id', $currency->id)->first();
            }
        }
    }
    
    /**
     * get delivery data in selected language
     * @param type $language_id
     * @return type
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
    
    /**
     * removes logo
     */
    public function removeLogo() {
        if($this->logo){
            $original_image_path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .
                'images' . DIRECTORY_SEPARATOR . 'deliveries' . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR .
                'original' . DIRECTORY_SEPARATOR . $this->logo);

            $files = [$original_image_path];

            foreach ($files as $file) {
                if (File::exists($file)) {
                    File::delete($file);
                }
            }
            
            $this->logo = null;
        }
    }
}
