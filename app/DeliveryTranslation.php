<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class DeliveryTranslation extends Model
{
    protected $table = 'delivery_translations';

    protected $fillable = [
        'name',
        'description'
    ];
}
