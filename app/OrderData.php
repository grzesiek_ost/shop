<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderData extends Model
{
    protected $table = 'order_datas';

    protected $fillable = [
        'user_id',
        'order_id',
        'name',
        'surname',
        'email',
        'phone',
        'city',
        'street',
        'house_number',
        'apartment_number',
        'zip_code',
    ];
    
    public function order() {
        return $this->belongsTo('App\Order');
    }
}
