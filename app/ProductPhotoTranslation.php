<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ProductPhotoTranslation extends Model
{
    protected $table = 'product_photo_translations';

    protected $fillable = [
        'name', 'product_photo_id', 'language_id'
    ];
}
