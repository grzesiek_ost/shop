<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceProduct extends Model
{
    protected $table = 'invoice_products';

    protected $fillable = [
        'invoice_id',
        'name',
        'size',
        'quantity',
        'price'
    ];
    
    public function invoice() {
        return $this->belongsTo('App\Invoice');
    }
    
    public function netPrice($duty) {
        return round($this->price / (1 + $duty / 100), 2);
    }
}
