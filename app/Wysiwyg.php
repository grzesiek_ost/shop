<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;
use Illuminate\Database\Eloquent\Relations\MorphOne;

class Wysiwyg extends Model
{
    protected $table = 'wysiwygs';

    protected $fillable = [
        
    ];

    /**
     * @return MorphOne
     */
    public function element(): MorphOne
    {
        return $this->morphOne(PageElement::class, 'elementable');
    }

    /**
     * @return HasMany
     */
    public function translation(): HasMany
    {
        return $this->hasMany(WysiwygTranslation::class);
    }

    /**
     * get wysiwyg data in selected language
     *
     * @param bool $language_id
     * @return mixed
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }

        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
}
