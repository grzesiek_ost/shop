<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\File;

class Payment extends Model
{
    protected $table = 'payments';

    protected $fillable = [
        'active',
        'logo'
    ];
    
    public function translation() {
        return $this->hasMany('App\PaymentTranslation');
    }
    
    /**
     * get delivery data in selected language
     * @param type $language_id
     * @return type
     */
    public function getData($language_id = false) {
        $language = Language::getMain();
        if(!$language_id){
            $language_id = session()->has('language') ? session('language') : $language->id;
        }
        
        if($language_id == $language->id){
            return $this->translation->where('language_id', $language_id)->first();
        }
        else{
            if($this->translation->where('language_id', $language_id)->count() > 0){
                return $this->translation->where('language_id', $language_id)->first();
            }
            else{
                return $this->translation->where('language_id', $language->id)->first();
            }
        }
    }
    
    /**
     * removes logo
     */
    public function removeLogo() {
        if($this->logo){
            $original_image_path = storage_path('app' . DIRECTORY_SEPARATOR . 'public' . DIRECTORY_SEPARATOR .
                'images' . DIRECTORY_SEPARATOR . 'payments' . DIRECTORY_SEPARATOR . $this->id . DIRECTORY_SEPARATOR .
                'original' . DIRECTORY_SEPARATOR . $this->logo);

            $files = [$original_image_path];

            foreach ($files as $file) {
                if (File::exists($file)) {
                    File::delete($file);
                }
            }
            
            $this->logo = null;
        }
    }
}
