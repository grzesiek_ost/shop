<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CategoryTranslation extends Model
{
    protected $table = 'category_translations';

    protected $fillable = [
        'name'
    ];
    
    public function category() {
        return $this->belongsTo('App\Category');
    }
    
    public function language() {
        return $this->hasMany('App\Language');
    }
}
