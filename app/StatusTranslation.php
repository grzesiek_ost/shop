<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusTranslation extends Model
{
    protected $table = 'status_translations';

    protected $fillable = [
        'name'
    ];
    
    public function status() {
        return $this->belongsTo('App\Status');
    }
    
    public function language() {
        return $this->hasMany('App\Language');
    }
}
