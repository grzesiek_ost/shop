<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderItem extends Model
{
    protected $table = 'order_items';

    protected $fillable = [
        'order_id',
        'size_id',
        'product_id',
        'size',
        'product',
        'count',
        'item_price'
    ];
    
    public function order()
    {
        return $this->belongsTo('App\Order');
    }
}
