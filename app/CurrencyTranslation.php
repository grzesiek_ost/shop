<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class CurrencyTranslation extends Model
{
    protected $table = 'currencies_translations';

    protected $fillable = [
        'name'
    ];
    
    public function currency() {
        return $this->belongsTo('App\Currency');
    }
    
    public function language() {
        return $this->belongsTo('App\Language');
    }
}
