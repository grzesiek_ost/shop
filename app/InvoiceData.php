<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class InvoiceData extends Model
{
    protected $table = 'invoice_datas';

    protected $fillable = [
        'invoice_id',
        'name',
        'surname',
        'email',
        'phone',
        'city',
        'street',
        'house_number',
        'apartment_number',
        'zip_code'
    ];
    
    public function invoice() {
        return $this->belongsTo('App\Invoice');
    }
}
