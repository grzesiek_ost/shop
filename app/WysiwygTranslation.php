<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class WysiwygTranslation extends Model
{
    protected $table = 'wysiwyg_translations';

    protected $fillable = [
        'wysiwyg_id', 'language_id', 'name', 'content'
    ];

    public function wysiwyg(): BelongsTo
    {
        return $this->belongsTo(Wysiwyg::class);
    }

    public function language(): BelongsTo
    {
        return $this->belongsTo(Language::class);
    }

    public function getContentAttribute($value)
    {
        return htmlspecialchars_decode($value);
    }
}
