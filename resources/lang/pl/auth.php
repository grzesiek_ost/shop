<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'Proszę podać poprawne dane logowania.',
    'throttle' => 'Zbyt wiele prób logowania. Spróbuj ponownie za :seconds sekund.',
    'reset_password_page' => 'Resetowanie hasła',
    'reset_password' => 'Resetuj hasło',
    'register_page' => 'Rejestracja',
    'register' => 'Zarejestruj',
    'verify_email' => 'Zweryfikuj swój adres email',
    'verification_link_info' => 'Zanim przejdziesz dalej, sprawdź pocztę e-mail pod kątem linku weryfikacyjnego. Jeśli nie dostałeś e-maila',
    'click_here' => 'kliknij tutaj',
    'link_sent' => 'Nowy link weryfikacyjny został wysłany na Twój adres e-mail'

];
