<?php

return [
    'name' => 'Imię',
    'surname' => 'Nazwisko',
    'email' => 'Adres e-mail',
    'phone' => 'Numer telefonu',
    'street' => 'Ulica',
    'house_number' => 'Numer domu',
    'apartment_number' => 'Numer mieszkania',
    'zip_code' => 'Kod pocztowy',
    'city' => 'Miasto',
    'delivery' => 'Dostawa',
    'payment' => 'Płatność',
    'message' => 'Wiadomość',
    'send' => 'Wyślij',
    'old_password' => 'Stare hasło',
    'password' => 'Hasło',
    'confirm_password' => 'Powtórz hasło',
    'remember_me' => 'Zapamiętaj dane logowania',
    'remind_password' => 'Przypomnij hasło',
    'login' => 'Zaloguj'
];
