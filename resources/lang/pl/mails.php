<?php

return [
    'order_thanks' => 'Dziękujemy za złożenie zamówienia',
    'delivery' => 'Dostawa',
    'payment' => 'Płatność',
    'order_date' => 'Data złożenia zamówienia',
    'order_products' => 'Zamówione produkty',
    'delivery_address' => 'Adres dostawy',
    'order_number' => 'Numer zamówienia',
    'order_value' => 'Wartość zamówienia',
    'mail_send' => 'Twoja wiadomość została wysłana',
    'mail_error' => 'Wystąpiły problemy z wysyłką wiadomości'
];

