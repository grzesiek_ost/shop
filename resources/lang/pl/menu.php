<?php

return [
    'shop' => 'Sklep',
    'panel' => 'Panel',
    'data' => 'Dane',
    'logout' => 'Wyloguj',
    'products' => 'Produkty',
    'orders' => 'Zamówienia',
    'invoices' => 'Faktury',
    'users' => 'Użytkownicy',
    'categories' => 'Kategorie',
    'sizes' => 'Rozmiary',
    'deliveries' => 'Dostawy',
    'payments' => 'Płatności',
    'languages' => 'Języki',
    'pages' => 'Strony',
    'favicon' => 'Favicona',
];
