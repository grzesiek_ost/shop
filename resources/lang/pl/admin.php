<?php

return [
    'order' => 'Zamówienie',
    'invoice' => 'Faktura',
    'invoice_generated' => 'Wygenerowano fakturę',
    'get_invoice' => 'Pobierz fakturę',
    'make_invoice' => 'Wygeneruj fakturę',
    'saved_changes' => 'Zapisano zmiany',
    'deleted' => 'Usunięto',
    'delete' => 'Usuń',
    'edit' => 'Edytuj',
    'fill_all' => 'Uzupełnij wszystkie pola',
    'photo' => 'Zdjęcie',
    'photos' => 'Zdjęcia',
    'delete_photo' => 'Usuń zdjęcie',
    'save' => 'Zapisz',
    'add_new' => 'Dodaj nowe',
    'name' => 'Nazwa',
    'native_name' => 'Nazwa w języku ojczystym',
    'first_name' => 'Imię',
    'surname' => 'Nazwisko',
    'email' => 'Email',
    'phone' => 'Telefon',
    'password' => 'Hasło',
    'category' => 'Kategoria',
    'category_path' => 'Ścieżka kategorii',
    'parent_category' => 'Nadrzędna kategoria',
    'price' => 'Cena',
    'description' => 'Opis',
    'active' => 'Aktywny',
    'activity' => 'Aktywność',
    'buyer_data' => 'Dane kupującego',
    'products' => 'Produkty',
    'order_price' => 'Cena zamówienia',
    'created_date' => 'Data utworzenia',
    'buy_date' => 'Data zakupu',
    'show' => 'Pokaż',
    'status' => 'Status',
    'payment' => 'Płatność',
    'delivery' => 'Dostawa',
    'back' => 'Powrót',
    'sizes' => 'Rozmiary',
    'quantity' => 'Ilość',
    'role' => 'Rola',
    'shortcut' => 'skrót',
    'flag' => 'Flaga',
    'quantity_info' => 'Uzupełnij wszysttkie pola, możesz dodać lub odjąć ilość każdego rozmiaru. 0 oznacza brak zmian',
    'default_size' => 'Domyślny rozmiar (tylko jeden rozmiar może być zaznaczony)',
    'confirm_delete' => 'Czy jesteś pewny, że chcesz usunąć wybraną pozycję?',
    'cancel' => 'Anuluj',
    'contact_data' => 'Dane kontaktowe',
    'last_registered' => 'Ostatni zarejestrowani użytkownicy',
    'last_orders' => 'Ostatnie zamówienia',
    'orders_sum' => 'Suma zamówień',
    'yes' => 'Tak',
    'no' => 'Nie',
    'url' => 'Link',
    'favicon' => 'Favicona',
    'favicon_info' => 'Wgraj faviconę. Musi być kwadratem o boku max. 256 px.',
    'check_to_delete' => 'Zaznacz, jeżeli chcesz usunąć',
];

