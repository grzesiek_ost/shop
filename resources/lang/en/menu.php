<?php

return [
    'shop' => 'Shop',
    'panel' => 'Panel',
    'data' => 'Data',
    'logout' => 'Logout',
    'products' => 'Products',
    'orders' => 'Orders',
    'invoices' => 'Invoices',
    'users' => 'Users',
    'categories' => 'Categories',
    'sizes' => 'Sizes',
    'deliveries' => 'Deliveries',
    'payments' => 'Payments',
    'languages' => 'Languages',
    'pages' => 'Pages',
    'favicon' => 'Favicon',
];
