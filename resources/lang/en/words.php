<?php

return [
    'login' => 'Login',
    'register' => 'Register',
    'more' => 'More details',
    'back_home' => 'Back to Home Page',
    '404_message' => 'The page you are looking for was not found.',
    'size' => 'Size',
    'sizes' => 'Sizes',
    'add_to_cart' => 'Add to cart',
    'buy' => 'Buy',
    'buy_now' => 'Buy now',
    'cart' => 'Cart',
    'photo' => 'Photo',
    'name' => 'Name',
    'price' => 'Price',
    'quantity' => 'Quantity',
    'empty_basket' => 'You have an empty basket',
    'next' => 'Next',
    'shipping_info' => 'Shipping information',
    'my_data' => 'My data',
    'return' => 'Return',
    'save' => 'Save',
    'delivery' => 'Delivery',
    'payment' => 'Payment',
    'summary' => 'Summary',
    'sum' => 'Sum',
    'order_finalization' => 'Order has been placed',
    'contact' => 'Contact',
    'cookie_info' => 'The site uses cookies. By using the site, you accept the use of such files',
    'understand' => 'I understand',
    'hello_msg' => 'Hello :name, nice to see You',
    'change_pass' => 'Change password',
    'meta_description' => 'Typical Janusz website',
    'search' => 'Search',
];
