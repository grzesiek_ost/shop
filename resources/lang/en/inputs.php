<?php

return [
    'name' => 'Name',
    'surname' => 'Surmane',
    'email' => 'E-mail adress',
    'phone' => 'Phone number',
    'street' => 'Street',
    'house_number' => 'House number',
    'apartment_number' => 'Apartment number',
    'zip_code' => 'Zip code',
    'city' => 'City',
    'delivery' => 'Delivery',
    'payment' => 'Payment',
    'message' => 'Message',
    'send' => 'Send',
    'password' => 'Password',
    'old_password' => 'Old password',
    'confirm_password' => 'Confirm password',
    'remember_me' => 'Remember me',
    'remind_password' => 'Remind password',
    'login' => 'Login'
];
