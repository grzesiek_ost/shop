<?php

return [
    'order_thanks' => 'Thank you for your order',
    'delivery' => 'Delivery',
    'payment' => 'Payment',
    'order_date' => 'Date of placing the order',
    'order_products' => 'Ordered products',
    'delivery_address' => 'Delivery address',
    'order_number' => 'Order number',
    'order_value' => 'Value of the order',
    'mail_send' => 'Mail send',
    'mail_error' => 'Error during sending message'
];

