<?php

return [
    'order' => 'Order',
    'invoice' => 'Invoice',
    'invoice_generated' => 'Invoice generated',
    'get_invoice' => 'Get invoice',
    'make_invoice' => 'Generate invoice',
    'saved_changes' => 'Saved changes',
    'deleted' => 'Deleted',
    'delete' => 'Delete',
    'edit' => 'Edit',
    'fill_all' => 'Please fill all fields',
    'photo' => 'Photo',
    'photos' => 'Photos',
    'delete_photo' => 'Delete photo',
    'save' => 'Save',
    'add_new' => 'Add new',
    'name' => 'Name',
    'native_name' => 'Native name',
    'first_name' => 'Name',
    'surname' => 'Surame',
    'email' => 'Email',
    'phone' => 'Phone',
    'password' => 'Password',
    'category' => 'Category',
    'category_path' => 'Category path',
    'parent_category' => 'Parent category',
    'price' => 'Price',
    'description' => 'Description',
    'active' => 'Active',
    'activity' => 'Activity',
    'buyer_data' => 'Buyer data',
    'products' => 'Products',
    'order_price' => 'Order price',
    'created_date' => 'Created date',
    'buy_date' => 'Buy date',
    'show' => 'Show',
    'status' => 'Status',
    'payment' => 'Payment',
    'delivery' => 'Delivery',
    'back' => 'Back',
    'sizes' => 'Sizes',
    'quantity' => 'Quantity',
    'role' => 'Role',
    'shortcut' => 'Shortcut',
    'flag' => 'Flag',
    'quantity_info' => 'Please fill all quantities, you can add or subtract quantity of each size. 0 means no changes',
    'default_size' => 'Default size (only one size can be checked)',
    'confirm_delete' => 'Are You sure that you want to delete selected item?',
    'cancel' => 'Cancel',
    'contact_data' => 'Contact data',
    'last_registered' => 'Last registered person',
    'last_orders' => 'Last orders',
    'orders_sum' => 'Orders sum',
    'yes' => 'Yes',
    'no' => 'No',
    'url' => 'Link',
    'favicon' => 'Favicon',
    'favicon_info' => 'Upload favicon. It must be a square with side length max. 256 px',
    'check_to_delete' => 'Select if you want to delete',
];

