<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Authentication Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used during authentication for various
    | messages that we need to display to the user. You are free to modify
    | these language lines according to your application's requirements.
    |
    */

    'failed' => 'These credentials do not match our records.',
    'throttle' => 'Too many login attempts. Please try again in :seconds seconds.',
    'reset_password_page' => 'Reset password',
    'reset_password' => 'Reset password',
    'register_page' => 'Register',
    'register' => 'Register',
    'verify_email' => 'Verify Your Email Address',
    'verification_link_info' => 'Before proceeding, please check your email for a verification link. If you did not receive the email',
    'click_here' => 'click here to request another',
    'link_sent' => 'A fresh verification link has been sent to your email address'

];
