<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
    <head>
    </head>
    <body>
        @yield('content')
    </body>
</html>