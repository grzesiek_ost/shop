@extends('mail.layout')

@section('content')
    <p>@lang('mails.order_thanks')</p>
    <p>@lang('mails.order_number'): {{ $order->id }}</p>
    <p>@lang('mails.payment'): {{ $order->payment }}</p>
    <p>@lang('mails.delivery'): {{ $order->delivery }} {{ $order->delivery_price }} {{ $order->currency }}</p>
    <p>@lang('mails.order_date'): {{ $order->created_at }}</p>
    <br/>
    <p>@lang('mails.order_products'):</p>
    @foreach($order->items as $item)
        <p>{{ $item->product }} [{{ $item->size }}], {{ $item->count }} x {{ $item->item_price }} {{ $order->currency }}</p>
    @endforeach
    <br/>
    <p>@lang('mails.delivery_address'):</p>
    <p>{{ $order->data->name }} {{ $order->data->surname }}</p>
    <p>{{ $order->data->email }} {{ $order->data->phone }}</p>
    <p>{{ $order->data->street }} {{ $order->data->house_number }}{{ $order->data->apartment_number ? '/'.$order->data->apartment_number : '' }}</p>
    <p>{{ $order->data->zip_code }}, {{ $order->data->city }}</p>
    <br/>
    <p>@lang('mails.order_value'): {{ $order->getOrderSum() + $order->delivery_price }} {{ $order->currency }}</p>
@endsection