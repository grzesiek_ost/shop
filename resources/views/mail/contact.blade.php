@extends('mail.layout')

@section('content')
    <p>Od: {{ $request->name }}</p>
    <p>Mail: {{ $request->email }}</p>
    <p>Telefon: {{ $request->phone }}</p>
    <p>Wiadomość: {{ $request->message }}</p>
@endsection