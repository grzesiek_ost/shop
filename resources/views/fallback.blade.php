@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-8 col-md-offset-2 fallback_div">
            <span>404</span>
            <p>@lang('words.404_message')</p>
            <a href="{{ route('home_page') }}" class="btn btn-info">@lang('words.back_home')</a>
        </div>
    </div>
</div>
@endsection
