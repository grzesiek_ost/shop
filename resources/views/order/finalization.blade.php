@extends('layouts.app')

@section('scripts')
@endsection

@section('style')
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            @lang('words.order_finalization')
        </div>
    </div>
</div>

@endsection
