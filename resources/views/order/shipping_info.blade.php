@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/shipping_info.js') }}"></script>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('words.shipping_info')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('order.delivery') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <span class="@if($errors->has('name')) error @endif">
                                {{ $errors->first('name') }}
                            </span>
                            <input class="form-control @if($errors->has('name')) error-input @endif" 
                                   placeholder="@lang('inputs.name')" 
                                   name="name" 
                                   type="text" 
                                   value="{{ old('name', $post['name'] ?? $user->name ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('surname')) error @endif">
                                {{ $errors->first('surname') }}
                            </span>
                            <input class="form-control @if($errors->has('surname')) error-input @endif" 
                                   placeholder="@lang('inputs.surname')" 
                                   name="surname" 
                                   type="text" 
                                   value="{{ old('surname', $post['surname'] ?? $user->surname ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('email')) error @endif">
                                {{ $errors->first('email') }}
                            </span>
                            <input class="form-control @if($errors->has('email')) error-input @endif" 
                                   placeholder="@lang('inputs.email')" 
                                   name="email" 
                                   type="email" 
                                   value="{{ old('email', $post['email'] ?? $user->email ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('phone')) error @endif">
                                {{ $errors->first('phone') }}
                            </span>
                            <input class="form-control @if($errors->has('phone')) error-input @endif" 
                                   placeholder="@lang('inputs.phone')" 
                                   name="phone" 
                                   type="tel" 
                                   value="{{ old('phone', $post['phone'] ?? $user->phone ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('street')) error @endif">
                                {{ $errors->first('street') }}
                            </span>
                            <input class="form-control @if($errors->has('street')) error-input @endif" 
                                   placeholder="@lang('inputs.street')" 
                                   name="street" 
                                   type="text" 
                                   value="{{ old('street', $post['street'] ?? $user->userInformation->street ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('house_number')) error @endif">
                                {{ $errors->first('house_number') }}
                            </span>
                            <input class="form-control @if($errors->has('house_number')) error-input @endif" 
                                   placeholder="@lang('inputs.house_number')" 
                                   name="house_number" 
                                   type="text" 
                                   value="{{ old('house_number', $post['house_number'] ?? $user->userInformation->house_number ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('apartment_number')) error @endif">
                                {{ $errors->first('apartment_number') }}
                            </span>
                            <input class="form-control @if($errors->has('apartment_number')) error-input @endif" 
                                   placeholder="@lang('inputs.apartment_number')" 
                                   name="apartment_number" 
                                   type="text" 
                                   value="{{ old('apartment_number', $post['apartment_number'] ?? $user->userInformation->apartment_number ?? '') }}">
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('zip_code')) error @endif">
                                {{ $errors->first('zip_code') }}
                            </span>
                            
                            <input class="form-control @if($errors->has('zip_code')) error-input @endif" 
                                   placeholder="@lang('inputs.zip_code')" 
                                   name="zip_code" 
                                   type="text" 
                                   value="{{ old('zip_code', $post['zip_code'] ?? $user->userInformation->zip_code ?? '') }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('city')) error @endif">
                                {{ $errors->first('city') }}
                            </span>
                            <input class="form-control @if($errors->has('city')) error-input @endif" 
                                   placeholder="@lang('inputs.city')" 
                                   name="city" 
                                   type="text" 
                                   value="{{ old('city', $post['city'] ?? $user->userInformation->city ?? '') }}"
                                   required>
                        </div>
                        
                        <div class="form-group col-xs-6">
                            <a href="{{ route('cart.back') }}" class="btn btn-lg btn-success btn-block" id="back_button">
                                @lang('words.return')
                            </a>
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('words.next')">
                        </div>
                        @if(isset($post['delivery']))
                            <input type="hidden" name="delivery" value="{{ $post['delivery'] }}" />
                        @endif
                        @if(isset($post['payment']))
                            <input type="hidden" name="payment" value="{{ $post['payment'] }}" />
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
