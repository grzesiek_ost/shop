@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/delivery.js') }}"></script>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/delivery.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('words.delivery')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('order.payment') }}">
                        {{ csrf_field() }}
                        <div class="form-check delivery_checkboxes">
                            @foreach($deliveries as $delivery)
                                    <label class="form-check-label" for="delivery_{{ $delivery->id }}">
                                        <input class="form-check-input" type="radio" name="delivery" id="delivery_{{ $delivery->id }}" 
                                               value="{{ $delivery->id }}" 
                                               @if(isset($post['delivery']) && $post['delivery'] == $delivery->id) 
                                                checked 
                                               @elseif($loop->first && !isset($post['delivery']))
                                                checked 
                                               @endif>

                                            {{ $delivery->getData()->name }}
                                        <img src="{{ asset('storage/images/deliveries/'.$delivery->id.'/original/'.$delivery->logo) }}">
                                    </label>
                            @endforeach
                        </div>
                        <div class="form-group col-xs-6">
                            <a href="{{ route('order.shipping_info') }}" class="btn btn-lg btn-success btn-block" id="back_button">@lang('words.return')</a>
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('words.next')">
                        </div>
                        @if(isset($post))
                            @foreach($post as $key => $item)
                                @if($key != 'delivery')
                                    <input type="hidden" name="{{ $key }}" value="{{ $item }}" />
                                @endif
                            @endforeach
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
