@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/summary.js') }}"></script>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('words.summary')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('order.save') }}">
                        {{ csrf_field() }}
                        @if(isset($post))
                            @foreach($post as $key => $item)
                                <div class="form-group">
                                    <span>@lang('inputs.'.$key): </span>
                                    <span>{{ $item }}</span>
                                    <input type="hidden" name="{{ $key }}" value="{{ $item }}" />
                                </div>
                            @endforeach
                        @endif
                        
                        <table class="table">
                            <thead>
                                <tr>
                                    <th>@lang('words.photo')</th>
                                    <th>@lang('words.name')</th>
                                    <th>@lang('words.price')</th>
                                    <th>@lang('words.size')</th>
                                    <th>@lang('words.quantity')</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($products as $product)
                                    <tr class="active">
                                        <td>
                                            @foreach($product->photos as $photo)
                                                @if($loop->first)
                                                    <img src="{{ asset('storage/images/products/'.$product->id ).'/550x400/'.$photo->photo }}" 
                                                         alt="{{ $photo->getData()->name }}" class="image-thumb">
                                                @endif
                                            @endforeach
                                        </td>
                                        <td>{{ $product->getProductData()->name }}</td>
                                        <td>{{ $product->getPrice()->price }} {{ $currency->shortcut }}</td>
                                        <td>{{ $product->size->getData()->name }}</td>
                                        <td>{{ $product->quantity }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        
                        
                        <div class="form-group">
                            <span>@lang('inputs.payment'): </span>
                            <span>{{ $payment->getData()->name }}</span>
                            <input type="hidden" name="payment" value="{{ $payment->id }}" />
                        </div>
                        <div class="form-group">
                            <span>@lang('inputs.delivery'): </span>
                            <span>{{ $delivery->getData()->name }}</span>
                            <span>{{ $delivery->getPrice()->price }}</span>
                            <input type="hidden" name="delivery" value="{{ $delivery->id }}" />
                        </div>
                        <div class="form-group">
                            <span>@lang('words.sum'): </span>
                            <span>{{ $cart_sum + $delivery->getPrice()->price }}</span>
                        </div>
                        
                        <div class="form-group col-xs-6">
                            <a href="{{ route('order.payment_back') }}" class="btn btn-lg btn-success btn-block" id="back_button">
                                @lang('words.return')
                            </a>
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('words.buy')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
