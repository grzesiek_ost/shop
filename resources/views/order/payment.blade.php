@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/payment.js') }}"></script>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/payment.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('words.payment')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('order.summary') }}">
                        {{ csrf_field() }}
                        <div class="form-check payment_checkboxes">
                            @foreach($payments as $payment)
                                    <label class="form-check-label" for="payment_{{ $payment->id }}">
                                        <input class="form-check-input" type="radio" name="payment" id="payment_{{ $payment->id }}" 
                                               value="{{ $payment->id }}" 
                                               @if(isset($post['payment']) && $post['payment'] == $payment->id) 
                                                checked 
                                               @elseif($loop->first && !isset($post['payment']))
                                                checked 
                                               @endif>

                                        <span>{{ $payment->getData()->name }}</span>
                                        @if($payment->logo)
                                            <img src="{{ asset('storage/images/payments/'.$payment->id.'/original/'.$payment->logo) }}">
                                        @endif
                                    </label>
                            @endforeach
                        </div>
                        <div class="form-group col-xs-6">
                            <a href="{{ route('order.delivery_back') }}" class="btn btn-lg btn-success btn-block" id="back_button">
                                @lang('words.return')
                            </a>
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('words.next')">
                        </div>
                        @if(isset($post))
                            @foreach($post as $key => $item)
                                @if($key != 'payment')
                                    <input type="hidden" name="{{ $key }}" value="{{ $item }}" />
                                @endif
                            @endforeach
                        @endif
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
