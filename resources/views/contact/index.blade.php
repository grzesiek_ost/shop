@extends('layouts.app')

@section('scripts')
    <script src="https://www.google.com/recaptcha/api.js" async defer></script>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('css/contact.css') }}">
@endsection

@section('content')
    <section id="contact">
        <div class="section-content">
            <h1 class="section-header">
                @lang('words.contact')
            </h1>
        </div>
        <div class="contact-section">
            <div class="container">
            @if (session('message'))
                <div class="col-xs-12">
                    <div class="alert alert-info">
                        {{ session('message') }}
                    </div>
                </div>
            @endif
            @if ($errors->any())
                <div class="col-xs-12">
                    <div class="alert alert-danger">
                        {!! implode('', $errors->all('<p>:message</p>')) !!}
                    </div>
                </div>
            @endif
                <form method="post" action="{{ route('contact.send') }}">
                    @csrf
                    <div class="col-xs-12 col-sm-6 form-line">
                        <div class="form-group">
                            <label for="name">@lang('inputs.name')</label>
                            <input type="text" name="name" class="form-control" id="name" required="" value="{{ old('name') ?? '' }}">
                        </div>
                        <div class="form-group">
                            <label for="email">@lang('inputs.email')</label>
                            <input type="email" name="email" class="form-control" id="email" required="" value="{{ old('email') ?? '' }}">
                        </div>	
                        <div class="form-group">
                            <label for="phone">@lang('inputs.phone')</label>
                            <input type="tel" name="phone" class="form-control" id="phone"  value="{{ old('phone') ?? '' }}">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="form-group">
                            <label for="message">@lang('inputs.message')</label>
                            <textarea class="form-control" name="message" id="message" required="">{{ old('message') ?? '' }}</textarea>
                        </div>
                        <div class="g-recaptcha" data-sitekey="{{ env('GOOGLE_RECAPTCHA_SITE_KEY') }}"></div>
                        <div>
                            <button type="submit" class="btn btn-default submit"><i class="fa fa-paper-plane" aria-hidden="true"></i> @lang('inputs.send')</button>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </section>
@endsection
