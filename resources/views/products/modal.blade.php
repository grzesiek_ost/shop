<!--modal informing about adding to the cart-->
<div class="modal fade" id="addedToCart" tabindex="-1" role="dialog" aria-labelledby="addedToCartLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <p>@lang('modals.added_to_cart')</p>
                <button type="button" class="btn btn-info" data-dismiss="modal">@lang('modals.close')</button>
            </div>
        </div>
    </div>
</div>