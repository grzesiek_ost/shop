@extends('layouts.app')

@section('style')
    <link rel="stylesheet" href="{{ asset('css/product.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('js/product_list.js') }}"></script>
@endsection

@section('content')

<input type="hidden" name="url_cart_store" value="{{ route('cart.add') }}" />
<section class="thumbnail_section">
    <div class="container">
        <!--thumbnails-->
        <div class="row">
            <div class="container">
                <a href="{{ route('shop.categories') }}">@lang('menu.shop')</a> &lsaquo;
                @foreach($category->getParentTreeCategories() as $cat)
                    <a href="{{ route('shop.products', ['category' => $cat->getData()->slug]) }}">{{ $cat->getData()->name }}</a>
                        @if(!$loop->last)
                            &lsaquo;
                        @endif
                @endforeach
                @if($category->getSubCategories()->count())
                    <p class="mobile_thumbnails">
                        @lang('menu.categories'):
                        @foreach($category->getSubCategories() as $sub_cat)
                            <a href="{{ route('shop.products', ['category' => $sub_cat->getData()->slug]) }}">
                                {{ $sub_cat->getData()->name }}
                            </a>
                            @if(!$loop->last)
                                |
                            @endif
                        @endforeach
                    </p>
                @endif
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container">
        <div class="row">
            <div class="col-xs-12 col-md-3">
                <!--categories-->
                <div class="col-xs-12 left_side_categories">
                    @if($category->getParentCategory())
                        <a href="{{ route('shop.products', ['category' => $category->getParentCategory()->getData()->slug]) }}">
                            &lsaquo; {{ $category->getParentCategory()->getData()->name }}
                        </a>
                        <br/>
                    @endif
                </div>
                <div class="col-xs-12 left_side_categories">
                    @foreach($category->getSameLevelCategories() as $cat)
                        <a href="{{ route('shop.products', ['category' => $cat->getData()->slug]) }}"
                           class="{{ $category->id == $cat->id ? 'selected_category' : '' }}">
                            {{ $cat->getData()->name }}
                        </a>
                        <br/>
                        @if($cat->id == $category->id)
                            @foreach($category->getSubCategories() as $sub_cat)
                            <a href="{{ route('shop.products', ['category' => $sub_cat->getData()->slug]) }}">
                                &rarr;{{ $sub_cat->getData()->name }}
                            </a>
                            <br/>
                            @endforeach
                        @endif
                    @endforeach
                </div>
                <div class="col-xs-12">
                    <form name="search_form" method="GET" action="{{ route('shop.search', ['category' => $category->getData()->slug]) }}">
                        <div class="input-group mb-3" style="display: flex;">
                            <input name="search_text" type="text" class="form-control" placeholder="@lang('words.search')">
                            <div class="input-group-append">
                                <button class="btn btn-outline-secondary" type="submit">@lang('words.search')</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
            <div class="col-xs-12 col-md-9">
                @foreach($products as $product)
                    <div class="col-xs-12 col-sm-4 col-md-3">
                        <div class="product-grid3">
                            <div class="product-image3">
                                <a href="{{ route('shop.product', ['id' => $product->id]) }}">
                                    @if($product->photos->first())
                                        <img class="pic-1" src="{{ asset('storage/images/products/'.$product->id.'/480x640/'.$product->photos->first->photo->photo) }}"
                                            style="max-height: 285px; min-height: 285px;">
                                        <img class="pic-2"
                                             @if($product->photos->skip(1)->first->photo)
                                                src="{{ asset('storage/images/products/'.$product->id.'/480x640/'.$product->photos->skip(1)->first->photo->photo) }}"
                                             @else
                                                src="{{ asset('storage/images/products/'.$product->id.'/480x640/'.$product->photos->first->photo->photo) }}"
                                             @endif
                                             style="max-height: 285px; min-height: 285px;">
                                    @else
                                        <i class="far fa-image fa-7x" style="color: grey;"></i>
                                    @endif
                                </a>
                                <ul class="social">
                                    <li>
                                        <a href="#" title="@lang('words.buy_now')" class="buy_now" data-type="buy">
                                            <i class="far fa-money-bill-alt"></i>
                                        </a>
                                    </li>
                                    <li>
                                        <a href="#" title="@lang('words.add_to_cart')" class="add_to_cart" data-type="add">
                                            <i class="fa fa-shopping-cart"></i>
                                        </a>
                                    </li>
                                </ul>
                                <ul class="product_sizes">
                                    @foreach($product->sizes as $size)
                                        <li>
                                            <a href="#" title="{{ $size->getData()->name }}" class="select_item"
                                               data-size-id="{{ $size->id }}" data-product-id="{{ $product->id }}">
                                                {{ $size->getData()->name }}
                                            </a>
                                        </li>
                                    @endforeach
                                </ul>
                                <i class="fas fa-spinner fa-spin" style="display: none;"></i>
                                @if($product->isNovelty())
                                    <span class="product-new-label">New</span>
                                @endif
                            </div>
                            <div class="product-content">
                                <h3 class="title">
                                    <a href="{{ route('shop.product', ['id' => $product->id]) }}">
                                        {{ $product->getProductData()->name }}
                                    </a>
                                </h3>
                                <div class="price">
                                    {{ $product->getPrice()->price }} {{ $currency->shortcut }}
                                    <!--<span>$75.00</span>-->
                                </div>
<!--                                <ul class="rating">
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star"></li>
                                    <li class="fa fa-star disable"></li>
                                    <li class="fa fa-star disable"></li>
                                </ul>-->
                            </div>
                        </div>
                    </div>
                @endforeach
                
                <input type="hidden" name="cart_path" value="{{ route('cart.index') }}" />
                <div class="col-xs-12">
                    {{ $products->links() }}
                </div>
            </div>
        </div>
    </div>
</section>

@include('products.modal')

@endsection
