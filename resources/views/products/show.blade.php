@extends('layouts.app')

@section('head')
    @if(isset($product->photos->first()->photo))
        <meta property="og:image" content="{{ asset('storage/images/products/'.$product->id).'/original/'.$product->photos->first()->photo }}" />
    @endif
    <meta property="og:title" content="{{ $product->getProductData()->name }}" />
    <meta property="og:description" content="{{ $product->getProductData()->description }}" />
    <meta property="og:type" content="product" />
    <meta property="og:url" content="{{ route('shop.product', ['id' => $product->id]) }}" />
@endsection

@section('style')
    <link rel="stylesheet" type="text/css" href="{{ asset('css/product.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('plugins/glightbox/dist/css/glightbox.css') }}">
@endsection

@section('scripts')
    <script src="{{ asset('plugins/glightbox/dist/js/glightbox.min.js') }}"></script>
    <script src="{{ asset('js/product.js') }}"></script>
@endsection

@section('content')

    <input type="hidden" name="url_cart_store" value="{{ route('cart.add') }}" />
    <input type="hidden" name="url_get_quantity" value="{{ route('get_quantity') }}" />
    <input type="hidden" name="url_increase_counter" value="TODO" />

{{--    <div id="fb-root"></div>--}}
{{--    <script async defer crossorigin="anonymous" src="https://connect.facebook.net/pl_PL/sdk.js#xfbml=1&version=v5.0"></script>--}}

    <div class="container">
        <div class="card-deck mb-3 text-center">
            <div class="card mb-4 box-shadow">
                <div class="card-body">
                    <div class="col-sm-6">
                        <div id="gallery">
                            @foreach($product->photos as $photo)
                                <a href="{{ asset('storage/images/products/'.$product->id).'/480x640/'.$photo->photo }}" class="glightbox"
                                @if($loop->index >= 9) style="display: none;" @endif>
                                    <img src="{{ asset('storage/images/products/'.$product->id).'/480x640/'.$photo->photo }}" >
                                </a>
                            @endforeach
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="card-header">
                            <h3 class="my-0 font-weight-normal">{{ $product->getProductData()->name }}</h3>
                        </div>
                        <h1 class="card-title pricing-card-title">
                            <small class="text-muted">
                                {{ $product->getPrice()->price }} / {{ $currency->shortcut }}
                            </small>
                        </h1>
                        <ul class="list-unstyled mt-3 mb-4">
                            <li>{{ $product->getProductData()->description }}</li>
                        </ul>
                        <label>@lang('words.sizes')</label>
                        <div id="sizes" class="form-group">
                            <select class="size selectpicker" name="size" id="size">
                                @foreach($product->sizes as $size)
                                    <option value="{{ $size->id }}" data-product-id="{{ $product->id }}">
                                        {{ $size->getData()->name }}
                                    </option>
                                @endforeach
                            </select>
                            <select class="quantity selectpicker" name="quantity" id="quantity">
                            </select>
                        </div>
                        <input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}" />
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-block btn-info" id="add_to_cart">
                                <i class="fas fa-spinner fa-spin" style="display: none;"></i>
                                <span class="add_to_cart">@lang('words.add_to_cart')</span>
                            </button>
                        </div>
                        <div class="col-xs-6">
                            <button type="button" class="btn btn-lg btn-block btn-info" id="buy" data-cart="{{ route('cart.index') }}">
                                <i class="fas fa-spinner fa-spin" style="display: none;"></i>
                                <span class="buy">@lang('words.buy')</span>
                            </button>
                        </div>
{{--                        <div class="fb-like" data-href="{{ route('shop.product', ['id' => $product->id]) }}" data-width=""--}}
{{--                             data-layout="standard" data-action="like" data-size="small" data-share="true"></div>--}}
                    </div>
                </div>
            </div>
      </div>
    </div>

    @include('products.modal')

@endsection