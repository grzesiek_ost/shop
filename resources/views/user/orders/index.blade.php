@extends('layouts.app')

@section('admin_style')
<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <div class="row">
            @foreach($orders as $order)
                <div class="col-xs-12" style="border-bottom: 1px dashed #a7a7a7;">
                    <div class="col-md-4">
                        @foreach($order->items as $product)
                            <p>{{ $product->product }} ({{ $product->size }}) | {{ $product->count }} x {{ $product->item_price .' '.$product->currency }}</p>
                        @endforeach
                    </div>
                    <div class="col-md-4">
                        <p>@lang('words.sum'): {{ $order->getOrderSum() .' '.$order->currency }}</p>
                        <p>{{ $order->payment }}</p>
                        <p>{{ $order->delivery }}: {{ $order->delivery_price .' '.$order->currency }}</p>
                    </div>
                    <div class="col-md-4 text-center">
                        <p>{{ $order->created_at->format('d.m.Y') }}</p>
                    </div>
                </div>
            @endforeach
    </div>
    
    <!--pagination-->
    {{ $orders->links() }}
</div>

@endsection
