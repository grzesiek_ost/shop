@extends('layouts.app')

@section('content')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('words.change_pass')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('user.information.changePassword') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <span class="@if($errors->has('old_password')) error @endif">
                                {{ $errors->first('old_password') }}
                            </span>
                            <input class="form-control @if($errors->has('old_password')) error-input @endif" 
                                   placeholder="@lang('inputs.old_password')" 
                                   name="old_password" 
                                   type="password" 
                                   value=""
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('password')) error @endif">
                                {{ $errors->first('password') }}
                            </span>
                            <input class="form-control @if($errors->has('password')) error-input @endif" 
                                   placeholder="@lang('inputs.password')" 
                                   name="password" 
                                   type="password" 
                                   value=""
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if($errors->has('confirm_password')) error @endif">
                                {{ $errors->first('confirm_password') }}
                            </span>
                            <input class="form-control @if($errors->has('password_confirmation')) error-input @endif" 
                                   placeholder="@lang('inputs.confirm_password')" 
                                   name="password_confirmation" 
                                   type="password" 
                                   value=""
                                   required>
                        </div>
                        <div class="form-group col-xs-6">
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('words.save')">
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection