@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('plugins/slick/slick.min.js') }}"></script>
    <script src="{{ asset('js/home.js') }}"></script>
@endsection

@section('style')
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/slick/slick-theme.css') }}">
    <link rel="stylesheet" href="{{ asset('css/home.css') }}">
@endsection

@section('content')
    @if($slider)
        <div>
            <div class="slider">
                @foreach($slider->activeImages as $image)
                    <div class="text-center slide"
                         style="background:url({{ asset('storage/images/slider/' . $slider->id . '/' . $sliderConfig['width'] . 'x' . $sliderConfig['height'] . '/' . $image->photo) }});">
                        <p>{{ $image->getData()->name ?? '' }}</p>
                    </div>
                @endforeach
            </div>
        </div>
    @endif
    @if($wysiwyg)
        <div class="jumbotron jumbotron-fluid">
            <div class="container">
                <h1 class="display-4">{{ $wysiwyg->getData()->name ?? '' }}</h1>
                <p class="lead">{!! $wysiwyg->getData()->content ?? ''  !!}</p>
            </div>
        </div>
    @endif
@endsection
