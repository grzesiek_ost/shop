@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">Dashboard</div>

                <div class="panel-body">
                    @if (session('status'))
                        <div class="alert alert-success">
                            {{ session('status') }}
                        </div>
                    @endif
                    
                    <p class="welcome_text">
                        @lang('words.hello_msg', [ 'name' => Auth::user()->name ])!
                    </p>
                    
                    @if(Auth::user()->isWorker())
                        <div class="col-md-6">
                            <p>@lang('admin.last_registered')</p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">@lang('admin.first_name') @lang('admin.surname')</th>
                                        <th scope="col">@lang('admin.contact_data')</th>
                                        <th scope="col">@lang('admin.created_date')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($last_registered as $person)
                                        <tr>
                                            <td scope="row">{{ $person->name }} {{ $person->surname }}</td>
                                            <td>
                                                <p>{{ $person->email }}</p>
                                                <p>{{ $person->phone }}</p>
                                            </td>
                                            <td>{{ $person->created_at->format('d.m.Y H:i') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                        <div class="col-md-6">
                            <p>@lang('admin.last_orders')</p>
                            <table class="table table-bordered">
                                <thead>
                                    <tr>
                                        <th scope="col">@lang('admin.buyer_data')</th>
                                        <th scope="col">@lang('admin.products')</th>
                                        <th scope="col">@lang('admin.order_price')</th>
                                        <th scope="col">@lang('admin.buy_date')</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($last_orders as $order)
                                        <tr>
                                            <td scope="row">{{ $order->data->name .' '. $order->data->surname }}</td>
                                            <td>
                                                @foreach($order->items as $product)
                                                    <p>{{ $product->product }} ({{ $product->size }}) | {{ $product->count }} x {{ $product->item_price .' '.$product->currency }}</p>
                                                @endforeach
                                            </td>
                                            <td>{{ $order->getOrderSum() .' '.$order->currency }}</td>
                                            <td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
                                        </tr>
                                    @endforeach
                                </tbody>
                            </table>
                        </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
