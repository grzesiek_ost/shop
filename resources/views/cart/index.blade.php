@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/cart.js') }}"></script>
@endsection

@section('content')

<input type="hidden" name="url_cart_destroy" value="{{ route('cart.remove') }}" />

<div class="container">
    <form action="{{ route('order.shipping_info_back') }}" method="post" class="products_list"
          @if(empty($products)) style="display: none;" @endif >
        {{ csrf_field() }}
        <table class="table">
            <thead>
                <tr>
                    <th>@lang('words.photo')</th>
                    <th>@lang('words.name')</th>
                    <th>@lang('words.price')</th>
                    <th>@lang('words.size')</th>
                    <th>@lang('words.quantity')</th>
                    <th></th>
                </tr>
            </thead>
            <tbody>
                @if($products)
                    @foreach($products as $product)
                        <tr class="active product_item">
                            <td>
                                @foreach($product->photos as $photo)
                                    @if($loop->first)
                                        <img src="{{ asset('storage/images/products/'.$product->id ).'/550x400/'.$photo->photo }}" 
                                             alt="{{ $photo->getData()->name }}" class="image-thumb">
                                    @endif
                                @endforeach
                            </td>
                            <td>{{ $product->getProductData()->name }}</td>
                            <td>{{ $product->getPrice()->price }} {{ $currency->shortcut }}</td>
                            <td>{{ $product->size->getData()->name }}</td>
                            <td>{{ $product->quantity }}</td>
                            <td>
                                <span class="remove_cart" data-product="{{ $product->id }}" data-size="{{ $product->size->id }}">
                                    <i class="fas fa-trash-alt clickable_icon"></i>
                                </span>
                            </td>
                        </tr>
                    @endforeach
                @endif
            </tbody>
        </table>
        @if(isset($post))
            @foreach($post as $key => $item)
                <input type="hidden" name="{{ $key }}" value="{{ $item }}" />
            @endforeach
        @endif
        <input type="submit" class="btn btn-info" value="@lang('words.next')" />
    </form>
    <div style="text-align: center; padding: 50px; @if(!empty($products)) display: none @endif" class="no_products_div">
        <p>@lang('words.empty_basket')</p>
        <i class="fab fa-opencart fa-7x"></i>
    </div>
</div>

@endsection
