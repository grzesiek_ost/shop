<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">
    
    @yield('head')
    
    <meta property="og:image" content="{{ asset('storage/images/logo.jpg') }}" />
    <meta property="og:title" content="{{ config('app.name') }}" />
    <meta property="og:description" content="@lang('words.meta_description')" />
    <meta property="og:type" content="website" />
    <meta property="og:url" content="{{ config('app.url') }}" />

    @if(file_exists('storage/favicon.ico'))
        <link rel="icon" href="{{ asset('storage/favicon.ico') }}" />
    @else
        <link rel="icon" href="{{ asset('favicon.ico') }}" />
    @endif

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('css/bootstrap-select.min.css') }}">
    <link rel="stylesheet" href="{{ asset('css/main.css') }}">
    
    @yield('style')
    @yield('admin_style')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top navbar-fixed-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ url('/') }}">
                        {{ config('app.name', 'Laravel') }}
                    </a>
                </div>

                <div class="navbar-collapse collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-left text-center">
                        <li>
                            <form name="change_language_form" id="change_language_form" method="post" action="{{ route('changeLanguage') }}">
                                {{ csrf_field() }}
                                <select class="language" name="language" data-width="8em">
                                    @foreach($all_languages as $language)
                                        <option value="{{ $language->id }}" data-content="<img src='{{ asset('images/flags/'. $language->shortcut .'.png') }}'/> {{ $language->native_name }}" @if (Session::has('language') && Session::get('language') ==  $language->id ) selected @endif></option>
                                    @endforeach
                                </select>
                            </form>
                        </li>
                        <li>
                            <form name="change_currency_form" id="change_currency_form" method="post" action="{{ route('changeCurrency') }}">
                                {{ csrf_field() }}
                                <select class="currency" name="currency" data-width="8em">
                                    @foreach($all_currencies as $currency)
                                        <option value="{{ $currency->id }}" @if (Session::has('currency') && Session::get('currency') == $currency->id) selected @endif>{{  $currency->shortcut }}</option>
                                    @endforeach
                                </select>
                            </form>
                        </li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right text-center">
                        <li><a href="{{ route('shop.categories') }}">@lang('menu.shop')</a></li>
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">@lang('words.login')</a></li>
                            <li><a href="{{ route('register') }}">@lang('words.register')</a></li>
                        @else
                            @if(Auth::user()->isWorker())
                                <li class="dropdown">
                                    <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                        @lang('menu.panel') <span class="caret"></span>
                                    </a>

                                    <ul class="dropdown-menu">
                                        <li><a href="{{ route('admin.products.index') }}">@lang('menu.products')</a></li>
                                        <li><a href="{{ route('admin.orders.index') }}">@lang('menu.orders')</a></li>
                                        <li><a href="{{ route('admin.invoices.index') }}">@lang('menu.invoices')</a></li>
                                        <li><a href="{{ route('admin.users.index') }}">@lang('menu.users')</a></li>
                                        <li><a href="{{ route('admin.categories.index') }}">@lang('menu.categories')</a></li>
                                        <li><a href="{{ route('admin.sizes.index') }}">@lang('menu.sizes')</a></li>
                                        <li><a href="{{ route('admin.deliveries.index') }}">@lang('menu.deliveries')</a></li>
                                        <li><a href="{{ route('admin.payments.index') }}">@lang('menu.payments')</a></li>
                                        <li><a href="{{ route('admin.languages.index') }}">@lang('menu.languages')</a></li>
                                        <li><a href="{{ route('admin.pages.index') }}">@lang('menu.pages')</a></li>
                                        <li><a href="{{ route('admin.favicon.edit') }}">@lang('menu.favicon')</a></li>
                                    </ul>
                                </li>
                            @endif
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->name }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('user.information.save') }}">@lang('menu.data')</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('user.orders.index') }}">@lang('menu.orders')</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('user.information.password') }}">@lang('passwords.change')</a>
                                    </li>
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            @lang('menu.logout')
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>

                                </ul>
                            </li>
                        @endguest
                        <li>
                            <a href="{{ route('contact.index') }}" title="@lang('words.contact')">
                                <i class="far fa-envelope cart_icon"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ route('cart.index') }}" title="@lang('words.cart')">
                                <i class="fab fa-opencart cart_icon"></i>
                            </a>
                        </li>
                    </ul>
                </div>
            </div>
        </nav>
        <div class="container-fluid">
            <div class="row">
            @yield('content')
            </div>
        </div>
        
        <div class="cookie_info">
            <span>
                @lang('words.cookie_info')
                <button type="button" class="btn btn-sm btn-primary">
                    @lang('words.understand')
                </button>
            </span>
        </div>
    </div>
    <footer class="container-fluid">
        <div class="row">
            <div class="col-sm-12 text-center">
                &copy; {{ date('Y') }} {{ config('app.name') }}
            </div>
        </div>
    </footer>

    <!-- Scripts -->
    <!--<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>-->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('js/main.js') }}"></script>
    <script src="{{ asset('js/bootstrap-select.min.js') }}"></script>
    
    <!--fontawesome-->
    <!--<script defer src="https://use.fontawesome.com/releases/v5.3.1/js/all.js" integrity="sha384-kW+oWsYx3YpxvjtZjFXqazFpA7UP/MbiY4jvs+RWZo2+N94PFZ36T6TFkc9O3qoB" crossorigin="anonymous"></script>-->
    <script src="{{ asset('plugins/fontawesome-5.8.1/all.min.js') }}"></script>
    
    @yield('scripts')
    @yield('admin_scripts')
</body>
</html>
