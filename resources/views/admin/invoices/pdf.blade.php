<meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>

<style>
    body { 
        font-family: DejaVu Sans; 
    }
    .page-break {
        page-break-after: always;
    }
    table, th, td {
        border: 1px solid black;
        border-collapse: collapse;
        font-size: 0.8em;
    }
</style>
<div>
    <!--TODO style and translate-->
    <h1 style="text-align: center;">Number {{ $invoice->invoice_number }}</h1>
    <p>Sale date: {{ $invoice->order->created_at->format('d.m.Y') }}</p>
    <p>Date of issue: {{ $invoice->created_at->format('d.m.Y') }}</p>
    <div style="float: left;">
        <h4>Buyer's details</h4>
        <p>{{ $invoice->data->name }} {{ $invoice->data->surname }}</p>
        <p>{{ $invoice->data->street }} {{ $invoice->data->house_number }}{{ $invoice->data->apartment_number ? '/'.$invoice->data->apartment_number : '' }}</p>
        <p>{{ $invoice->data->zip_code }} {{ $invoice->data->city }}</p>
    </div>
    <div style="float: right;">
        <h4>Seller details</h4>
    </div>
    <div style="clear: both;"></div>
    <table>
        <tr>
            <th>Lp.</th>
            <th>Name</th>
            <th>Quantity</th>
            <th>Gross price</th>
            <th>Net price</th>
            <th>Sum gross price</th>
            <th>Sum net price</th>
            <th>Duty</th>
            <th>Duty value</th>
        </tr>
        @foreach($invoice->products as $product)
            <tr>
                <td>{{ $loop->index + 1 }}</td>
                <td>{{ $product->name }} | {{ $product->size }}</td>
                <td>{{ number_format($product->quantity, 2) }}</td>
                <td>{{ number_format($product->price, 2) }}</td>
                <td>{{ number_format($product->netPrice($invoice->duty), 2) }}</td>
                <td>{{ number_format($product->price * $product->quantity, 2) }}</td>
                <td>{{ number_format($product->netPrice($invoice->duty) * $product->quantity, 2) }}</td>
                <td>{{ number_format($invoice->duty, 2) }}</td>
                <td>{{ number_format(($product->price * $product->quantity) - ($product->netPrice($invoice->duty) * $product->quantity), 2) }}</td>
            </tr>
        @endforeach
        <tr>
            <td>{{ $invoice->products->count() + 1 }}</td>
            <td>{{ $invoice->delivery }}</td>
            <td>1.00</td>
            <td>{{ number_format($invoice->delivery_price, 2) }}</td>
            <td>{{ number_format($invoice->netPrice($invoice->duty), 2) }}</td>
            <td>{{ number_format($invoice->delivery_price, 2) }}</td>
            <td>{{ number_format($invoice->netPrice($invoice->duty), 2) }}</td>
            <td>{{ number_format($invoice->duty, 2) }}</td>
            <td>{{ number_format($invoice->delivery_price - $invoice->netPrice($invoice->duty), 2) }}</td>
        </tr>
    </table>
    <div style="clear: both;"></div>
    <div>
        <p>Payment type: {{ $invoice->payment }}</p>
    </div>
</div>

