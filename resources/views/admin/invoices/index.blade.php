@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.buyer_data')</th>
                <th>@lang('admin.products')</th>
                <th>@lang('admin.order_price')</th>
                <th>@lang('admin.created_date')</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($invoices as $invoice)
                <tr class="active">
                    <td>{{ $invoice->id }}</td>
                    <td>
                        <p style="margin: 0;">{{ $invoice->data->name .' '. $invoice->data->surname }}</p>
                        <p style="margin: 0;">{{ $invoice->data->street .' '. $invoice->data->house_number . ($invoice->data->apartment_number ? '/'.$invoice->data->apartment_number : '') }}</p>
                        <p style="margin: 0;">{{ $invoice->data->zip_code .' '. $invoice->data->city }}</p>
                        <p style="margin: 0;">{{ $invoice->data->email }}</p>
                        <p style="margin: 0;">{{ $invoice->data->phone }}</p>
                    </td>
                    <td>
                        @foreach($invoice->products as $product)
                            <p>{{ $product->name }} ({{ $product->size }}) | {{ $product->quantity }} x {{ $product->price .' '.$invoice->currency }}</p>
                        @endforeach
                    </td>
                    <td>
                        <!--TODO-->
                    </td>
                    <td>{{ $invoice->created_at->format('d.m.Y H:i') }}</td>
                    <td>
                        <a href="{{ route('admin.invoices.delete', ['id' => $invoice->id]) }}" 
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.invoices.show', ['id' => $invoice->id]) }}" title="@lang('admin.show')">
                            <i class="admin_fa fas fa-file-invoice fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $invoices->links() }}
</div>

@endsection
