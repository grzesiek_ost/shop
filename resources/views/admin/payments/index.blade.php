@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/Sortable.min.js') }}"></script>
    <script src="{{ asset('js/admin/payment.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<input type="hidden" name="url_change_order" value="{{ route('admin.payments.order') }}" />

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.payments.create') }}" role="button">@lang('admin.add_new')</a>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.name')</th>
                <th>@lang('admin.description')</th>
                <th>@lang('admin.photo')</th>
                <th>@lang('admin.activity')</th>
                <th>*</th>
            </tr>
        </thead>
        <tbody id="sortable_tbody">
            @foreach($payments as $payment)
                <tr class="active sortable_tr" draggable="false" data-sortable-id="{{ $loop->iteration }}">
                    <td class="sortable_handle">
                        <i class="fas fa-grip-horizontal"></i>
                        {{ $payment->id }}
                    </td>
                    <td>{{ $payment->getData()->name }}</td>
                    <td>{{ $payment->getData()->description }}</td>
                    <td>
                        @if($payment->logo)
                            <img src="{{ asset('storage/images/payments/'.$payment->id.'/original/'.$payment->logo) }}" 
                                 alt="{{ $payment->getData()->name }}" class="miniature" draggable="false" />
                        @endif
                    </td>
                    <td>{{ $payment->active ? 'yes' : 'no' }}</td>
                    <td>
                        <a href="{{ route('admin.payments.edit', ['id' => $payment->id]) }}" title="@lang('admin.edit')">
                            <i class="admin_fa far fa-edit fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.payments.delete', ['id' => $payment->id]) }}" 
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $payments->links() }}
</div>

@endsection
