@extends('layouts.app')


@section('admin_style')
<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form method="post" action="{{ route('admin.deliveries.save') }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        <fieldset>
                            @foreach($languages as $language)
                                <div>
                                    <img src="{{ asset('images/flags/'.$language->shortcut).'.png' }}" 
                                                    alt="{{ $language->name }}" />
                                    <p class="language_name">{{ ucfirst($language->name) }}</p>
                                </div>
                                <div class="form-group">
                                    <span class="@if(session()->has('error_data.name.'.$language->id)) error @endif">
                                        @if(session()->has('error_data.name.'.$language->id))
                                            {{ session('error_data.name.'.$language->id) }}
                                        @endif
                                    </span>
                                    <input class="form-control @if(session()->has('error_data.name.'.$language->id)) error-input @endif" 
                                           placeholder="@lang('admin.name') ({{ $language->shortcut }})" 
                                           name="name[{{ $language->id }}]" 
                                           type="text" 
                                           value="{{ old('name.'.$language->id, isset($delivery) ? $delivery->getData($language->id)->name : '') }}"
                                           required>
                                </div>
                                <div class="form-group">
                                    <span class="@if(session()->has('error_data.description.'.$language->id)) error @endif">
                                        @if(session()->has('error_data.description.'.$language->id))
                                            {{ session('error_data.description.'.$language->id) }}
                                        @endif
                                    </span>
                                    <input class="form-control @if(session()->has('error_data.description.'.$language->id)) error-input @endif" 
                                           placeholder="@lang('admin.description') ({{ $language->shortcut }})" 
                                           name="description[{{ $language->id }}]" 
                                           type="text" 
                                           value="{{ old('description.'.$language->id, isset($delivery) ? $delivery->getData($language->id)->description : '') }}"
                                           required>
                                </div>
                            @endforeach
                            <div class="checkbox">
                                <label>
                                    <span class="@if(session()->has('error_data.active')) error @endif">
                                        @if(session()->has('error_data.active')) 
                                            {{ session('error_data.active') }}
                                        @endif
                                    </span>
                                    <input name="active" 
                                           type="checkbox" 
                                           {{ (! empty(old('active')) ? 'checked' : (isset($delivery) && $delivery->active ? 'checked' : '')) }}
                                           >@lang('admin.active')
                                </label>
                            </div>
                            @foreach($currencies as $currency)
                                <p>{{ $currency->shortcut }}</p>
                                <div class="form-group">
                                    <span class="@if(session()->has('error_data.price.'.$currency->id)) error @endif">
                                        @if(session()->has('error_data.price.'.$currency->id))
                                            {{ session('error_data.price.'.$currency->id) }}
                                        @endif
                                    </span>
                                    <input class="form-control @if(session()->has('error_data.price.'.$currency->id)) error-input @endif" 
                                           placeholder="@lang('admin.price') ({{ $currency->shortcut}})" 
                                           name="price[{{ $currency->id}}]" 
                                           type="text" 
                                           value="{{ old('price.'.$currency->id, isset($delivery) ? $delivery->getPrice($currency->id)->price : '') }}"
                                           required>
                                </div>
                            @endforeach
                            <div class="form-group">
                                <div class="col-sm-6">
                                    <span>@lang('admin.photo'):</span>
                                    @if ($errors->has('photo'))
                                        <span class=error">
                                            {{ $errors->first('photo') }}
                                        </span>
                                    @endif
                                    <input type="file" name="photo" value="" accept="image/*" />
                                </div>
                                <div class="col-sm-6">
                                    @if(isset($delivery->logo))
                                        <label for="delete_photo">@lang('admin.delete_photo')</label>
                                        <input type="checkbox" name="delete_photo" id="delete_photo" />
                                        <img src="{{ asset('storage/images/deliveries/'.$delivery->id.'/original/'.$delivery->logo) }}" 
                                            alt="{{ $delivery->getData()->name }}" class="miniature" />
                                    @endif
                                </div>
                            </div>
                            <input type="hidden" name="id" value="{{ $delivery->id ?? '' }}" />
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection