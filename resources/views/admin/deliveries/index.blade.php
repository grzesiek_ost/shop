@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/Sortable.min.js') }}"></script>
    <script src="{{ asset('js/admin/delivery.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<input type="hidden" name="url_change_order" value="{{ route('admin.deliveries.order') }}" />

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.deliveries.create') }}" role="button">@lang('admin.add_new')</a>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.name')</th>
                <th>@lang('admin.description')</th>
                <th>@lang('admin.photo')</th>
                <th>@lang('admin.price')</th>
                <th>@lang('admin.activity')</th>
                <th>*</th>
            </tr>
        </thead>
        <tbody id="sortable_tbody">
            @foreach($deliveries as $delivery)
                <tr class="active sortable_tr" draggable="false" data-sortable-id="{{ $loop->iteration }}">
                    <td class="sortable_handle">
                        <i class="fas fa-grip-horizontal"></i>
                        {{ $delivery->id }}
                    </td>
                    <td>{{ $delivery->getData()->name }}</td>
                    <td>{{ $delivery->getData()->description }}</td>
                    <td>
                        @if($delivery->logo)
                            <img src="{{ asset('storage/images/deliveries/'.$delivery->id.'/original/'.$delivery->logo) }}" 
                                 alt="{{ $delivery->getData()->name }}" class="miniature" draggable="false" />
                        @endif
                    </td>
                    <td>{{ $delivery->getPrice()->price }} {{ $currency->shortcut }}</td>
                    <td>{{ $delivery->active ? 'yes' : 'no' }}</td>
                    <td>
                        <a href="{{ route('admin.deliveries.edit', ['id' => $delivery->id]) }}" title="@lang('admin.edit')">
                            <i class="admin_fa far fa-edit fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.deliveries.delete', ['id' => $delivery->id]) }}"
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $deliveries->links() }}
</div>

@endsection
