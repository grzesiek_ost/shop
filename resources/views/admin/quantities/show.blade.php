@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.quantity_info')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_quantity" method="post" action="{{ route('admin.products.quantity.save', ['id' => $product->id]) }}">
                        {{ csrf_field() }}
                        <fieldset>
                            @foreach($product->sizes as $size)
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.size')) error @endif">
                                    @if(session()->has('error_data.size'))
                                        {{ session('error_data.size') }}
                                    @endif
                                </span>
                                <label for="size[{{$size->id}}]">{{ $size->getData()->name }} : {{ $product->sizeQuantity($size->id) }}</label>
                                <input class="form-control @if(session()->has('error_data.size.'.$size->id)) error-input @endif"
                                       name="size[{{ $size->id }}]" 
                                       type="number" 
                                       value="{{ old('size.'.$size->id) ?? 0 }}"
                                       required>
                            </div>
                            @endforeach
                            <input type="hidden" name="id" value="{{ $product->id ?? '' }}" />
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection