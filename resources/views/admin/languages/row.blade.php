@foreach($languages as $language)
    <div class="row" id="language_{{ $language->id }}" style="@if($language->main) font-weight: bold; @endif">
        <div class="col-xs-12 col-sm-6 col-md-2">
            {{ $language->name }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
            {{ $language->native_name }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
            {{ $language->shortcut }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
            <img src="{{ asset('images/flags/'.$language->shortcut.'.png') }}"
                 alt="{{ $language->name }}" class="miniature" />
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
            @if($language->active)
                @lang('admin.yes')
            @else
                @lang('admin.no')
            @endif
        </div>
        <div class="col-xs-12 col-sm-6 col-md-2">
            <a href="{{ route('admin.languages.edit', ['id' => $language->id]) }}" title="@lang('admin.edit')">
                <i class="admin_fa far fa-edit fa-2x"></i>
            </a>
            <a href="{{ route('admin.languages.delete', ['id' => $language->id]) }}"
               title="@lang('admin.delete')" class="delete_item">
                <i class="admin_fa far fa-trash-alt fa-2x"></i>
            </a>
        </div>
    </div>
@endforeach