@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_language" method="post" action="{{ route('admin.languages.update', ['id' => $language->id]) }}" enctype="multipart/form-data">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <fieldset>
                            <div class="form-group">
                                <span class="@if($errors->has('name')) error @endif">
                                    {{ $errors->first('name') }}
                                </span>
                                <input class="form-control @if($errors->has('name')) error-input @endif"
                                       placeholder="@lang('admin.name')"
                                       name="name"
                                       type="text"
                                       value="{{ old('name') ?? $language->name }}"
                                       required maxlength="191">
                            </div>
                            <div class="form-group">
                                <span class="@if($errors->has('native_name')) error @endif">
                                    {{ $errors->first('native_name') }}
                                </span>
                                <input class="form-control @if($errors->has('native_name')) error-input @endif"
                                       placeholder="@lang('admin.native_name')"
                                       name="native_name"
                                       type="text"
                                       value="{{ old('native_name') ?? $language->native_name }}"
                                       required maxlength="191">
                            </div>
                            <div class="form-group">
                                <span class="@if($errors->has('shortcut')) error @endif">
                                    {{ $errors->first('shortcut') }}
                                </span>
                                <input class="form-control @if($errors->has('shortcut')) error-input @endif"
                                       placeholder="@lang('admin.shortcut')"
                                       name="shortcut"
                                       type="text"
                                       value="{{ old('shortcut') ?? $language->shortcut }}"
                                       required maxlength="2" minlength="2">
                            </div>
                            <div class="checkbox">
                                <label>
                                    <input name="active"
                                           type="checkbox"
                                            {{ (! empty(old('active')) || $language->active ? 'checked' : '') }}
                                    >@lang('admin.active')
                                </label>
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection