<!--dropzone modal-->
<div class="modal fade" id="dropzoneModal" tabindex="-1" role="dialog" aria-labelledby="dropzoneModalLabel" aria-hidden="true"
    data-backdrop="static" data-keyboard="false">
    <div class="modal-dialog modal-dialog-centered modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <div class="row" style="padding-top: 40px;">
                    <div class="col-md-3 docs-toggles">
                        @include('admin.cropper.settings_panel')
                    </div>
                    <div class="col-md-9">
                        <div class="img-container">
                            <img id="image" src="" alt="" />
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>