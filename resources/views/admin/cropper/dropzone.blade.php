@section('admin_style')
    @parent
    <link rel="stylesheet" href="{{ asset('plugins/cropper/cropper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/dropzone/dropzone.min.css') }}">

    <link rel="stylesheet" href="{{ asset('css/admin/dropzone-cropper.css') }}">
@append

@section('admin_scripts')
    @parent
    <script src="{{ asset('plugins/cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-cropper/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('plugins/dropzone/dropzone.min.js') }}"></script>

    <script src="{{ asset('js/admin/dropzone-cropper.js') }}"></script>
@append

@section('content')
    @parent
    @include('admin.cropper.dropzone_modal')
    @include('admin.cropper.show_image_modal')
@append

    <div class="dropzone" id="dropzone" data-url="{{ route('admin.ajaxUpload') }}"
         data-delete-url="{{ route('admin.ajaxDelete') }}"
         data-add-remove-links="true"
         data-max-file-size="{{ $maxFileSize ?? 2 }}"
         data-max-files="{{ $maxFiles ?? 1 }}"
         data-default-message="@lang('dropzoneCropper.dropImages')"
         data-param-name="{{ $paramName ?? 'file' }}"
         data-accepted-files="{{ $acceptedFiles ?? 'image/jpeg' }}"
         data-ratio="{{ $photoConfig['aspectRatio']['width']/$photoConfig['aspectRatio']['height'] }}"
    >
    </div>
    <div id="photo_data" style="display: none;"></div>
