<div class="btn-group">
    <label class="btn btn-primary btn-upload" for="inputImage" title="@lang('dropzoneCropper.select')">
        <input type="file" class="sr-only" id="inputImage" name="file" accept=".jpg,.jpeg,.png,.gif,.bmp,.tiff">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.select')">
            <span class="fa fa-upload"></span> @lang('dropzoneCropper.select')
        </span>
    </label>
</div>
<div class="btn-group d-flex flex-nowrap" data-toggle="buttons">
{{--    <label class="btn btn-primary">--}}
{{--        <input type="radio" class="sr-only" id="aspectRatio0" name="aspectRatio" value="1.7777777777777777">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 16 / 9">--}}
{{--            16:9--}}
{{--        </span>--}}
{{--    </label>--}}
{{--    <label class="btn btn-primary">--}}
{{--        <input type="radio" class="sr-only" id="aspectRatio1" name="aspectRatio" value="1.3333333333333333">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 4 / 3">--}}
{{--            4:3--}}
{{--        </span>--}}
{{--    </label>--}}
{{--    <label class="btn btn-primary">--}}
{{--        <input type="radio" class="sr-only" id="aspectRatio2" name="aspectRatio" value="1">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 1 / 1">--}}
{{--            1:1--}}
{{--        </span>--}}
{{--    </label>--}}
{{--    <label class="btn btn-primary">--}}
{{--        <input type="radio" class="sr-only" id="aspectRatio3" name="aspectRatio" value="0.6666666666666666">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: 2 / 3">--}}
{{--            2:3--}}
{{--        </span>--}}
{{--    </label>--}}
{{--    <label class="btn btn-primary active">--}}
{{--        <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="aspectRatio: NaN">--}}
{{--            Free--}}
{{--        </span>--}}
{{--    </label>--}}
    @if(isset($photoConfig['aspectRatio']))
        <label class="btn btn-primary">
            <input type="radio" class="sr-only" id="aspectRatio" name="aspectRatio" value="{{ $photoConfig['aspectRatio']['width']/$photoConfig['aspectRatio']['height'] }}">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.aspectRatio') {{ $photoConfig['aspectRatio']['width'] }} / {{ $photoConfig['aspectRatio']['height'] }}">
                {{ $photoConfig['aspectRatio']['width'] }}:{{ $photoConfig['aspectRatio']['height'] }}
            </span>
        </label>
    @else
        <label class="btn btn-primary active">
            <input type="radio" class="sr-only" id="aspectRatio4" name="aspectRatio" value="NaN">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.aspectRatio'): NaN">
                @lang('dropzoneCropper.free')
            </span>
        </label>
    @endif
</div>
<div class="btn-group docs-buttons">
    <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="move" title="@lang('dropzoneCropper.move')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.move')">
            <span class="fas fa-arrows-alt"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="setDragMode" data-option="crop" title="@lang('dropzoneCropper.setDragMode')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.setDragMode')">
            <span class="fa fa-crop"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="zoom" data-option="0.1" title="@lang('dropzoneCropper.zoomIn')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.zoomIn')">
            <span class="fa fa-search-plus"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="zoom" data-option="-0.1" title="@lang('dropzoneCropper.zoomOut')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.zoomOut')">
            <span class="fa fa-search-minus"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="move" data-option="-10" data-second-option="0" title="@lang('dropzoneCropper.moveLeft')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.moveLeft')">
            <span class="fa fa-arrow-left"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="move" data-option="10" data-second-option="0" title="@lang('dropzoneCropper.moveRight')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.moveRight')">
            <span class="fa fa-arrow-right"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="-10" title="@lang('dropzoneCropper.moveUp')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.moveUp')">
            <span class="fa fa-arrow-up"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="move" data-option="0" data-second-option="10" title="@lang('dropzoneCropper.moveDown')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.moveDown')">
            <span class="fa fa-arrow-down"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="rotate" data-option="-45" title="@lang('dropzoneCropper.rotateLeft')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.rotateLeft')">
            <span class="fas fa-undo"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="rotate" data-option="45" title="@lang('dropzoneCropper.rotateRight')">
      <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.rotateRight')">
        <span class="fas fa-redo"></span>
      </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="scaleX" data-option="-1" title="@lang('dropzoneCropper.flipHorizontal')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.flipHorizontal')">
            <span class="fas fa-arrows-alt-h"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="scaleY" data-option="-1" title="@lang('dropzoneCropper.flipVertical')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.flipVertical')">
            <span class="fas fa-arrows-alt-v"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="crop" title="@lang('dropzoneCropper.crop')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.crop')">
            <span class="fa fa-check"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="clear" title="@lang('dropzoneCropper.clear')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.clear')">
            <span class="fas fa-times"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="reset" title="@lang('dropzoneCropper.reset')">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.reset')">
            <span class="fas fa-sync-alt"></span>
        </span>
    </button>
    <button type="button" class="btn btn-primary" data-method="zoomTo" data-option="1">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.zoomTo')">
            Zoom 100%
        </span>
    </button>
</div>
<div class="btn-group btn-group-crop docs-buttons">
    {{--@foreach($photoConfig['sizes'] as $size)
        <button type="button" class="btn btn-success" data-method="getCroppedCanvas" data-option="{ &quot;width&quot;: {{ $size['width'] }}, &quot;height&quot;: {{ $size['height'] }} }">
            <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="$().cropper(&quot;getCroppedCanvas&quot;, { width: {{ $size['width'] }}, height: {{ $size['height'] }} })">
                {{ $size['width'] }}&times;{{ $size['height'] }}
            </span>
        </button>
    @endforeach--}}
{{--    <button type="button" class="btn btn-primary" data-method="zoomTo" data-option="1">--}}
{{--        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="cropper.zoomTo(1)">--}}
{{--            Zoom to 100%--}}
{{--        </span>--}}
{{--    </button>--}}
</div>
<div class="docs-data">
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <label class="input-group-text" for="dataWidth">@lang('dropzoneCropper.width')</label>
        </span>
        <input type="text" class="form-control" id="dataWidth" placeholder="@lang('dropzoneCropper.width')" disabled>
        <span class="input-group-append">
            <span class="input-group-text">px</span>
        </span>
    </div>
    <div class="input-group input-group-sm">
        <span class="input-group-prepend">
            <label class="input-group-text" for="dataHeight">@lang('dropzoneCropper.height')</label>
        </span>
        <input type="text" class="form-control" id="dataHeight" placeholder="@lang('dropzoneCropper.height')" disabled>
        <span class="input-group-append">
            <span class="input-group-text">px</span>
        </span>
    </div>
</div>
<div class="docs-buttons">
    <button type="button" class="btn btn-primary" data-method="getCroppedCanvas" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.getCroppedImage')">
            @lang('dropzoneCropper.getCroppedImage')
        </span>
    </button>
    <button type="button" class="btn btn-success btn-lg btn-block" data-method="cropImage" data-option="{ &quot;maxWidth&quot;: 4096, &quot;maxHeight&quot;: 4096 }">
        <span class="docs-tooltip" data-toggle="tooltip" data-animation="false" title="@lang('dropzoneCropper.cropImage')">
            @lang('dropzoneCropper.cropImage')
        </span>
    </button>
</div>