@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('plugins/jquery-cropper/cropper.min.css') }}">
    <link rel="stylesheet" href="{{ asset('plugins/jquery-cropper/main.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/jquery-cropper/cropper.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-cropper/jquery-cropper.min.js') }}"></script>
    <script src="{{ asset('plugins/jquery-cropper/main.js') }}"></script>
@endsection

@section('content')

    <div class="container">
        <div class="row" style="padding-top: 40px;">
            <div class="col-md-3 docs-toggles">
                @include('admin.cropper.settings_panel')
            </div>
            <div class="col-md-9">
                <div class="img-container">
                    <img id="image" src="" alt="" />
                </div>
            </div>
        </div>
    </div>

    @include('admin.cropper.show_image_modal')

@endsection