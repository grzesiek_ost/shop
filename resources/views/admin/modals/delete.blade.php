<!--modal confirming the removal-->
<div class="modal fade" id="deleteModal" tabindex="-1" role="dialog" aria-labelledby="deleteModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
            <div class="modal-body" style="text-align: center;">
                <p>@lang('admin.confirm_delete')</p>
                <form method="POST" action="">
                    @csrf
                    @method('DELETE')
                    <button type="button" class="btn btn-default" data-dismiss="modal">@lang('admin.cancel')</button>
                    <input type="submit" class="btn btn-danger" value="@lang('admin.delete')">
                </form>
            </div>
        </div>
    </div>
</div>