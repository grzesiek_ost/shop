@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.sizes.create') }}" role="button">@lang('admin.add_new')</a>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.name')</th>
                <th>*</th>
            </tr>
        </thead>
        <tbody>
            @foreach($sizes as $size)
                <tr class="active">
                    <td>{{ $size->id }}</td>
                    <td>{{ $size->getData()->name }}</td>
                    <td>
                        <a href="{{ route('admin.sizes.edit', ['id' => $size->id]) }}" title="@lang('admin.edit')">
                            <i class="admin_fa far fa-edit fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.sizes.delete', ['id' => $size->id]) }}" 
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $sizes->links() }}
</div>

@endsection
