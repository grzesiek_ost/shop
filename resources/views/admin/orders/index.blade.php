@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.buyer_data')</th>
                <th>@lang('admin.products')</th>
                <th>@lang('admin.order_price')</th>
                <th>@lang('admin.status')</th>
                <th>@lang('admin.payment')</th>
                <th>@lang('admin.delivery')</th>
                <th>@lang('admin.buy_date')</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($orders as $order)
                <tr class="active">
                    <td>{{ $order->id }}</td>
                    <td>
                        <p style="margin: 0;">{{ $order->data->name .' '. $order->data->surname }}</p>
                        <p style="margin: 0;">{{ $order->data->street .' '. $order->data->house_number . ($order->data->apartment_number ? '/'.$order->data->apartment_number : '') }}</p>
                        <p style="margin: 0;">{{ $order->data->zip_code .' '. $order->data->city }}</p>
                        <p style="margin: 0;">{{ $order->data->email }}</p>
                        <p style="margin: 0;">{{ $order->data->phone }}</p>
                    </td>
                    <td>
                        @foreach($order->items as $product)
                            <p>{{ $product->product }} ({{ $product->size }}) | {{ $product->count }} x {{ $product->item_price .' '.$product->currency }}</p>
                        @endforeach
                    </td>
                    <td>{{ $order->getOrderSum() .' '.$order->currency }}</td>
                    <td>TODO</td>
                    <td>{{ $order->payment }}</td>
                    <td>
                        {{ $order->delivery }}
                        <br>
                        {{ $order->delivery_price .' '.$order->currency }}
                    </td>
                    <td>{{ $order->created_at->format('d.m.Y H:i') }}</td>
                    <td>
                        <a href="{{ route('admin.orders.delete', ['id' => $order->id]) }}" 
                           title="@lang('admin.delete') {{ lcfirst(trans('admin.order')) }}" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                        @if($order->invoice)
                            <a href="{{ route('admin.invoices.show', ['id' => $order->invoice->id]) }}" title="@lang('admin.get_invoice')">
                                <i class="admin_fa fas fa-file-invoice fa-2x"></i>
                            </a>
                        @else
                            <a href="{{ route('admin.invoices.store', ['id' => $order->id]) }}" title="@lang('admin.make_invoice')">
                                <i class="admin_fa far fa-plus-square fa-2x"></i>
                            </a>
                        @endif
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $orders->links() }}
</div>

@endsection
