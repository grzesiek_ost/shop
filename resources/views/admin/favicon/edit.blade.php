@extends('layouts.app')

@section('admin_style')

@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('admin.favicon_info')</h3>
                    </div>
                    <div class="error" style="padding: 15px 0 0 15px;">{!! implode('', $errors->all('<p>:message</p>')) !!}</div>
                    <div class="panel-body">
                        <form name="favicon_update" method="post" action="{{ route('admin.favicon.update') }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            <div class="form-group">
                                <input type="file" name="favicon" accept="image/x-icon">
                            </div>
                            <div class="form-group">
                                <div class="col-sm-12">
                                    <span>@lang('admin.favicon'):</span>
                                    @if ($errors->has('photo'))
                                        <span class=error">
                                            {{ $errors->first('photo') }}
                                        </span>
                                    @endif
                                    @if(file_exists('storage/favicon.ico'))
                                        <img src="{{ asset('storage/favicon.ico') }}" style="padding: 2em;">
                                    @else
                                        <img src="{{ asset('favicon.ico') }}" style="padding: 2em;">
                                    @endif
                                </div>
                                @if(file_exists('storage/favicon.ico'))
                                    <div class="col-sm-12">
                                        <label for="delete">@lang('admin.check_to_delete')</label>
                                        <input type="checkbox" name="delete" id="delete" />
                                    </div>
                                @endif
                            </div>
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection