@foreach($categories as $category)
    <div class="row" id="category_{{ $category->id }}" data-category_id="{{ $category->category_id }}">
        <div class="col-xs-12 col-sm-6 col-md-3">
            {{ $category->getData()->name }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            {{ $category->makeTree() }}
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            @if($category->photo)
                <img src="{{ asset('storage/images/categories/640x480/'.$category->photo) }}"
                    alt="{{ $category->getData()->name }}" class="miniature" />
            @endif
        </div>
        <div class="col-xs-12 col-sm-6 col-md-3">
            <a href="{{ route('admin.categories.edit', ['id' => $category->id]) }}" title="@lang('admin.edit')">
                <i class="admin_fa far fa-edit fa-2x"></i>
            </a>
            <a href="{{ route('admin.categories.delete', ['id' => $category->id]) }}" 
               title="@lang('admin.delete')" class="delete_item">
                <i class="admin_fa far fa-trash-alt fa-2x"></i>
            </a>
            <a href="" class="show_subcategory" data-category_id="{{ $category->id }}" 
               data-show_subcategories_url="{{ route('admin.categories.show_subcategories', ['id' => $category->id]) }}">
                <i class="admin_fa far fa-caret-square-down fa-2x rotate"></i>
            </a>
        </div>
    </div>
@endforeach