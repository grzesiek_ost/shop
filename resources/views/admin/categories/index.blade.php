@extends('layouts.app')

@section('scripts')
    <script src="{{ asset('js/admin/categories.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('admin_style')
<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.categories.create') }}" role="button">@lang('admin.add_new')</a>
    <div class="container">
        <div class="row header_row">
            <div class="col-xs-12 col-sm-6 col-md-3">
                <p>@lang('admin.name')</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <p>@lang('admin.category_path')</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                <p>@lang('admin.photo')</p>
            </div>
            <div class="col-xs-12 col-sm-6 col-md-3">
                
            </div>
        </div>
        @include('admin.categories.row')
    </div>
</div>

@endsection

