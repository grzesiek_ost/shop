@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

    <div class="container">
        <div class="row">
            <div class="col-md-12">
                <div class="panel panel-default">
                    <div class="panel-heading">
                        <h3 class="panel-title">@lang('admin.fill_all')</h3>
                    </div>
                    <div class="panel-body">
                        <form name="save_category" method="post" action="{{ route('admin.categories.update', ['id' => $category->id]) }}" enctype="multipart/form-data">
                            {{ csrf_field() }}
                            {{ method_field('PUT') }}
                            @include('admin.cropper.dropzone', [
                                'photoConfig' => $photoConfig,
                                'maxFileSize' => 2,
                                'maxFiles' => 1,
                                'paramName' => 'file',
                                'acceptedFiles' => 'image/jpeg'
                            ])
                            <fieldset>
                                @foreach($languages as $language)
                                    <div>
                                        <img src="{{ asset('images/flags/'.$language->shortcut).'.png' }}"
                                                        alt="{{ $language->name }}" />
                                        <p class="language_name">{{ ucfirst($language->name) }}</p>
                                    </div>
                                    <div class="form-group">
                                        <span class="@if(session()->has('error_data.name.'.$language->id)) error @endif">
                                            @if(session()->has('error_data.name.'.$language->id))
                                                {{ session('error_data.name.'.$language->id) }}
                                            @endif
                                        </span>
                                        <input class="form-control @if(session()->has('error_data.name.'.$language->id)) error-input @endif"
                                               placeholder="@lang('admin.name') ({{ $language->shortcut }})"
                                               name="name[{{ $language->id }}]"
                                               type="text"
                                               value="{{ old('name.'.$language->id) ?? $category->getData($language->id)->name }}"
                                               required>
                                    </div>
                                @endforeach
                                <div class="form-group">
                                    <label for="category">@lang('admin.parent_category')</label>
                                    <select class="category selectpicker" name="category" id="category">
                                        <option></option>
                                        @foreach($categories as $item)
                                            <option value="{{ $item->id }}"
                                                    {{ (! empty(old('category')) && old('category') == $item->id ? 'selected' : $category->category_id == $item->id ? 'selected' : '') }}
                                                    >
                                                {{ $item->makeTree() }}
                                            </option>
                                        @endforeach
                                    </select>
                                </div>
                                <div class="form-group">
                                    <div class="col-sm-12">
                                        <span>@lang('admin.photo'):</span>
                                        @if ($errors->has('photo'))
                                            <span class=error">
                                                {{ $errors->first('photo') }}
                                            </span>
                                        @endif
                                        <img src="{{ asset('storage/images/categories/' .
                                        $photoConfig['width'] . 'x' . $photoConfig['height'] . '/' . $category->photo) }}"
                                             alt="{{ $category->getData()->name }}" class="miniature" />
                                    </div>
                                </div>
                                <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                            </fieldset>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection