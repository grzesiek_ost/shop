@extends('layouts.app')


@section('admin_style')
<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_status" method="post" action="{{ route('admin.statuses.save') }}">
                        {{ csrf_field() }}
                        <fieldset>
                            @foreach($languages as $language)
                                <div>
                                    <img src="{{ asset('images/flags/'.$language->shortcut).'.png' }}" 
                                                    alt="{{ $language->name }}" />
                                    <p class="language_name">{{ ucfirst($language->name) }}</p>
                                </div>
                                <div class="form-group">
                                    <span class="@if(session()->has('error_data.name.'.$language->id)) error @endif">
                                        @if(session()->has('error_data.name.'.$language->id))
                                            {{ session('error_data.name.'.$language->id) }}
                                        @endif
                                    </span>
                                    <input class="form-control @if(session()->has('error_data.name.'.$language->id)) error-input @endif" 
                                           placeholder="@lang('admin.name') ({{ $language->shortcut }})" 
                                           name="name[{{ $language->id }}]" 
                                           type="text" 
                                           value="{{ old('name.'.$language->id) ?? isset($status) ? $status->getData($language->id)->name : '' }}"
                                           required>
                                </div>
                            @endforeach
                            
                            <input type="hidden" name="id" value="{{ $status->id ?? '' }}" />
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection