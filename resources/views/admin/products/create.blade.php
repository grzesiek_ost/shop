@extends('layouts.app')


@section('admin_style')

<!--TODO without this glyphicons in bootstrap-select are missed -->
<link href="//netdna.bootstrapcdn.com/bootstrap/3.0.0/css/bootstrap-glyphicons.css" rel="stylesheet">

<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
<script src="{{ asset('js/admin/products.js') }}"></script>
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_product" method="post" action="{{ route('admin.products.save') }}">
                        {{ csrf_field() }}
                        <fieldset>
                            @foreach($languages as $language)
                            <div>
                                <img src="{{ asset('images/flags/'.$language->shortcut).'.png' }}" 
                                                alt="{{ $language->name }}" />
                                <p class="language_name">{{ ucfirst($language->name) }}</p>
                            </div>
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.name.'.$language->id)) error @endif">
                                    @if(session()->has('error_data.name.'.$language->id))
                                        {{ session('error_data.name.'.$language->id) }}
                                    @endif
                                </span>
                                <input class="form-control @if(session()->has('error_data.name.'.$language->id)) error-input @endif" 
                                       placeholder="@lang('admin.name') ({{ $language->shortcut }})" 
                                       name="name[{{ $language->id }}]" 
                                       type="text" 
                                       value="{{ old('name.'.$language->id) ?? isset($product) ? $product->getProductData($language->id)->name : '' }}"
                                       required>
                            </div>
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.description.'.$language->id)) error @endif">
                                    @if(session()->has('error_data.description.'.$language->id))
                                        {{ session('error_data.description.'.$language->id) }}
                                    @endif
                                </span>
                                <input class="form-control @if(session()->has('error_data.description.'.$language->id)) error-input @endif" 
                                       placeholder="@lang('admin.description') ({{ $language->shortcut }})" 
                                       name="description[{{ $language->id }}]" 
                                       type="text" 
                                       value="{{ old('description.'.$language->id) ?? isset($product) ? $product->getProductData($language->id)->description : '' }}"
                                       required>
                            </div>
                            @endforeach
                            <div class="checkbox">
                                <label>
                                    <span class="@if(session()->has('error_data.active')) error @endif">
                                        @if(session()->has('error_data.active')) 
                                            {{ session('error_data.active') }}
                                        @endif
                                    </span>
                                    <input name="active" 
                                           type="checkbox" 
                                           {{ (! empty(old('active')) ? 'checked' : (isset($product) && $product->active ? 'checked' : '')) }}
                                           >@lang('admin.active')
                                </label>
                            </div>
                            @foreach($currencies as $currency)
                            <p>{{ $currency->shortcut }}</p>
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.price.'.$currency->id)) error @endif">
                                    @if(session()->has('error_data.price.'.$currency->id))
                                        {{ session('error_data.price.'.$currency->id) }}
                                    @endif
                                </span>
                                <input class="form-control @if(session()->has('error_data.price.'.$currency->id)) error-input @endif" 
                                       placeholder="@lang('admin.price') ({{ $currency->shortcut}})" 
                                       name="price[{{ $currency->id}}]" 
                                       type="text" 
                                       value="{{ old('price.'.$currency->id) ?? isset($product) ? $product->getPrice($currency->id)->price : '' }}"
                                       required>
                            </div>
                            @endforeach
                            <div class="form-group">
                                <label>@lang('admin.category')</label>
                                <select class="category" name="category">
                                    @foreach($categories as $item)
                                        <option value="{{ $item->id }}"
                                                {{ (! empty(old('category')) && old('category') == $item->id ? 'selected' : (isset($product) && $product->category_id == $item->id ? 'selected' : '')) }}
                                                >
                                            {{ $item->makeTree() }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <div class="form-group">
                                <label>@lang('admin.sizes')</label>
                                <select class="sizes selectpicker" name="sizes[]" multiple data-actions-box="true">
                                    @foreach($sizes as $size)
                                        <option value="{{ $size->id }}"
                                                {{ (! empty(old('size')) && in_array($size->id, old('size')) ? 'selected' : (isset($selected_sizes) && in_array($size->id, $selected_sizes) ? 'selected' : !isset($selected_sizes) && $size->default_size ? 'selected' : '')) }}>
                                            {{ $size->getData()->name }}
                                        </option>
                                    @endforeach
                                </select>
                            </div>
                            <input type="hidden" name="id" value="{{ $product->id ?? '' }}" />
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection