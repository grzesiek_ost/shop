@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/products.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.products.create') }}" role="button">@lang('admin.add_new')</a>
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.name')</th>
                <th>@lang('admin.price')</th>
                <th>*</th>
            </tr>
        </thead>
        <tbody>
            @foreach($products as $product)
                <tr class="active">
                    <td>{{ $product->id }}</td>
                    <td><a href="{{ route('shop.product', ['id' => $product->id]) }}">{{ $product->getProductData()->name }}</a></td>
                    <td>{{ $product->getPrice()->price }} {{ $currency->shortcut }}</td>
                    <td>
                        <a href="{{ route('admin.products.edit', ['id' => $product->id]) }}" title="@lang('admin.edit')">
                            <i class="admin_fa far fa-edit fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.products.delete', ['id' => $product->id]) }}"
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.products.photos.index', ['id' => $product->id]) }}" title="@lang('admin.photos')">
                            <i class="admin_fa far fa-images fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.products.quantity.show', ['id' => $product->id]) }}" title="@lang('admin.quantity')">
                            <i class="admin_fa fas fa-sort-numeric-up fa-2x"></i>
                        </a>
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $products->links() }}
</div>

@endsection
