@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/Sortable.min.js') }}"></script>
    <script src="{{ asset('js/admin/order.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

    @include('admin.modals.delete')

    <input type="hidden" name="url_change_order" value="{{ route('admin.pages.slider.photos.order', ['id' => $slider->element->page_id, 'slider_id' => $slider->id]) }}" />
    <input type="hidden" name="item_change_order" value="slider_id" />

    <div class="container">
        @if (session('message'))
            <div class="alert alert-info">{{ session('message') }}</div>
        @endif
        <a class="btn btn-primary" href="{{ route('admin.pages.index') }}" role="button">@lang('admin.back')</a>
        <a class="btn btn-primary" href="{{ route('admin.pages.slider.photos.create', ['id' => $slider->element->page_id, 'slider_id' => $slider->id]) }}" role="button">@lang('admin.add_new')</a>

        <div id="sortable_div">
            @foreach($slider->images as $image)
                <div class="sortable_element" draggable="false">
                    <div class="sortable_handle">
                        <i class="fas fa-grip-horizontal"></i>
                        {{ $image->getData()->name }}
                    </div>
                    <div>
                        @if($image->photo)
                            <img src="{{ asset('storage/images/slider/' . $slider->id . '/' .
                            $photoConfig['width'] . 'x' . $photoConfig['height'] . '/' . $image->photo) }}"
                                 alt="{{ $image->getData()->name }}" class="miniature" draggable="false"
                                 data-sortable-id="{{ $loop->iteration }}" />
                        @endif
                    </div>
                    <div>
                        <a href="{{ route('admin.pages.slider.photos.edit', ['id' => $slider->element->page_id, 'slider_id' => $slider->id, 'photo_id' => $image->id]) }}"
                           title="@lang('admin.edit')">
                            <i class="admin_fa far fa-edit fa-2x"></i>
                        </a>
                        <a href="{{ route('admin.pages.slider.photos.delete', ['id' => $slider->element->page_id, 'slider_id' => $slider->id, 'photo_id' => $image->id]) }}"
                           title="@lang('admin.delete')" class="delete_item">
                            <i class="admin_fa far fa-trash-alt fa-2x"></i>
                        </a>
                    </div>
                </div>
            @endforeach
        </div>
    </div>
    <input type="hidden" name="slider_id" id="slider_id" value="{{ $slider->id }}" />
@endsection