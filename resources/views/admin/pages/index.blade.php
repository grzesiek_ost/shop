@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.name')</th>
                <th>@lang('admin.url')</th>
                <th></th>
            </tr>
        </thead>
        <tbody>
            @foreach($pages as $page)
                <tr class="active">
                    <td>{{ $page->id }}</td>
                    <td>{{ $page->name }}</td>
                    <td>{{ $page->url }}</td>
                    <td>
                        @foreach($page->elements as $element)
                            @if($element->type == 'slider')
                                <a class="btn btn-warning" href="{{ route('admin.pages.slider.index', [$page->id, $element->elementable_id]) }}"
                                   role="button">Slider</a>
                            @elseif($element->type == 'wysiwyg')
                                <a class="btn btn-info" href="{{ route('admin.pages.wysiwyg.index', [$page->id, $element->elementable_id]) }}"
                                   role="button">Text</a>
                            @endif
                        @endforeach
                    </td>
                </tr>
            @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $pages->links() }}
</div>

@endsection
