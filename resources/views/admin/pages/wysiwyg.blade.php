@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('plugins/trumbowyg/dist/ui/trumbowyg.min.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/trumbowyg/dist/trumbowyg.min.js') }}"></script>
    <script src="{{ asset('js/admin/wysiwyg.js') }}"></script>
@endsection

@section('content')

<div class="container">
    <div class="error" style="padding: 15px 0 0 15px;">{!! implode('', $errors->all('<p>:message</p>')) !!}</div>
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_wysiwyg" method="post" action="{{ route('admin.pages.wysiwyg.update', [$wysiwyg->element->page_id, $wysiwyg->id]) }}">
                        {{ csrf_field() }}
                        {{ method_field('PUT') }}
                        <fieldset>
                            @foreach($all_languages as $language)
                            <div>
                                <img src="{{ asset('images/flags/'.$language->shortcut).'.png' }}" 
                                                alt="{{ $language->name }}" />
                                <p class="language_name">{{ ucfirst($language->name) }}</p>
                            </div>
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.name.'.$language->id)) error @endif">
                                    @if(session()->has('error_data.name.'.$language->id))
                                        {{ session('error_data.name.'.$language->id) }}
                                    @endif
                                </span>
                                <input class="form-control @if(session()->has('error_data.name.'.$language->id)) error-input @endif" 
                                       placeholder="@lang('admin.name') ({{ $language->shortcut }})" 
                                       name="name[{{ $language->id }}]" 
                                       type="text" 
                                       value="{{ old('name.'.$language->id) ?? isset($wysiwyg) ? ($wysiwyg->getData($language->id)->name ?? '') : '' }}"
                                       required>
                            </div>
                            <div class="form-group">
                                <span class="@if(session()->has('error_data.content.'.$language->id)) error @endif">
                                    @if(session()->has('error_data.content.'.$language->id))
                                        {{ session('error_data.content.'.$language->id) }}
                                    @endif
                                </span>
                                <textarea name="content[{{ $language->id }}]" required
                                          class="trumbowyg form-control @if(session()->has('error_data.content.'.$language->id)) error-input @endif">
                                    {!! old('content.'.$language->id) ?? isset($wysiwyg) ? ($wysiwyg->getData($language->id)->content ?? '') : '' !!}
                                </textarea>
                            </div>
                            @endforeach
                            <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                        </fieldset>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection