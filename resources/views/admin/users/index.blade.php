@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <table class="table">
        <thead>
            <tr>
                <th>#</th>
                <th>@lang('admin.first_name')</th>
                <th>@lang('admin.surname')</th>
                <th>@lang('admin.email')</th>
                <th>@lang('admin.phone')</th>
                <th>@lang('admin.role')</th>
                <th>@lang('admin.orders_sum')</th>
                <th>.</th>
            </tr>
        </thead>
        <tbody>
        @foreach($users as $user)
            <tr class="active">
                <td>{{ $user->id }}</td>
                <td>{{ $user->name }}</td>
                <td>{{ $user->surname }}</td>
                <td>{{ $user->email }}</td>
                <td>{{ $user->phone }}</td>
                <td>{{ $user->role->name }}</td>
                <td>{{ $user->getOrdersSum() }}</td>
                <td>
                    <a href="{{ route('admin.users.edit', ['id' => $user->id]) }}" title="@lang('admin.edit')">
                        <i class="admin_fa far fa-edit fa-2x"></i>
                    </a>
                    <a href="{{ route('admin.users.delete', ['id' => $user->id]) }}" 
                       title="@lang('admin.delete')" class="delete_item">
                        <i class="admin_fa far fa-trash-alt fa-2x"></i>
                    </a>
                </td>
            </tr>
        @endforeach
        </tbody>
    </table>
    <!--pagination-->
    {{ $users->links() }}
</div>

@endsection