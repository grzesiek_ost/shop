@extends('layouts.app')


@section('admin_style')
<link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('content')

<div class="container">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">@lang('admin.fill_all')</h3>
                </div>
                <div class="panel-body">
                    <form name="save_user" method="post" action="{{ route('admin.users.save') }}">
                        {{ csrf_field() }}
                        <div class="form-group">
                            <span class="@if(session()->has('error_data.name')) error @endif">
                                @if(session()->has('error_data.name'))
                                    {{ session('error_data.name') }}
                                @endif
                            </span>
                            <input class="form-control @if(session()->has('error_data.name')) error-input @endif" 
                                   placeholder="@lang('admin.first_name')" 
                                   name="name" 
                                   type="text" 
                                   value="{{ old('name') ?? isset($user) ? $user->name : '' }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if(session()->has('error_data.surname')) error @endif">
                                @if(session()->has('error_data.surname'))
                                    {{ session('error_data.surname') }}
                                @endif
                            </span>
                            <input class="form-control @if(session()->has('error_data.surname')) error-input @endif" 
                                   placeholder="@lang('admin.surname')" 
                                   name="surname" 
                                   type="text" 
                                   value="{{ old('surname') ?? isset($user) ? $user->surname : '' }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if(session()->has('error_data.email')) error @endif">
                                @if(session()->has('error_data.email'))
                                    {{ session('error_data.email') }}
                                @endif
                            </span>
                            <input class="form-control @if(session()->has('error_data.email')) error-input @endif" 
                                   placeholder="@lang('admin.email')" 
                                   name="email" 
                                   type="email" 
                                   value="{{ old('email') ?? isset($user) ? $user->email : '' }}"
                                   required>
                        </div>
                        <div class="form-group">
                            <span class="@if(session()->has('error_data.phone')) error @endif">
                                @if(session()->has('error_data.phone'))
                                    {{ session('error_data.phone') }}
                                @endif
                            </span>
                            <input class="form-control @if(session()->has('error_data.phone')) error-input @endif" 
                                   placeholder="@lang('admin.phone')" 
                                   name="phone" 
                                   type="tel" 
                                   value="{{ old('phone') ?? isset($user) ? $user->phone : '' }}"
                                   >
                        </div>
                        <div class="form-group">
                            <span class="@if(session()->has('error_data.password')) error @endif">
                                @if(session()->has('error_data.password'))
                                    {{ session('error_data.password') }}
                                @endif
                            </span>
                            <input class="form-control @if(session()->has('error_data.password')) error-input @endif" 
                                   placeholder="@lang('admin.password')" 
                                   name="password" 
                                   type="password" 
                                   value="{{ old('password') ?? '' }}"
                                   required
                                   autocomplete="off">
                        </div>
                        <input type="hidden" name="id" value="{{ $user->id ?? '' }}" />
                        <input class="btn btn-lg btn-success btn-block" type="submit" value="@lang('admin.save')">
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection