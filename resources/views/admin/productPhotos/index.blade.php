@extends('layouts.app')

@section('admin_style')
    <link rel="stylesheet" href="{{ asset('css/admin/admin.css') }}">
@endsection

@section('admin_scripts')
    <script src="{{ asset('plugins/Sortable.min.js') }}"></script>
    <script src="{{ asset('js/admin/productsPhotos.js') }}"></script>
    <script src="{{ asset('js/admin/modals/delete.js') }}"></script>
@endsection

@section('content')

@include('admin.modals.delete')

<input type="hidden" name="url_change_order" value="{{ route('admin.products.photos.order', ['id' => $product->id]) }}" />

<div class="container">
    @if (session('message'))
        <div class="alert alert-info">{{ session('message') }}</div>
    @endif
    <a class="btn btn-primary" href="{{ route('admin.products.index') }}" role="button">@lang('admin.back')</a>
    <a class="btn btn-primary" href="{{ route('admin.products.photos.create', ['id' => $product->id]) }}" role="button">@lang('admin.add_new')</a>

    <div id="sortable_div">
        @foreach($product->photos as $photo)
            <div class="sortable_element" draggable="false">
                <div class="sortable_handle">
                    <i class="fas fa-grip-horizontal"></i>
                    {{ $photo->getData()->name }}
                </div>
                <div>
                    @if($photo->photo)
                        <img src="{{ asset('storage/images/products/' . $product->id . '/' .
                            $photoConfig['width'] . 'x' . $photoConfig['height'] . '/' . $photo->photo) }}"
                             alt="{{ $photo->getData()->name }}" class="miniature" draggable="false"
                             data-sortable-id="{{ $loop->iteration }}" />
                    @endif
                </div>
                <div>
                    <a href="{{ route('admin.products.photos.edit', ['id' => $product->id, 'photo_id' => $photo->id]) }}" 
                       title="@lang('admin.edit')">
                        <i class="admin_fa far fa-edit fa-2x"></i>
                    </a>
                    <a href="{{ route('admin.products.photos.delete', ['id' => $product->id, 'photo_id' => $photo->id]) }}" 
                       title="@lang('admin.delete')" class="delete_item">
                        <i class="admin_fa far fa-trash-alt fa-2x"></i>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
</div>
<input type="hidden" name="product_id" id="product_id" value="{{ $product->id }}" />
@endsection