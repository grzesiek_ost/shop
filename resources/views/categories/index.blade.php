@extends('layouts.app')

@section('style')
    <link rel="stylesheet" href="{{ asset('css/homepage.css') }}">
@endsection

@section('content')
    <div class="container categories">
        @foreach($categories as $category)
            <div class="col-xs-12 col-sm-6 col-md-4 thumbex">
                <div class="thumbnail">
                    <a href="{{ route('shop.products', ['category' => \Illuminate\Support\Str::slug($category->getData()->name)]) }}">
                        <img src="{{asset('storage/images/categories/640x480/'.$category->photo)}}" />
                        <span>{{ $category->getData()->name }}</span>
                    </a>
                </div>
            </div>
        @endforeach
    </div>
@endsection
