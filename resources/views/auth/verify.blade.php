@extends('layouts.app')

@section('content')
<div class="container">
    <div class="row justify-content-center">
        <div class="col-md-8">
            <div class="card">
                <div class="card-header">@lang('auth.verify_email')</div>

                <div class="card-body">
                    @if (session('resent'))
                        <div class="alert alert-success" role="alert">
                            @lang('auth.link_sent')
                        </div>
                    @endif

                    @lang('auth.verification_link_info'), <a href="{{ route('verification.resend') }}">@lang('auth.click_here')</a>.
                </div>
            </div>
        </div>
    </div>
</div>
@endsection