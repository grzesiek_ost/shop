<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::group(['middleware' => 'language'], function () {
    
    // home page
    Route::get('/', 'LayoutController@index')->name('home_page');

    // auth routes
    Auth::routes(['verify' => true]);

    // only authorized users can access these routes, admin routes
    Route::group([
        'prefix' => 'admin', 
        'as' => 'admin.', 
        'middleware' => ['auth', 'verified', 'is_worker'], 
        'namespace' => 'Admin'], 
        function () {

        //    invoices routes
        Route::group(['prefix' => 'invoices', 'as' => 'invoices.'], function () {
            Route::get('/', InvoicesController::class.'@index')->name('index');
            Route::get('/{id}/generate', InvoicesController::class.'@store')->name('store');
            Route::get('/{id}/show', InvoicesController::class.'@show')->name('show');
            Route::delete('/{id}/delete', InvoicesController::class.'@destroy')->name('delete');
        });

        //    products routes
        Route::group(['prefix' => 'products', 'as' => 'products.'], function () {
            Route::get('/', ProductsController::class.'@index')->name('index');
            Route::get('/create', ProductsController::class.'@create')->name('create');
            Route::get('/{id}/edit', ProductsController::class.'@edit')->name('edit');
            Route::delete('/{id}/delete', ProductsController::class.'@destroy')->name('delete');
            Route::post('/store', ProductsController::class.'@store')->name('save');

            //      products photos routes
            Route::group(['prefix' => 'photos', 'as' => 'photos.'], function () {
                Route::get('/{id}', ProductPhotosController::class.'@index')->name('index');
                Route::post('/{id}/store', ProductPhotosController::class.'@store')->name('save');
                Route::put('/{id}/update/{photo_id}', ProductPhotosController::class.'@update')->name('update');
                Route::get('/{id}/create', ProductPhotosController::class.'@create')->name('create');
                Route::get('/{id}/edit/{photo_id}', ProductPhotosController::class.'@edit')->name('edit')
                    ->where('photo_id', '[0-9]+');
                Route::delete('/{id}/delete/{photo_id}', ProductPhotosController::class.'@destroy')->name('delete')
                    ->where('photo_id', '[0-9]+');
                Route::post('/{id}/change-order', ProductPhotosController::class.'@changeOrder')->name('order');
            });

            //      products quantities routes
            Route::group(['prefix' => 'quantity', 'as' => 'quantity.'], function () {
                Route::get('/{id}', QuantitiesController::class.'@show')->name('show');
                Route::post('/{id}/store', QuantitiesController::class.'@store')->name('save');
            });
        });

        //    categories routes
        Route::group(['prefix' => 'categories', 'as' => 'categories.'], function () {
            Route::get('/', CategoriesController::class.'@index')->name('index');
            Route::get('/create', CategoriesController::class.'@create')->name('create');
            Route::get('/{id}/edit', CategoriesController::class.'@edit')->name('edit');
            Route::post('/store', CategoriesController::class.'@store')->name('save');
            Route::put('/{id}/update', CategoriesController::class.'@update')->name('update');
            Route::delete('/{id}/delete', CategoriesController::class.'@destroy')->name('delete');
            Route::post('/{id}/show-subcategories', CategoriesController::class.'@showSubcategories')->name('show_subcategories');
        });

        //    orders routes
        Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
            Route::get('/', OrdersController::class.'@index')->name('index');
            Route::get('/{id}/edit', OrdersController::class.'@edit')->name('edit');
            Route::post('/store', OrdersController::class.'@store')->name('save');
            Route::delete('/{id}/delete', OrdersController::class.'@destroy')->name('delete');
        });

        //    deliveries routes
        Route::group(['prefix' => 'deliveries', 'as' => 'deliveries.'], function () {
            Route::get('/', DeliveriesController::class.'@index')->name('index');
            Route::get('/create', DeliveriesController::class.'@create')->name('create');
            Route::get('/{id}/edit', DeliveriesController::class.'@edit')->name('edit');
            Route::post('/store', DeliveriesController::class.'@store')->name('save');
            Route::delete('/{id}/delete', DeliveriesController::class.'@destroy')->name('delete');
            Route::post('/change-order', DeliveriesController::class.'@changeOrder')->name('order');
        });

        //    payments routes
        Route::group(['prefix' => 'payments', 'as' => 'payments.'], function () {
            Route::get('/', PaymentsController::class.'@index')->name('index');
            Route::get('/create', PaymentsController::class.'@create')->name('create');
            Route::get('/{id}/edit', PaymentsController::class.'@edit')->name('edit');
            Route::post('/store', PaymentsController::class.'@store')->name('save');
            Route::delete('/{id}/delete', PaymentsController::class.'@destroy')->name('delete');
            Route::post('/change-order', PaymentsController::class.'@changeOrder')->name('order');
        });

        //    sizes routes
        Route::group(['prefix' => 'sizes', 'as' => 'sizes.'], function () {
            Route::get('/', SizesController::class.'@index')->name('index');
            Route::get('/create', SizesController::class.'@create')->name('create');
            Route::get('/{id}/edit', SizesController::class.'@edit')->name('edit');
            Route::post('/store', SizesController::class.'@store')->name('save');
            Route::delete('/{id}/delete', SizesController::class.'@destroy')->name('delete');
        });

        //    statuses routes
        Route::group(['prefix' => 'statuses', 'as' => 'statuses.'], function () {
            Route::get('/', StatusesController::class.'@index')->name('index');
            Route::get('/create', StatusesController::class.'@create')->name('create');
            Route::get('/{id}/edit', StatusesController::class.'@edit')->name('edit');
            Route::post('/store', StatusesController::class.'@store')->name('save');
            Route::delete('/{id}/delete', StatusesController::class.'@destroy')->name('delete');
        });

        //    users routes
        Route::group(['prefix' => 'users', 'as' => 'users.'], function () {
            Route::get('/', UsersController::class.'@index')->name('index');
            Route::get('/create', UsersController::class.'@create')->name('create');
            Route::get('/{id}/edit', UsersController::class.'@edit')->name('edit');
            Route::post('/store', UsersController::class.'@store')->name('save');
            Route::delete('/{id}/delete', UsersController::class.'@destroy')->name('delete');
        });

        //    languages routes
        Route::group(['prefix' => 'languages', 'as' => 'languages.'], function () {
            Route::get('/', LanguagesController::class.'@index')->name('index');
            Route::get('/create', LanguagesController::class.'@create')->name('create');
            Route::get('/{id}/edit', LanguagesController::class.'@edit')->name('edit');
            Route::post('/store', LanguagesController::class.'@store')->name('save');
            Route::delete('/{id}/delete', LanguagesController::class.'@destroy')->name('delete');
            Route::put('/{id}/update', LanguagesController::class.'@update')->name('update');
        });

        //    pages routes
        Route::group(['prefix' => 'pages', 'as' => 'pages.'], function () {
            Route::get('/', PagesController::class.'@index')->name('index');

            //  slider routes
            Route::group(['prefix' => '/{id}/slider/{slider_id}', 'as' => 'slider.'], function () {
                Route::get('/', SliderController::class.'@index')->name('index');

                //  slider images
                Route::group(['prefix' => '/photos', 'as' => 'photos.'], function () {
                    Route::get('/create', SliderController::class.'@create')->name('create');
                    Route::get('/edit/{photo_id}', SliderController::class.'@edit')->name('edit')
                        ->where('photo_id', '[0-9]+');
                    Route::post('/store', SliderController::class.'@store')->name('save');
                    Route::put('/update/{photo_id}', SliderController::class.'@update')->name('update');
                    Route::delete('/delete/{photo_id}', SliderController::class.'@destroy')->name('delete')
                        ->where('photo_id', '[0-9]+');
                    Route::post('/change-order', SliderController::class.'@changeOrder')->name('order');
                });
            });

            //  wysiwyg routes
            Route::group(['prefix' => '/{id}/wysiwyg', 'as' => 'wysiwyg.'], function () {
                Route::get('/{wysiwyg_id}', PagesController::class.'@wysiwyg')->name('index')->where('wysiwyg_id', '[0-9]+');
                Route::put('/{wysiwyg_id}/update', PagesController::class.'@updateWysiwyg')->name('update')->where('wysiwyg_id', '[0-9]+');
            });
        });

        //    favicon routes
        Route::group(['prefix' => 'favicon', 'as' => 'favicon.'], function () {
            Route::get('/', FaviconController::class.'@edit')->name('edit');
            Route::put('/update', FaviconController::class.'@update')->name('update');
        });

        Route::post('/ajax-upload', UploadController::class.'@create')->name('ajaxUpload');
        Route::delete('/ajax-delete', UploadController::class.'@delete')->name('ajaxDelete');
    });
    
    //only authorized users can access these routes
    Route::group(['middleware' => ['auth', 'verified']], function () {
        
        Route::group([
            'prefix' => 'user', 
            'as' => 'user.', 
            'namespace' => 'User'], 
            function () {
                //  user orders
                Route::group(['prefix' => 'orders', 'as' => 'orders.'], function () {
                    Route::get('/', OrdersController::class.'@index')->name('index');
                });

                //  user information
                Route::group(['prefix' => 'information', 'as' => 'information.'], function () {
                    Route::get('/', UserInformationsController::class.'@index')->name('index');
                    Route::post('/', UserInformationsController::class.'@store')->name('save');
                    Route::get('/change-password', UserInformationsController::class.'@password')->name('password');
                    Route::post('/change-password', UserInformationsController::class.'@changePassword')->name('changePassword');
                });
            }
        );

    //    page after login
        Route::get('/home', 'HomeController@index')->name('home');

    //    user routes
        Route::group(['prefix' => 'user', 'as' => 'user.'], function () {
            Route::get('/', function(){
                echo 'site for logged user';
            });
        });
    });

    //  order routes
    Route::group(['prefix' => 'order', 'as' => 'order.'], function () {
        Route::get('shipping-information', OrdersController::class.'@shippingInfo')->name('shipping_info');
        Route::post('shipping-information', OrdersController::class.'@backToShippingInfo')->name('shipping_info_back');
        Route::post('delivery', OrdersController::class.'@delivery')->name('delivery');
        Route::get('delivery', OrdersController::class.'@delivery');
        Route::post('delivery/back', OrdersController::class.'@backToDelivery')->name('delivery_back');
        Route::get('delivery/back', OrdersController::class.'@backToDelivery');
        Route::post('payment', OrdersController::class.'@payment')->name('payment');
        Route::post('payment/back', OrdersController::class.'@backToPayment')->name('payment_back');
        Route::get('payment/back', OrdersController::class.'@backToPayment');
        Route::post('summary', OrdersController::class.'@summary')->name('summary');
        Route::get('summary', OrdersController::class.'@summary');
        Route::post('store', OrdersController::class.'@store')->name('save');
        Route::get('finalization', OrdersController::class.'@finalization')->name('finalization');
    });

    Route::post('get-quantity', QuantitiesController::class.'@returnQuantity')->name('get_quantity');

    //  shop routes
    Route::group(['prefix' => 'shop', 'as' => 'shop.'], function () {
        Route::get('/', CategoriesController::class.'@categories')->name('categories');
        Route::get('{category}', CategoriesController::class.'@products')->name('products');
        Route::get('products/{id}', ProductsController::class.'@show')->name('product');
        Route::get('{category}/search', CategoriesController::class.'@search')->name('search');
    });

    Route::get('products', ProductsController::class.'@index')->name('products');
    //Route::get('products/{id}', ProductsController::class.'@show');


    //  cart routes
    Route::group(['prefix' => 'cart', 'as' => 'cart.'], function () {
        Route::get('/', CartsController::class.'@index')->name('index');
        Route::post('/', CartsController::class.'@backToCart')->name('back');
        Route::post('store', CartsController::class.'@store')->name('add');
        Route::post('destroy', CartsController::class.'@destroy')->name('remove');
    });


    Route::group(['prefix' => 'contact', 'as' => 'contact.'], function () {
        Route::get('/', ContactController::class.'@index')->name('index');
        Route::post('send', ContactController::class.'@send')->name('send');
    });

    Route::post('change-language', LanguagesController::class.'@changeLanguage')->name('changeLanguage');

    Route::post('change-currency', CurrenciesController::class.'@changeCurrency')->name('changeCurrency');

    //  404 page
    Route::fallback(function () {
        return view('fallback');
    });
});